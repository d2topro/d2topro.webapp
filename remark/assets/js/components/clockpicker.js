/*!
 * remark v1.0.7 (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("clockpicker", {
  mode: "default",
  defaults: {
    donetext: "Done"
  }
});
