/*!
 * remark v1.0.7 (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("select2", {
  mode: "default",
  defaults: {
    width: "style"
  }
});
