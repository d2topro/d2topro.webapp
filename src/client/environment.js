(function(window) {
    window.__env = {
        key: 'development',
        apiUrl: 'https://demo-esportsfield.herokuapp.com',
        socketServerUrl: 'https://demo-esportsfield.herokuapp.com',
        assetsUrl: 'https://s3.eu-central-1.amazonaws.com/assets-winranger-com',
        debug: true
    };
})(window);
