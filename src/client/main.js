'use strict';

(function() {
    /**
     * PUSH Notification manager
     */

    var userSubscription;

    if (!('serviceWorker' in navigator)) {
        return console.warn('Service worker is not supported');
    }

    // Are Notifications supported in the service worker?
    if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
        console.warn('Notifications are not supported.');
        return;
    }

    // Check if push messaging is supported
    if (!('PushManager' in window)) {
        console.warn('Push messaging is not supported.');
        return;
    }

    return navigator.serviceWorker
        .register('/service-worker.js')
        .then(function() {
            // Check the current Notification permission.
            // If its denied, it's a permanent block until the
            // user changes the permission
            if (Notification.permission === 'denied') {
                throw new Error('Notifications has been blocked');
            }

            return navigator.serviceWorker.ready;
        })
        .then(function(serviceWorkerRegistration) {

            return serviceWorkerRegistration.pushManager.subscribe({
                userVisibleOnly: true
            });
        })
        .then(function(subscription) {
            userSubscription = subscription.toJSON();

            return request('/api/account', {
                method: 'GET'
            });
        })
        .then(function(response) {
            var account = response.json;

            if (isSubscriptionEqual(account, userSubscription)) {
                console.log('Subscription is valid')
                return;
            }

            return request('/api/account/browser-push/subscription', {
                method: 'PUT',
                headers: {
                    'X-CSRF-Token': response.headers['X-CSRF-Token'],
                    'Content-Type': 'application/json'
                },
                body: userSubscription
            })
            .then(function() {
                console.log('subscription has been updated');
            });
        })
        .catch(function(err) {
            console.error('Error during subscribe', err);
        });

    function isSubscriptionEqual(account, subscription) {
        var bs = account.browser_subscription;

        return bs && (bs.endpoint === subscription.endpoint &&
            bs.keys.p256dh === subscription.keys.p256dh &&
            bs.keys.auth === subscription.keys.auth);
    }

    function request(url, params) {
        url = window.__env.apiUrl + url;

        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open(params.method, url, true);
            xhr.withCredentials = true;
            xhr.onload = function() {
                if ([200, 201].indexOf(this.status) !== -1) {
                    return resolve({
                        json: this.response ? JSON.parse(this.response) : '',
                        headers: {
                            'X-CSRF-Token':
                                this.getResponseHeader('X-CSRF-Token')
                        }
                    });
                }

                var error = new Error(this.statusText);
                error.code = this.status;

                return reject(error);
            };

            xhr.onerror = reject;

            for (var key in params.headers) {
                xhr.setRequestHeader(key, params.headers[key]);
            }

            xhr.send(JSON.stringify(params.body));
        });
    }
    /**
     * END Notification manager
     */

})();
