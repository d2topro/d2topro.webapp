(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .directive('tournamentLargeImg', tournamentLargeImg);

    /* @ngInject */
    function tournamentLargeImg() {
        return {
            restrict: 'A',
            scope: {
                url: '='
            },
            link: function(scope, element) {
                var url = scope.url ?
                    scope.url :
                    'https://s3.eu-central-1.amazonaws.com/assets-winranger-com/miscellaneous/pages-bg/dashboard-bg.jpg';
                element.css('background-image', 'url(' + url + ')');
            }
        };
    }
})();
