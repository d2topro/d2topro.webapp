(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .directive('currentMatchTeam', currentMatchTeam);

    /* @ngInject */
    function currentMatchTeam() {
        return {
            restrict: 'A',
            scope: {
                team: '='
            },
            templateUrl: 'app/tournaments/details/view-modes/current-match/team.html'
        }
    }
})();
