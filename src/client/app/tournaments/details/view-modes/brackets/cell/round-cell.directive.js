(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .directive('roundCell', roundCell);

    /* @ngInject */
    function roundCell(MatchService) {
        return {
            restrict: 'A',
            scope: {
                game: '='
            },
            templateUrl: 'app/tournaments/details/view-modes/brackets/cell/round-cell.html',
            link: function(scope) {
                if (scope.game.state === MatchService.states().INVISIBLE) {
                    scope.state = 'invisible';
                }

                if (scope.game.state === MatchService.states().GAME) {
                    scope.state = 'game';
                    MatchService.serialize(scope.game)
                        .then(function(game) {
                            scope.game = game;
                        });
                }

                if (scope.game.state === MatchService.states().UNKNOWN) {
                    scope.state = 'unknown';
                }

            }
        }
    }
})();
