(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .controller('TournamentDetailsController', tournamentDetailsController);

    /* @ngInject */
    function tournamentDetailsController($q, $window, $intercom, $scope,
        lodash, tournament, TournamentDetailsService, MatchService,
        CurrentMatchService, TournamentRequestService, AccountModalService,
        TournamentService, gettextCatalog, SocketService) {

        var vm = this;
        vm.tournament = tournament;
        vm.switchTo = switchTo;
        vm.rounds = null;
        vm.viewMode = null;
        vm.currentMatch = null;
        vm.accountCurrentMatch = null;
        vm.teams = [];
        vm.launchDota = launchDota;
        vm.currentLang = currentLang;
        vm.showAccountInfoModal = showAccountInfoModal;
        vm.sortByEloRating = sortByEloRating;
        vm.sortByAvgEloRating = sortByAvgEloRating;
        vm.sortByName = sortByName;
        vm.sortByTeamName = sortByTeamName;
        vm.participantsReverse = participantsReverse;


        activate();


        SocketService.on('tm:match:start', function() {
            return MatchService.fetch(vm.tournament.id);
        });

        SocketService.on('tm:match:complete', function() {
            return MatchService.fetch(vm.tournament.id);
        });


        var currentMatchListener = $scope.$watch(
            function() {
                return CurrentMatchService.getMatches();
            },
            function(newValue, oldValue) {
                if (newValue !== oldValue) {
                    activate();
                }
            }
        );

        var matchListener = $scope.$watch(
            function() {
                return MatchService.getMatches(vm.tournament.id);
            },
            function(newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                return newValue.then(function(matches) {
                    vm.rounds = TournamentDetailsService
                        .getRounds(vm.tournament, matches);
                });
            }
        );

        var requestsListener = $scope.$watch(
            function() {
                return TournamentRequestService
                    .getRequests(vm.tournament.id);
            },
            function(newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                return newValue
                    .then(function(requests) {
                        vm.participants = vm.tournament.isSolo() ? requests :
                            TournamentDetailsService.getTeams(requests);
                        return TournamentService
                            .getTournament(vm.tournament.id);
                    })
                    .then(function(tournament) {
                        vm.tournament = tournament;
                    });
            });


        $scope.$on('destroy', function() {
            currentMatchListener();
            matchListener();
            requestsListener();
        });

        function activate() {
            return $q.all({
                    requests: TournamentRequestService.fetch(vm.tournament.id),
                    matches: MatchService.getMatches(vm.tournament.id),
                    currentMatch: CurrentMatchService.getMatch(vm.tournament.id)
                })
                .then(function(response) {
                    if (response.currentMatch) {
                        vm.currentMatch = response.currentMatch;
                    }

                    vm.requests = response.requests;
                    vm.participants = vm.tournament.isSolo() ? vm.requests :
                        TournamentDetailsService.getTeams(vm.requests);

                    vm.rounds = TournamentDetailsService
                        .getRounds(vm.tournament, response.matches);

                    vm.generalInfo = TournamentDetailsService
                        .getGeneralInfo(vm.tournament);

                    vm.bracketsGridWidth = vm.rounds.bracketsGridWidth;
                })
                .then(setViewMode)
                .then(getAccountCurrentMatch);
        }

        function getAccountCurrentMatch() {
            return MatchService.fetchCurrentMatch()
                .then(function(matches) {
                    vm.accountCurrentMatch = lodash.find(matches, function(m) {
                        return m.id === vm.currentMatch.id;
                    });
                });
        }

        function setViewMode() {
            vm.viewMode = vm.currentMatch ? 'currentMatch' : 'brackets';
            if (!vm.tournament.isInvitationPeriod() &&
                !vm.tournament.isRunning()) {
                vm.viewMode = 'generalInfo';
            }
        }

        function switchTo(viewMode) {
            vm.viewMode = viewMode;
            return viewMode;
        }

        function launchDota() {
            $intercom('trackEvent', 'click-launch-dota-btn');
            $window.location.href = 'steam://run/570';
        }

        function showAccountInfoModal(account) {
            return AccountModalService.showAccountInfoModal(account);
        }

        function sortByEloRating() {
            vm.participants = lodash.sortBy(vm.participants,
                function(o) {
                    return o.account.statistics[0].rank;
                });
            participantsReverse();
            return vm.participants;
        }

        function sortByAvgEloRating() {
            vm.participants = lodash.sortBy(vm.participants, 'rankAverage');
            participantsReverse();
            return vm.participants;
        }

        function sortByName() {
            vm.participants = lodash.sortBy(vm.participants,
                function(o) {
                    return o.account.steam_personal_name;
                });
            return vm.participants;
        }

        function sortByTeamName() {
            vm.participants = lodash.sortBy(vm.participants, 'name');
            return vm.participants;
        }

        function participantsReverse() {
            vm.participants = lodash.reverse(vm.participants);
            return vm.participants;
        }

        function currentLang() {
            return gettextCatalog.getCurrentLanguage();
        }
    }
})();
