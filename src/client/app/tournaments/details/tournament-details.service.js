(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .factory('TournamentDetailsService', tournamentDetailsService);

    /* @ngInject */
    function tournamentDetailsService($log, lodash, gettextCatalog,
        MatchService, MatchMockService, MatchResource, DEFAULT_TEAM_LOGO) {

        var roundStr = gettextCatalog.getString('Round');
        var semiFinalStr = gettextCatalog.getString('Semi-final');
        var finalStr = gettextCatalog.getString('Final');

        return {
            getPlaceTitle: getPlaceTitle,
            getRounds: getRounds,
            getGeneralInfo: getGeneralInfo,
            getTeams: getTeams
        };

        function getGeneralInfo(tournament) {
            var placeRewards = tournament.getPlaceRewards();
            // var placeRewards = [100,50,25,15,10,5,4,2];

            var rewards = lodash.map(placeRewards, function(reward, i) {
                return {
                    title: getPlaceTitle(i),
                    amount: reward,
                    getImgName: function() {
                        switch (i) {
                            case 0:
                                return '1place';
                            case 1:
                                return '2place';
                            case 2:
                                return '3place';
                            default:
                                return 'place';
                        }
                    }
                };
            });


            var policy = [{
                    title: tournament.isSolo() ?
                        gettextCatalog.getString('Solo players') : gettextCatalog.getString('Team players'),
                    icon: 'icons8-controller'
                }, {
                    title: gettextCatalog.getString('Best of ') +
                        tournament.policy.final_series_count,
                    icon: 'icons8-leaderboard'
                }, {
                    title: tournament.participants()
                        .current + '/' +
                        tournament.participants()
                        .total,
                    icon: 'icons8-user-group-man-man'
                }

            ];

            return {
                rewards: rewards,
                policy: policy
            };
        }

        function getPlaceTitle(index) {
            switch (index) {
                case 0:
                    return gettextCatalog.getString('1st');
                case 1:
                    return gettextCatalog.getString('2nd');
                case 2:
                    return gettextCatalog.getString('3rd-4th');
                case 3:
                    return gettextCatalog.getString('5th-8th');
                case 4:
                    return gettextCatalog.getString('9th-16th');
                case 5:
                    return gettextCatalog.getString('17th-32st');
                case 6:
                    return gettextCatalog.getString('33nd-64rd');
                case 7:
                    return gettextCatalog.getString('65th-128th');
                case 8:
                    return gettextCatalog.getString('129th-256th');
                case 8:
                    return gettextCatalog.getString('257th-512th');
            }
        }

        function getTeams(requests) {
            return lodash
                .chain(requests)
                .groupBy('team_id')
                .map(function(requests) {
                    return {
                        name: requests[0].team.name,
                        logo_url: requests[0].team.logo_url || DEFAULT_TEAM_LOGO,
                        players: requests,
                        rankAverage: (lodash
                            .sumBy(requests, function(p) {
                                // TODO: Should find as for tournament.game_app_id
                                return p.account.statistics[0].rank;
                            })) / requests.length
                    };
                })
                .value();
        }

        function getRounds(tournament, matches) {
            var totalRounds = tournament.getMatchesCount();
            var rounds = [];

            matches = lodash.map(matches, function(match) {
                return new MatchResource(match);
            });

            var groupedRounds = lodash
                .chain(matches)
                .sortBy(function(match) {
                    return [match.round, match.index].join();
                })
                .groupBy(function(match) {
                    return [match.round, match.index].join();
                })
                .map(function(match) {
                    return lodash.max(match, 'series');
                })
                .groupBy('round')
                .value();

            for (var i = totalRounds - 1; i >= 0; i--) {
                if (groupedRounds[i]) {
                    rounds.push(groupedRounds[i]);
                } else {
                    var match = {
                        state: MatchService.states()
                            .UNKNOWN,
                        index: 0,
                        round: i
                    };

                    rounds.push([new MatchResource(match)]);
                }
            }

            fillEmptyMatches(rounds);
            getRoundDetails(rounds);
            splitByVisibleCells(rounds);

            return rounds;
        }

        function fillEmptyMatches(rounds) {
            rounds.forEach(function(round) {
                var indexCount = Math.pow(2, round[0].round);
                for (var i = 0; i < indexCount; i++) {
                    var matchedElement = {};

                    matchedElement = lodash.find(round, function(game) {
                        return game.index === i;
                    });

                    if (matchedElement === undefined) {
                        var match = new MatchResource({
                            index: i,
                            round: round[0].round,
                            state: MatchService.states()
                                .UNKNOWN
                        });

                        round.splice(i, 0, match);
                    }
                }
            });

            return rounds;
        }

        function getRoundDetails(rounds) {
            for (var i = 0; i < rounds.length - 1; i++) {
                var visiblePositions = [];
                var firstVisible;
                var delta = Math.pow(2, i);
                var title = i === rounds.length - 2 ?
                    semiFinalStr :
                    roundStr + ' ' + (i + 1);

                if (i === 0 || i === 1) {
                    firstVisible = 1;
                } else {
                    firstVisible = Math.pow(2, i - 1);
                }

                for (var j = 0; j < rounds[i].length; j++) {
                    visiblePositions.push(firstVisible);
                    firstVisible += delta;
                }

                rounds[i].details = {
                    title: title,
                    visiblePositions: visiblePositions
                };
            }

            rounds[rounds.length - 1].details = {
                title: finalStr,
                visiblePositions: [rounds[0].length / 2],
            };

            rounds.bracketsGridWidth =
                rounds.length * 230 + (rounds.length) * 15 - 30;

            return rounds;
        }

        function splitByVisibleCells(rounds) {
            var totalRounds = rounds.length - 1;
            var totalRoundCells = Math.pow(2, totalRounds);

            for (var i = 0; i < rounds.length; i++) {
                var visiblePositions = rounds[i].details.visiblePositions;

                for (var j = 0; j < totalRoundCells; j++) {
                    var isVisible = visiblePositions.find(function(position) {
                        return position === j + 1 ? true : false;
                    });

                    if (isVisible) {
                        var game = rounds[i][j];
                        if (game.state !== MatchService.states()
                            .UNKNOWN) {
                            game.state = MatchService.states()
                                .GAME
                        }
                    } else {
                        var invisibleMatchCell = new MatchResource({
                            round: (totalRounds - i),
                            state: MatchService.states()
                                .INVISIBLE
                        });

                        rounds[i].splice(j, 0, invisibleMatchCell);
                    }
                }
            }

            return rounds;
        }
    }
})();
