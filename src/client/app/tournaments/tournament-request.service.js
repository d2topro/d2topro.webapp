(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .factory('TournamentRequestService', tournamentRequestService);

    /* @ngInject */
    function tournamentRequestService($q, lodash, TournamentPlayRequestResource,
        AccountService) {

        var promise = {};

        return {
            fetch: fetch,
            getRequests: getRequests,
            getAccountRequest: getAccountRequest
        };

        function fetch(tournamentId) {
            promise[tournamentId] = TournamentPlayRequestResource.get({
                    id: tournamentId
                })
                .$promise;

            return promise[tournamentId];
        }

        function getRequests(tournamentId) {
            return promise[tournamentId] ?
                promise[tournamentId] : fetch(tournamentId);
        }

        function getAccountRequest(tournamentId) {
            return $q
                .all({
                    account: AccountService.getAccount(),
                    requests: getRequests(tournamentId)
                })
                .then(function (response) {
                    return lodash.find(response.requests, function (request) {
                        return request.account_id === response.account.id;
                    });
                });
        }
    }
})();
