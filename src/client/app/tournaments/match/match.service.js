(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .factory('MatchService', matchService);

    /* @ngInject */
    function matchService($q, $log, lodash, MatchResource, TeamNameService,
        AccountService, TournamentRequestService, TeamService, DEFAULT_TEAM_LOGO) {

        var currentMatchPromise, promise = {};

        return {
            fetch: fetch,
            getMatches: getMatches,
            getMatchWithOutcome: getMatchWithOutcome,
            serialize: serialize,
            states: states,
            fetchCurrentMatch: fetchCurrentMatch
        };

        function fetch(tournamentId) {
            promise[tournamentId] = MatchResource.get({
                    id: tournamentId
                })
                .$promise;

            return promise[tournamentId];
        }

        function getMatches(tournamentId) {
            return promise[tournamentId] ?
                promise[tournamentId] :
                fetch(tournamentId);
        }


        function fetchCurrentMatch() {
            currentMatchPromise = MatchResource.accountCurrentMatches()
                .$promise;

            return currentMatchPromise.then(function(resource) {
                return resource;
            });
        }

        function states() {
            return {
                INVISIBLE: 0,
                GAME: 1,
                UNKNOWN: 2
            };
        }

        function getMatchWithOutcome(tournamentId, matchId) {
            return getMatches(tournamentId)
                .then(function(matches) {
                    return lodash.find(matches, function(match) {
                        return match.id === matchId;
                    });
                })
                .then(function(match) {
                    return TournamentRequestService
                        .getAccountRequest(match.getTournamentId())
                        .then(function(request) {
                            var accountIndex = match.getGroups()
                                .indexOf(request.getGroup());
                            var opponentIndex = accountIndex === 0 ? 1 : 0;

                            match.accountOutcome = match.getOutcome()[accountIndex];
                            match.opponentOutcome = match.getOutcome()[opponentIndex];

                            return match;
                        });
                })
                .catch(function(cause) {
                    return $log.error(cause);
                });
        }

        function serialize(match) {
            match.goodTeam = {};
            match.badTeam = {};

            return TournamentRequestService
                .fetch(match.getTournamentId())
                .then(function(requests) {
                    return lodash.groupBy(requests, 'group_id');
                })
                .then(function(response) {
                    var goodTeamRequests = response[match.groups[0]];
                    var badTeamRequests = response[match.groups[1]];

                    if (goodTeamRequests[0].team_id) {
                        $q.all({
                                goodAccountTeam: TeamService.getTeam(goodTeamRequests[0].team_id),
                                badAccountTeam: TeamService.getTeam(badTeamRequests[0].team_id)
                            })
                            .then(function(response) {
                                match.goodTeam.name = response.goodAccountTeam.name;
                                if (response.goodAccountTeam.logo_url) {
                                    match.goodTeam.logoUrl = response.goodAccountTeam.logo_url;
                                } else {
                                    match.goodTeam.logoUrl = DEFAULT_TEAM_LOGO;
                                }

                                match.badTeam.name = response.badAccountTeam.name;
                                if (response.badAccountTeam.logo_url) {
                                    match.badTeam.logoUrl = response.badAccountTeam.logo_url;
                                } else {
                                    match.badTeam.logoUrl = DEFAULT_TEAM_LOGO;
                                }
                            });
                    } else {
                        match.goodTeam.name = goodTeamRequests[0]
                            .account.steam_personal_name;
                        match.goodTeam.logoUrl = goodTeamRequests[0]
                            .account.steam_avatar_url;

                        match.badTeam.name = badTeamRequests[0]
                            .account.steam_personal_name;
                        match.badTeam.logoUrl = badTeamRequests[0]
                            .account.steam_avatar_url;
                    }

                    match.goodTeam.members = [];
                    lodash.forEach(goodTeamRequests, function(request) {
                        return match.goodTeam.members.push(request.account);
                    });

                    match.badTeam.members = [];
                    lodash.forEach(badTeamRequests, function(request) {
                        return match.badTeam.members.push(request.account);
                    });

                    match.goodTeam.score = match.score[0];
                    match.badTeam.score = match.score[1];

                    if (match.series % 2) {
                        match.goodTeam.isDire = true;
                        match.badTeam.isDire = false;
                    } else {
                        match.goodTeam.isDire = false;
                        match.badTeam.isDire = true;
                    }

                    return match;
                })
                .catch(function(cause) {
                    return $log.error(cause);
                });
        }
    }
})();
