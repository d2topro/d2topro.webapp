(function() {
    'use strict';

    angular.module('app.tournaments').factory('MatchMockService', matchMockService);

    /* @ngInject */
    function matchMockService() {
        return {getMatches: getMatches}

        function getMatches() {
            return [
                {
                    has_series_end: true,
                    name: "t37 m36 2/2 (4,2) 1/1 0:0",
                    id: 36,
                    tournament_id: 37,
                    round: 1,
                    index: 1,
                    series: 0,
                    series_type: 1,
                    groups: [
                        4, 2
                    ],
                    score: [
                        0, 0
                    ],
                    outcome: [
                        0, 0
                    ],
                    status: 5,
                    started_at: null,
                    completed_at: "2016-10-21T09:42:42.889Z",
                    lobby_expired_at: "2016-10-21T09:42:42.971Z",
                    expired_at: "2016-10-21T09:42:42.889Z",
                    created_at: "2016-10-21T09:37:42.284Z",
                    updated_at: "2016-10-21T09:42:42.890Z"
                }, {
                    has_series_end: true,
                    name: "t37 m37 1/2 (3,1) 1/1 0:0",
                    id: 37,
                    tournament_id: 37,
                    round: 1,
                    index: 0,
                    series: 0,
                    series_type: 1,
                    groups: [
                        3, 1
                    ],
                    score: [
                        0, 0
                    ],
                    outcome: [
                        0, 0
                    ],
                    status: 2,
                    started_at: "2016-10-21T09:39:18.265Z",
                    completed_at: null,
                    lobby_expired_at: null,
                    expired_at: "2016-10-21T12:37:43.179Z",
                    created_at: "2016-10-21T09:37:42.898Z",
                    updated_at: "2016-10-21T09:43:20.222Z"
                }
            ]
            // var matches = [
            //     {
            //         has_series_end: true,
            //         name: "t31 m15 2/2 (2,3) 1/1 0:1",
            //         id: 15,
            //         tournament_id: 31,
            //         round: 3,
            //         index: 2,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             2, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             2, 1
            //         ],
            //         status: 3,
            //         started_at: "2016-10-20T12:29:46.085Z",
            //         completed_at: "2016-10-20T12:33:42.889Z",
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:29:02.871Z",
            //         created_at: "2016-10-20T12:29:02.392Z",
            //         updated_at: "2016-10-20T12:33:42.890Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m16 1/2 (4,1) 1/1 0:1",
            //         id: 16,
            //         tournament_id: 31,
            //         round: 3,
            //         index: 1,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             4, 1
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             2, 1
            //         ],
            //         status: 3,
            //         started_at: "2016-10-20T12:29:29.878Z",
            //         completed_at: "2016-10-20T12:34:43.035Z",
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:29:03.020Z",
            //         created_at: "2016-10-20T12:29:02.656Z",
            //         updated_at: "2016-10-20T12:34:43.035Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m17 1/1 (1,3) 1/3 0:1",
            //         id: 17,
            //         tournament_id: 31,
            //         round: 3,
            //         index: 4,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             1, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             2, 1
            //         ],
            //         status: 3,
            //         started_at: "2016-10-20T12:35:32.669Z",
            //         completed_at: "2016-10-20T12:40:10.443Z",
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:34:45.424Z",
            //         created_at: "2016-10-20T12:34:45.114Z",
            //         updated_at: "2016-10-20T12:40:10.444Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m18 1/1 (1,3) 2/3 0:1",
            //         id: 18,
            //         tournament_id: 31,
            //         round: 3,
            //         index: 5,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             1, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             0, 0
            //         ],
            //         status: 2,
            //         started_at: "2016-10-20T12:40:50.112Z",
            //         completed_at: null,
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:40:11.646Z",
            //         created_at: "2016-10-20T12:40:11.364Z",
            //         updated_at: "2016-10-20T12:40:50.112Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m18 1/1 (1,3) 2/3 0:1",
            //         id: 18,
            //         tournament_id: 31,
            //         round: 3,
            //         index: 3,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             1, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             0, 0
            //         ],
            //         status: 2,
            //         started_at: "2016-10-20T12:40:50.112Z",
            //         completed_at: null,
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:40:11.646Z",
            //         created_at: "2016-10-20T12:40:11.364Z",
            //         updated_at: "2016-10-20T12:40:50.112Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m18 1/1 (1,3) 2/3 0:1",
            //         id: 18,
            //         tournament_id: 31,
            //         round: 3,
            //         index: 6,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             1, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             0, 0
            //         ],
            //         status: 2,
            //         started_at: "2016-10-20T12:40:50.112Z",
            //         completed_at: null,
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:40:11.646Z",
            //         created_at: "2016-10-20T12:40:11.364Z",
            //         updated_at: "2016-10-20T12:40:50.112Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m18 1/1 (1,3) 2/3 0:1",
            //         id: 18,
            //         tournament_id: 31,
            //         round: 3,
            //         index: 0,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             1, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             0, 0
            //         ],
            //         status: 2,
            //         started_at: "2016-10-20T12:40:50.112Z",
            //         completed_at: null,
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:40:11.646Z",
            //         created_at: "2016-10-20T12:40:11.364Z",
            //         updated_at: "2016-10-20T12:40:50.112Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m18 1/1 (1,3) 2/3 0:1",
            //         id: 18,
            //         tournament_id: 31,
            //         round: 3,
            //         index: 7,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             1, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             0, 0
            //         ],
            //         status: 2,
            //         started_at: "2016-10-20T12:40:50.112Z",
            //         completed_at: null,
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:40:11.646Z",
            //         created_at: "2016-10-20T12:40:11.364Z",
            //         updated_at: "2016-10-20T12:40:50.112Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m18 1/1 (1,3) 2/3 0:1",
            //         id: 18,
            //         tournament_id: 31,
            //         round: 2,
            //         index: 1,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             1, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             0, 0
            //         ],
            //         status: 2,
            //         started_at: "2016-10-20T12:40:50.112Z",
            //         completed_at: null,
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:40:11.646Z",
            //         created_at: "2016-10-20T12:40:11.364Z",
            //         updated_at: "2016-10-20T12:40:50.112Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m18 1/1 (1,3) 2/3 0:1",
            //         id: 18,
            //         tournament_id: 31,
            //         round: 2,
            //         index: 0,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             1, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             0, 0
            //         ],
            //         status: 2,
            //         started_at: "2016-10-20T12:40:50.112Z",
            //         completed_at: null,
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:40:11.646Z",
            //         created_at: "2016-10-20T12:40:11.364Z",
            //         updated_at: "2016-10-20T12:40:50.112Z"
            //     },
            //     {
            //         has_series_end: true,
            //         name: "t31 m18 1/1 (1,3) 2/3 0:1",
            //         id: 18,
            //         tournament_id: 31,
            //         round: 1,
            //         index: 0,
            //         series: 0,
            //         series_type: 1,
            //         groups: [
            //             1, 3
            //         ],
            //         score: [
            //             0, 1
            //         ],
            //         outcome: [
            //             0, 0
            //         ],
            //         status: 2,
            //         started_at: "2016-10-20T12:40:50.112Z",
            //         completed_at: null,
            //         lobby_expired_at: null,
            //         expired_at: "2016-10-20T15:40:11.646Z",
            //         created_at: "2016-10-20T12:40:11.364Z",
            //         updated_at: "2016-10-20T12:40:50.112Z"
            //     }
            //
            // ];
            //
            // return matches;
        }
    }
})();
