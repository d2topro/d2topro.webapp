(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .factory('CurrentMatchService', currentMatchService);

    /* @ngInject */
    function currentMatchService($q, $log, lodash, MatchResource, MatchService,
        AccountService, TournamentService, TournamentRequestService) {

        var promise;

        return {
            fetch: fetch,
            getMatches: getMatches,
            getMatch: getMatch
        }

        function fetch() {
            promise = TournamentService
                .fetchMyTournaments()
                .then(function (myTournaments) {
                    var tournaments = lodash.filter(myTournaments, function (tournament) {
                        return tournament.status === 4;
                    });
                    var promises = lodash.map(tournaments, function (tournament) {
                        return $q
                            .all({
                                requests: TournamentRequestService.fetch(tournament.id),
                                matches: MatchService.fetch(tournament.id),
                                account: AccountService.getAccount()
                            })
                            .then(function (response) {
                                var myRequest = lodash.find(response.requests, function (request) {
                                    return response.account.id === request.account_id;
                                });

                                var myMatches = lodash.filter(response.matches, function (match) {
                                    return lodash.includes(match.groups, myRequest.group_id);
                                });

                                var min = lodash.chain(myMatches)
                                    .map('round')
                                    .min()
                                    .value();
                                var match = lodash
                                    .chain(myMatches)
                                    .filter(function (m) {
                                        return m.round === min;
                                    })
                                    .sortBy(function (m) {
                                        return -m.series;
                                    })
                                    .value()[0];

                                return match;
                            })
                            .then(function (match) {
                                if (match !== undefined) {
                                    return serialize(match);
                                }
                            });
                    });

                    return $q.all(promises);
                })
                .then(function (matches) {
                    return lodash.compact(matches);
                })
                .catch(function (cause) {
                    return $log.error(cause);
                });

            return promise;
        }

        function getMatches() {
            return promise ? promise : fetch();
        }

        function getMatch(tournamentId) {
            return getMatches()
                .then(function (matches) {
                    if (matches.length === 0) {
                        return null;
                    }

                    return lodash.find(matches, function (match) {
                        return match.tournament_id === tournamentId;
                    });
                });
        }

        function serialize(match) {
            if (!match) {
                return;
            }

            return $q.all({
                    account: AccountService.getAccount(),
                    match: MatchService.serialize(match),
                    tournament: TournamentService.getTournament(match.tournament_id)
                })
                .then(function (response) {
                    var myTeam = lodash.find(response.match.goodTeam.members, function (member) {
                        return member.id === response.account.id;
                    });

                    match.name = response.tournament.name;

                    if (myTeam === undefined) {
                        var swap = response.match.goodTeam;
                        match.goodTeam = response.match.badTeam;
                        match.badTeam = swap;
                    }

                    if (match.isInvitationPeriod()) {
                        match.stringStatus = 'invitation-period';
                    }

                    if (match.isRunning()) {
                        match.stringStatus = 'running';
                    }

                    if (match.isAwaiting()) {
                        if ((match.goodTeam.score > match.badTeam.score) &&
                            !match.has_series_end && (match.goodTeam.score > 0)) {
                            match.stringStatus = 'completed';
                        } else {
                            match.stringStatus = 'finished';
                        }
                    }

                    return match;
                })
                .catch(function (cause) {
                    return $log.error(cause);
                });
        }
    }
})();
