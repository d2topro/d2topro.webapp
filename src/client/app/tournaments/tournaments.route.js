(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .run(appRun);

    /* @ngInject */
    function appRun(routerHelper, gettextCatalog) {
        routerHelper.configureStates(getStates());

        function getStates() {
            return [{
                state: 'root.tournaments',
                config: {
                    url: '/tournaments',
                    //templateUrl: 'app/tournaments/tournament.html',
                    template: '<ui-view class="shuffle-animation"/>',
                    abstract: true
                }
            }, {
                state: 'root.tournaments.details',
                config: {
                    url: '/:id',
                    templateUrl: 'app/tournaments/details/tournament-details.html',
                    controller: 'TournamentDetailsController',
                    controllerAs: 'vm',
                    title: 'Tournament Details',
                    settings: {},
                    resolve: {
                        /* @ngInject */
                        tournament: function($stateParams, TournamentService) {
                            return TournamentService.getTournament($stateParams.id);
                        }
                    }
                }
            }];
        }
    }
})();
