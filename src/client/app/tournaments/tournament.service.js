(function () {
	'use strict';

	angular
		.module('app.tournaments')
		.factory('TournamentService', tournamentService);

	/* @ngInject */
	function tournamentService(lodash, $q, $state, $uibModal,
		TournamentResource, CurrencyService) {
		var promise, myTournamentPromise;

		return {
			fetch: fetch,
			filerTournaments: filerTournaments,
			getTournaments: getTournaments,
			fetchMyTournaments: fetchMyTournaments,
			getMyTournaments: getMyTournaments,
			getTournament: getTournament,
			showTournamentWonModal: showTournamentWonModal,
			showTournamentLoseModal: showTournamentLoseModal,
			showTournamentInfoModal: showTournamentInfoModal,
			userIsParticipant: userIsParticipant,
			enterLobby: enterLobby,
			filterNewTournaments: filterNewTournaments
		};

		function fetch() {
			promise = $q
				.all({
					tournaments: TournamentResource.query()
						.$promise,
					currencies: CurrencyService.getCurrencies()
				})
				.then(function (response) {
					lodash.each(response.tournaments, function (tournament) {
						mapCurrencies(tournament, response.currencies);
					});

					return response.tournaments;
				});

			return promise;
		}

		function getTournaments() {
			return promise ? promise : fetch();
		}


		function filerTournaments(tournaments, isNewTournaments) {
			var openLobby = [],
				running = [],
				byCooldown = [],
				premium = [],
				byDate = [];

			tournaments.forEach(function (tournament) {
				if (tournament.isPremium()) {
					return premium.push(tournament);
				} else {

					if (tournament.isReady()) {
						return openLobby.push(tournament);
					}

					if (tournament.isInvitationPeriod() ||
						tournament.isRunning()) {
						return running.push(tournament);
					}

					if (tournament.is_cooldown) {
						return byCooldown.push(tournament);
					}

					if (tournament.starts_at) {
						return byDate.push(tournament);
					}
				}
			});

			byDate.sort(function (a, b) {
				return a.starts_at - b.starts_at;
			});
			return isNewTournaments ?
				openLobby.concat(premium, byCooldown, byDate) :
				openLobby.concat(premium, byCooldown, byDate, running);

		}


		function fetchMyTournaments() {
			myTournamentPromise = TournamentResource.getAccountTournaments()
				.$promise;

			return myTournamentPromise;
		}

		function filterNewTournaments(tournaments) {
			return lodash.filter(tournaments, function (t) {
				return (t.isInvitationPeriod() || t.isRunning());
			});
		}

		function getMyTournaments() {
			return myTournamentPromise ?
				myTournamentPromise : fetchMyTournaments();
		}

		function getTournament(id) {
			return $q.all({
					tournament: TournamentResource.get({
							id: id
						})
						.$promise,
					currencies: CurrencyService.getCurrencies()
				})
				.then(function (response) {
					mapCurrencies(response.tournament, response.currencies);
					return response.tournament;
				});
		}

		function mapCurrencies(tournament, currencies) {
			tournament.currency = lodash.find(currencies, function (c) {
				return tournament.currency_id === c.id;
			});

			tournament.rewardCurrency = lodash.find(currencies, function (c) {
				return tournament.reward_currency_id === c.id;
			});
		}

		function userIsParticipant(tournament, account) {
			var playRequests = tournament.requests;

			// if (playRequests.length === 0) {
			//     return isParticipant;
			// }
			//

			var participant = lodash.find(playRequests, function (request) {
				return request.account_id === account.id;
			});

			return participant ? true : false;

		}

		function enterLobby(tournamentId) {
			$state.go('root.tournaments.details', {
				id: tournamentId
			});
		}

		function showTournamentWonModal(data) {
			return $uibModal
				.open({
					animation: true,
					backdrop: 'static',
					templateUrl: 'app/tournaments/modals/won/modal.html',
					controller: 'TournamentWonModalController',
					controllerAs: 'vm',
					size: 'md modal-center',
					backdropClass: 'BACKDROP_CLASS',
					windowTopClass: 'tournament-won-modal',
					resolve: {
						data: function () {
							return data;
						}
					}
				})
				.result;
		}

		function showTournamentLoseModal(data) {
			return $uibModal
				.open({
					animation: true,
					backdrop: 'static',
					templateUrl: 'app/tournaments/modals/lose/modal.html',
					controller: 'TournamentLoseModalController',
					controllerAs: 'vm',
					size: 'md modal-center',
					backdropClass: 'BACKDROP_CLASS',
					windowTopClass: 'tournament-lose-modal',
					resolve: {
						data: function () {
							return data;
						}
					}
				})
				.result;
		}

		function showTournamentInfoModal(tournament) {
			return $uibModal
				.open({
					animation: true,
					templateUrl: 'app/tournaments/modals/info/info.html',
					controller: 'TournamentInfoModalController',
					controllerAs: 'vm',
					size: 'lg modal-center',
					backdropClass: 'BACKDROP_CLASS',
					windowTopClass: 'tournament-info-modal',
					resolve: {
						tournament: function () {
							return tournament;
						}
					}
				})
				.result;
		}
	}
})();
