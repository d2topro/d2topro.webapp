(function() {
    'use strict';

    angular.module('app.tournaments')
        .factory('AbuseDetectService', abuseDetectService);

    /* @ngInject */
    function abuseDetectService(localStorageService) {
        var MAX_LEAVES_COUNT = 2;
        var BAN_AMOUNT = 10 * 60 * 1000;

        return {
            detectAbuseActivity: detectAbuseActivity,
            setAbuseActivity: setAbuseActivity,
            getBanTime: getBanTime
        };

        function getStorage() {
            var leavesCount = localStorageService.get('leavesCount');
            if (!leavesCount) {
                localStorageService.set('leavesCount', {});
                leavesCount = localStorageService.get('leavesCount');
            }

            return leavesCount;
        }

        function getBanTime(tournament) {
            var leavesCount = getStorage();
            return leavesCount[tournament.id] ?
                Math.ceil((leavesCount[tournament.id].timestamp -
                    Date.now()) / 1000 / 60) : null;
        }


        function detectAbuseActivity(tournament) {
            var leavesCount = getStorage();

            if (leavesCount[tournament.id] &&
                leavesCount[tournament.id].count > MAX_LEAVES_COUNT &&
                leavesCount[tournament.id].timestamp > Date.now()) {

                return true;
            }

            if (leavesCount[tournament.id] &&
                leavesCount[tournament.id].timestamp) {
                leavesCount[tournament.id].count = 0;
                leavesCount[tournament.id].timestamp = null;
            }

            return false;

        }

        function setAbuseActivity(tournament) {
            var leavesCount = getStorage();
            if (!tournament.isReady()) {

                return false;
            }

            if (leavesCount[tournament.id]) {
                leavesCount[tournament.id].count++;
                if (leavesCount[tournament.id].count >= MAX_LEAVES_COUNT) {
                    leavesCount[tournament.id].timestamp = Date.now() + BAN_AMOUNT;
                }
            } else {
                leavesCount[tournament.id] = {
                    count: 1
                };
            }

            localStorageService.set('leavesCount', leavesCount);
        }
    }
})();
