(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .factory('GameMockService', gameMockService);

    function gameMockService() {
        return {
            getCurrentGames: getCurrentGames
        }

        function getCurrentGames() {
            var games = [{
                id: 1,
                status: 1,
                good_team_id: 1,
                bad_team_id: 2,
                round: 0,
                game_index: 0,
                series_type: 3,
                good_team_series_wins: 0,
                bad_team_series_wins: 0,
                has_reward_been_given: false,
                place_taken: null,
                started_at: "2016-08-23T14:18:52.305Z",
                completed_at: null,
                created_at: "2016-08-01T10:18:52.305Z",
                updated_at: "2016-08-01T10:18:52.305Z",
                lobby_id: 1,
                tournament_id: 8,
                lobby: {
                    series_type: 3,
                    radiant_series_wins: 0,
                    dire_series_wins: 0,
                    has_radiant_won: null,
                    state: 0,
                    connect: null,
                    has_first_blood_happened: false,
                    created_at: "2016-08-01T10:18:52.308Z",
                    match_outcome: 0,
                    match_id: null,
                    lobby_id: null,
                    password: "qwerty",
                    radiant: [
                        "76561198304160198"
                    ],
                    dire: [
                        "76561198254154844"
                    ]
                },
                goodGuys: {
                    id: 6,
                    name: "rvrvr",
                    tag: "rrrv",
                    logo_url: "https://s3.eu-central-1.amazonaws.com/assets.winranger.com/7bf178f07d4a27e677818836d4c9f27b.jpeg",
                    is_virtual: false,
                    won_games_count: 0,
                    lost_games_count: 0,
                    members: [{
                        id: 6,
                        is_solo: false,
                        steam_id: "76561198304160198",
                        steam_personal_name: "binarnik25",
                        steam_avatar_url: "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/51/51e7817a9e3b34e65e6d32a10268b8b4fcddf70f_full.jpg",
                        steam_country_code: null,
                        TeamPlayer: {
                            status: 2,
                            role: 2,
                            won_games_count: 0,
                            lost_games_count: 0,
                            rate: 0,
                            created_at: "2016-08-01T10:07:33.651Z",
                            updated_at: "2016-08-01T10:07:33.651Z",
                            account_id: 6,
                            team_id: 6
                        }
                    }]
                },
                badGuys: {
                    id: 7,
                    name: "OfficerBarbrady7",
                    tag: "Offic",
                    logo_url: null,
                    is_virtual: false,
                    won_games_count: 0,
                    lost_games_count: 0,
                    members: [{
                        id: 7,
                        is_solo: false,
                        steam_id: "76561198254154844",
                        steam_personal_name: "OfficerBarbrady7",
                        steam_avatar_url: "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/c3/c34985daa0644c1b18cff29d18ed5d00ff0a0e9d_full.jpg",
                        steam_country_code: null,
                        TeamPlayer: {
                            status: 2,
                            role: 2,
                            won_games_count: 0,
                            lost_games_count: 0,
                            rate: 0,
                            created_at: "2016-08-01T10:09:38.985Z",
                            updated_at: "2016-08-01T10:09:38.985Z",
                            account_id: 7,
                            team_id: 7
                        }
                    }]
                }
            }]

            return games;

        }
    }

})();
