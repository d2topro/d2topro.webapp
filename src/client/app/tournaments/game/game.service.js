(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .factory('GurrentGameService', GurrentGameService);

    function GurrentGameService($q, GameResource, TournamentService) {

        var promise;

        return {
            fetch: fetch,
            getCurrentGame: getCurrentGame
        };

        function fetch() {
            promise = GameResource.getCurrentGames()
                .$promise;
            return promise;
        }

        function getCurrentGames() {
            return promise ? promise : fetch();
        }

        function getCurrentGame() {
            var myTournaments = [];

            return $q.all({
                    myTournaments: TournamentService.fetchMyTournaments(),
                    currentGames: getCurrentGames(),
                })
                .then(function(response) {
                    if (hasNoActivity(response)) {
                        return null;
                    }

                    myTournaments = response.myTournaments;
                    // var currentGame = new GameResource(GameMockService.getCurrentGames()[0]);
                    var currentGame = new GameResource(response.currentGames[0]);
                    return currentGame.isTournamentRunning(myTournaments) ?
                        currentGame : null;
                });
        }

        function hasNoActivity(response) {
            return (response.myTournaments.length === 0 ||
                response.currentGames.length === 0);
        }
    }
})();
