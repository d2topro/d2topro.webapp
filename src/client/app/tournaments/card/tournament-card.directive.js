(function () {
    'use strict'

    angular
        .module('app.tournaments')
        .directive('tournamentCard', tournamentCard);

    /* @ngInject */
    function tournamentCard($window, moment, lodash, gettextCatalog,
        TournamentService, CardService, SpinnerService, CurrentMatchService,
        logger) {

        return {
            restrict: 'E',
            scope: {
                tournament: '=',
                account: '='
            },
            templateUrl: 'app/tournaments/card/card.html',
            link: function (scope, element) {
                scope.moment = moment;
                scope.invitationEndTime =
                    scope.tournament.getInvitationPeriodEnd();

                scope.startsAt = function () {
                    var date = Date.parse(new Date(scope.tournament.starts_at));
                    return date === 0 ? Date.now() + 61 * 1000 : date;
                };

                scope.status = function () {
                    return CardService.getTournamentStrStatus(scope.tournament);
                };

                scope.colorClass = function () {
                    return scope.isParticipant() ? scope.status() + '-color' : 'default';
                };

                scope.getParticipantsCounter = function () {
                    var total = scope.tournament.participants()
                        .total;
                    var current = scope.tournament.participants()
                        .current;

                    return current + '/' + total;
                };

                scope.startSpinner = function () {
                    return SpinnerService.start(element);
                };

                scope.getPrizePool = function () {
                    return scope.tournament.getPrizePoolAmount();
                };

                scope.isParticipant = function () {
                    return TournamentService
                        .userIsParticipant(scope.tournament, scope.account);
                };

                scope.join = function () {
                    return CardService.joinTournament(scope.tournament, element);
                };

                scope.leave = function () {
                    return CardService.leaveTournament(scope.tournament, element);
                };

                scope.enterLobby = function () {
                    return TournamentService.enterLobby(scope.tournament.id);
                };

                scope.launchDota = function () {
                    $window.location.href = 'steam://run/570';
                };
            }
        };
    }
})();
