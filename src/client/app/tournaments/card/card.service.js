(function () {
    'use strict';

    angular.module('app.tournaments')
        .factory('CardService', cardService);

    /* @ngInject */
    function cardService($intercom, translate, logger, lodash,
        TournamentService, SpinnerService, BalanceService,
        TournamentRequestService, ServerError, AbuseDetectService) {

        return {
            joinTournament: joinTournament,
            leaveTournament: leaveTournament,
            getTournamentStrStatus: getTournamentStrStatus
        };

        function getTournamentStrStatus(tournament) {
            switch (tournament.status) {
            case 0:
                return 'new';
            case 1:
                return 'ready';
            case 2:
            case 3:
                return 'pre';
            case 4:
                if (Date.now() < tournament.getInvitationPeriodEnd()) {
                    return 'invitation-period';
                }
                return 'running';
            case 5:
                break;
            default:
                return 'passed';
            }
        }

        function joinTournament(tournament, element) {
            if (AbuseDetectService.detectAbuseActivity(tournament)) {
                return logger.error(translate(
                    'You are leave this tournament many times,' +
                    ' wait {{time}} mins for register again', {
                        time: AbuseDetectService.getBanTime(tournament)
                    }
                ));
            }

            if (element) {
                SpinnerService.start(element);
            }

            return tournament
                .makePlayRequest()
                .then(function () {
                    $intercom('trackEvent', 'WR_TOUR_JOINED', {
                        id: tournament.id,
                        name: tournament.name,
                        fee: tournament.fee_amount,
                        participants: [
                            tournament.requests.length,
                            tournament.participants_count
                        ].join('/'),
                        is_cooldown: tournament.is_cooldown
                    });

                    return TournamentRequestService.fetch(tournament.id);
                })
                .then(function () {
                    SpinnerService.stop(element);
                    logger.success(
                        translate('You successfully join the tournament'), {},
                        translate('Tournament')
                    );
                })
                .then(BalanceService.fetch)
                .then(TournamentService.fetchMyTournaments)
                .catch(function (cause) {
                    logger.error(
                        ServerError.message(cause), {},
                        translate('Error')
                    );
                    SpinnerService.stop(element);
                });
        }

        function leaveTournament(tournament, element) {
            if (element) {
                SpinnerService.start(element);
            }

            return tournament
                .undoPlayRequest()
                .then(function (response) {
                    $intercom('trackEvent', 'WR_TOUR_LEFT', {
                        id: tournament.id,
                        name: tournament.name
                    });

                    var accountIds = lodash.map(response, function (r) {
                        return r.account_id;
                    });

                    lodash.remove(tournament.requests, function (request) {
                        return accountIds.indexOf(request.account_id) !== -1;
                    });

                    return TournamentRequestService.fetch(tournament.id);
                })
                .then(function () {
                    SpinnerService.stop(element);
                    logger.success(
                        translate('You successfully leave the tournament'), {},
                        translate('Tournament')
                    );
                })
                .then(BalanceService.fetch)
                .catch(function (cause) {
                    logger.error(
                        ServerError.message(cause), {}, translate('Error')
                    );
                    SpinnerService.stop(element);
                })
                .then(AbuseDetectService.setAbuseActivity(tournament));
        }

    }
})();
