(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .directive('cardTimeStatus', cardTimeStatus);

    /* @ngInject */
    function cardTimeStatus(gettextCatalog) {
        return {
            restrict: 'A',
            scope: {
                tournament: '='
            },
            link: function (scope, element) {
                var statusString = gettextCatalog.getString('By filling');

                if (!scope.tournament.isCooldown()) {
                    statusString = scope.tournament.calendarStartsAt();
                    statusString = statusString.substring(0, 6) +
                        statusString.substring(8, 16);
                }

                if (scope.tournament.isInvitationPeriod()) {
                    statusString = gettextCatalog.getString('Now');
                }

                if (scope.tournament.isRunning()) {
                    statusString = gettextCatalog.getString('Running');
                }

                element.text(statusString);

            }
        };
    }
})();
