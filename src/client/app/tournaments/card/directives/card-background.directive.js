(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .directive('cardBackground', cardBackground);

    /* @ngInject */
    function cardBackground() {
        return {
            restrict: 'A',
            scope: {
                tournament: '=',
                colorClass: '='
            },
            link: function (scope, element) {
                element.append('<div class="tournament-card-bg-overlay ' +
                    scope.colorClass + '"></div>');
                element.css('background-image', 'url(' +
                    scope.tournament.getCardBgImageUrl() + ')');
            }
        };
    }
})();
