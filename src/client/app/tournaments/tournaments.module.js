(function() {
    'use strict';

    angular
        .module('app.tournaments', [
            'app.core',
            'app.widgets'
        ]);
})();
