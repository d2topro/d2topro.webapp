(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .controller('TournamentInfoModalController', tournamentInfoModalController);

    /* @ngInject*/
    function tournamentInfoModalController($uibModalInstance, $q, $state, $window,
        $intercom, AccountService, AccountTeamService, TournamentService, tournament,
        moment, logger, gettextCatalog) {

        var vm = this;
        vm.moment = moment;
        vm.tournament = tournament;
        vm.account = null;
        vm.team = null;
        vm.isParticipantOfTournament = null;

        vm.close = close;
        vm.join = join;
        vm.leave = leave;

        activate();

        function activate() {
            return $q.all({
                    account: AccountService.getAccount(),
                    team: AccountTeamService.getTeam()
                })
                .then(function(response) {
                    vm.account = response.account;
                    vm.team = response.team;
                    vm.isParticipantOfTournament = TournamentService
                        .isParticipantOfTournament(vm.tournament, vm.account, vm.team);
                });
        }

        function close() {
            $uibModalInstance.close();
            activate();
        }

        function join() {
            return vm.tournament.makePlayRequest()
                .then(function(response) {
                    if (response.error) {
                        throw new Error(gettextCatalog.getString(response.error.message));
                    }

                    AccountService.fetch();
                    TournamentService.fetchMyTournaments();

                    var metadata = {
                        tournament_name: vm.tournament.name
                    };

                    if (vm.tournament.getPlayerFee() === 0) {
                        $intercom('trackEvent', 'join-freeroll-tournament', metadata);
                    } else {
                        $intercom('trackEvent', 'join-tournament', metadata);
                    };
                })
                .then(close)
                .catch(function(cause) {
                    logger.error(cause.message);
                });
        }

        function leave() {
            var message = (vm.tournament.isReady() && !vm.tournament.is_cooldown) ?
                gettextCatalog.getString('Are you willing to lose 50% of chargeback?') :
                gettextCatalog.getString('Are you sure?');

            if (!confirm(message)) {
                return;
            }

            return vm.tournament.undoPlayRequest()
                .then(function(response) {
                    if (response.error) {
                        throw new Error(response.error.message);
                    }

                    var metadata = {
                        tournament_name: vm.tournament.name
                    };

                    return $intercom('trackEvent', 'leave-tournament', metadata);
                })
                .then(AccountService.fetch)
                .then(close)
                .catch(function(cause) {
                    logger.error(cause.message);
                });
        }
    }
})();
