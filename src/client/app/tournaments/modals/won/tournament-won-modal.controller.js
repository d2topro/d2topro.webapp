(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .controller('TournamentWonModalController', tournamentWonModalController);

    /* @ngInject*/
    function tournamentWonModalController($scope, $q, $uibModalInstance, $state,
        $rootScope, $intercom, AccountService, data) {

        var vm = this;
        vm.account = null;
        vm.reward = data.reward ? data.reward : false;
        vm.match = data.match;
        vm.toDashboard = toDashboard;

        activate();

        function activate() {
            return $q.all({
                    account: AccountService.getAccount(),
                })
                .then(function (response) {
                    vm.account = response.account;
                });
        }

        function toDashboard() {
            $uibModalInstance.close();
            $intercom('trackEvent', 'close-won-modal');
            $state.go('root.dashboard', {});
            vm.reward ?
                $rootScope.$emit('wallet:animation', {}) :
                false;
        }
    }

})();
