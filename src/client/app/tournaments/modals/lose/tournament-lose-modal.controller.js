/**
 * Created by DTuzenkov on 4/21/16.
 */
(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .controller('TournamentLoseModalController', tournamentLoseModalController);

    /* @ngInject */
    function tournamentLoseModalController($scope, $uibModalInstance, $state,
        $rootScope, $intercom, $q, AccountService, MatchService, data) {
        var vm = this;
        vm.account = null;
        vm.toDashboard = toDashboard;
        vm.reward = data.reward ? data.reward : false;
        vm.match = data.match;

        activate();

        function activate() {
            return $q
                .all({
                    account: AccountService.getAccount(),
                })
                .then(function (response) {
                    vm.account = response.account;
                });
        }

        function toDashboard() {
            $uibModalInstance.close();
            $intercom('trackEvent', 'close-lose-modal');
            $state.go('root.dashboard', {});
            vm.reward ?
                $rootScope.$emit('wallet:animation', {}) :
                false;
        }
    }

})();
