(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .controller('TournamentAcceptModalController', controller);

    /* @ngInject */
    function controller($scope, $q, $state, $uibModalInstance, $intercom,
        lodash, TournamentRequestService, TournamentService, SocketService,
        AccountService, tournament) {

        var vm = this;
        vm.requests = [];
        vm.my = null;
        vm.isAccepted = false;
        vm.tournament = tournament;
        vm.accept = accept;
        vm.onExpire = onExpire;
        vm.getAcceptedCount = getAcceptedCount;
        vm.tournament.starts_at = new Date(tournament.starts_at);

        activate();

        SocketService.on('tm:request:update', update);

        function activate() {
            return $q
                .all({
                    account: AccountService.getAccount(),
                    requests: TournamentRequestService.fetch(tournament.id)
                })
                .then(function (result) {
                    vm.requests = result.requests;
                    vm.isAccepted = !!lodash.find(
                        result.requests,
                        function (r) {
                            return r.account_id === result.account.id &&
                                r.is_approved;
                        }
                    );
                });
        }

        function update(data) {
            var request = lodash.find(vm.requests, function (r) {
                return r.account.id === data.account.id;
            });

            if (!request) {
                return;
            }

            request.is_approved = true;

            if (vm.requests.length === getAcceptedCount()) {
                currentMatch();
            }
        }

        function getAcceptedCount() {
            return lodash.filter(vm.requests, function (r) {
                    return r.is_approved;
                })
                .length;
        }

        function accept() {
            return TournamentRequestService
                .getAccountRequest(tournament.id)
                .then(function (request) {
                    return request.$approve({ id: tournament.id });
                })
                .then(function () {
                    vm.isAccepted = true;
                });
        }

        function currentMatch() {
            return $state.go('root.tournaments.details', { id: tournament.id });
        }

        function onExpire() {
            return $q.all([
                    TournamentRequestService.fetch(tournament.id),
                    // TournamentService.fetch()
                ])
                .then(function () {
                    return $uibModalInstance.close();
                });
        }

        $scope.$on('destroy', function () {
            SocketService.off('tm:request:update', update);
        });
    }

})();
