(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .factory('TournamentsModalService', service);

    /* @ngInject */
    function service($uibModal, TournamentService) {

        return {
            showTournamentRequestsModal: showTournamentRequestsModal
        };

        function showTournamentRequestsModal(tournamentId) {
            return $uibModal
                .open({
                    animation: true,
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'app/tournaments/modals/accept/accept.html',
                    controller: 'TournamentAcceptModalController',
                    controllerAs: 'vm',
                    size: 'sm modal-center width-400',
                    backdropClass: 'BACKDROP_CLASS',
                    windowTopClass: '',
                    resolve: {
                        tournament: function () {
                            return TournamentService
                                .getTournament(tournamentId);
                        }
                    }
                })
                .result;
        }
    }
})();
