(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .factory('TeamNameService', teamNameService);

    /* @ngInject */
    function teamNameService() {
        var names = [
            "Alicorns",
            "Banshees",
            "Basilisks",
            "Bigfoots",
            "Black Dogs",
            "Black Eyed Beings",
            "Bogeymans",
            "Bogles",
            "Bray Road Beasts",
            "Brownies",
            "Centaurs",
            "Cerberus",
            "Charybdises",
            "Chimeras",
            "Cockatrices",
            "Cyclopss",
            "Cynocephalus",
            "Demons",
            "Doppelgangers",
            "Dragons",
            "Dwarfs",
            "Echidnas",
            "Elfs",
            "Fairys",
            "Ghosts",
            "Gnomes",
            "Goblins",
            "Golems",
            "Gorgons",
            "Griffins",
            "Grim Reapers",
            "Hobgoblins",
            "Hydras",
            "Imps",
            "Ladons",
            "Leprechauns",
            "Loch Ness",
            "Manticores",
            "Medusas",
            "Mermaidss",
            "Minotaurs",
            "Mothmans",
            "Mutants",
            "Nemean Lions",
            "New Jersey Devils",
            "Nymphs",
            "Ogres",
            "Orthros",
            "Pegasus",
            "Phoenixes",
            "Pixies",
            "Sasquatches",
            "Satyrs",
            "Scyllas",
            "Sea Monsterss",
            "Sea-Goats",
            "Shades",
            "Shapeshifterss",
            "Sirenss",
            "Sphinxs",
            "Sprites",
            "Sylphs",
            "Thunderbirds",
            "Typhons",
            "Unicorns"
        ];

        return {
            names: names
        }
    }
})();
