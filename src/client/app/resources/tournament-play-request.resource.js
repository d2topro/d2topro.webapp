(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('TournamentPlayRequestResource',
            tournamentPlayRequestResource
        );

    /* @ngInject */
    function tournamentPlayRequestResource($resource, AccountResource) {
        var resource = $resource('/api/tournaments/:id/requests', {
            id: '@id'
        }, {
            get: {
                method: 'GET',
                isArray: true,
                transformResponse: function (data) {
                    var requests = angular.fromJson(data);
                    angular.forEach(requests, function (request) {
                        request.account = new AccountResource(request.account);
                    });

                    return requests;
                }
            },
            approve: {
                method: 'PUT'
            },
            create: {
                method: 'POST',
                isArray: true
            },
            delete: {
                method: 'DELETE',
                isArray: true
            }
        });

        resource.prototype.getGroup = function () {
            return this.group_id;
        };

        return resource;
    }
})();
