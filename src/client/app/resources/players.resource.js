/**
 * Created by DTuzenkov on 12/1/15.
 */
(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('PlayerResource', playerResource);

    /* @ngInject */
    function playerResource($resource) {
        var resource = $resource('/api/players', {}, {
            search: {method: 'POST', isArray: true}
        });

        //resource.prototype.account_id
        return resource;
    }
})();
