/**
 * Created by DTuzenkov on 12/1/15.
 */
(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('AccountResource', account);

    /* @ngInject */
    function account($resource, lodash) {
        var resource = $resource('/api/account', {}, {
            update: {
                method: 'PUT'
            },
            toggleMode: {
                method: 'PUT',
                url: '/api/account/play-mode'
            },
            acceptTermsConds: {
                method: 'PUT',
                url: '/api/account/accept-terms-conds'
            },
            firstStart: {
                method: 'PUT',
                url: '/api/account/first-start'
            },
            getToken: {
                method: 'GET',
                url: '/api/account/token'
            }
        });

        resource.prototype.playSoloMode = function () {
            return this.togglePlayMode(true);
        };

        resource.prototype.playTeamMode = function () {
            return this.togglePlayMode(false);
        };

        resource.prototype.togglePlayMode = function (isSolo) {
            var resource = this;
            resource.is_solo = isSolo;

            return resource.$update();
        };

        resource.prototype.getName = function () {
            return this.steam_personal_name;
        };

        resource.prototype.getWinrate = function (gameAppId) {
            gameAppId = gameAppId || 1;
            var stat = lodash.find(this.statistics, function (stat) {
                return stat.game_app_id === gameAppId;
            });
            var total = stat.wins_count + stat.loses_count + stat.leave_count;

            return total > 0 ? (stat.wins_count / total) * 100 : 0;
        };

        resource.prototype.getLeaverate = function (gameAppId) {
            gameAppId = gameAppId || 1;
            var stat = lodash.find(this.statistics, function (stat) {
                return stat.game_app_id === gameAppId;
            });
            var total = stat.wins_count + stat.loses_count + stat.leave_count;

            return total > 0 ? (stat.leave_count / total) * 100 : 0;
        };


        return resource;
    }
})();
