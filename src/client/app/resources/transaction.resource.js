(function() {
    'use strict';

    angular
        .module('app.resources')
        .factory('TransactionResource', transactionResource);

    /* @ngInject */
    function transactionResource($resource, moment, lodash, gettextCatalog) {

        var resource = $resource('/api/account/wallet/transactions', {}, {
            get: {
                method: 'GET',
                isArray: true
            }
        });

        var CURRENCY = {
            WRC: 1,
            USD: 2
        };

        resource.prototype.isUSD = function() {
            return this.currency_id === CURRENCY.USD;
        };

        resource.prototype.isWRC = function() {
            return this.currency_id === CURRENCY.WRC;
        };

        resource.prototype.isIn = function() {
            var _in = [1, 4, 5];
            return lodash.includes(_in, this.action);
        };

        resource.prototype.isOut = function() {
            var out = [2, 3];
            return lodash.includes(out, this.action);
        };

        resource.prototype.getCalendarDate = function() {
            var date = new Date(this.created_at);
            var today = new Date();
            var offset = Math.round(
                (today.getDate() - date.getDate()) % (24 * 3600 * 1000)
            );

            switch (offset) {
                case 0:
                    return gettextCatalog.getString('Today');
                case 1:
                    return gettextCatalog.getString('Yesterday');
                default:
                    return moment(this.created_at)
                        .format('LL');
            }
        };



        resource.prototype.getTime = function() {
            return moment(this.created_at)
                .format('LT');
        };

        return resource;
    }
})();
