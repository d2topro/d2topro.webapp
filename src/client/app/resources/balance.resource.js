(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('BalanceResource', balanceResource);

    /* @ngInject */
    function balanceResource($resource) {

        var resource = $resource('/api/account/wallet/balance', {}, {
            get: {
                method: 'GET',
                isArray: true
            }
        });

        var CURRENCY = {
            WRC: 1,
            USD: 2
        };

        resource.prototype.isUSD = function () {
            return this.currency_id === CURRENCY.USD;
        };

        resource.prototype.isWRC = function () {
            return this.currency_id === CURRENCY.WRC;
        };

        return resource;
    }
})();
