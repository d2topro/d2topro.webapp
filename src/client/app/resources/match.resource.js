(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('MatchResource', matchResource);

    /* @ngInject */
    function matchResource($resource, lodash) {
        var resource = $resource('/api/tournaments/:id/matches', {
            id: '@id'
        }, {
            get: {
                method: 'GET',
                isArray: true
            },
            accountCurrentMatches: {
                method: 'GET',
                isArray: true,
                url: '/api/account/matches/current'
            },
            accountLastMatches: {
                method: 'GET',
                isArray: true,
                url: '/api/account/matches/last'
            }
        });

        // Match statuses from back end

        var STATUS = {
            NEW: 0,
            PRE_START: 1,
            IN_PROGRESS: 2,
            COMPLETED: 3,
            TECH_LOSE: 4,
            EXPIRED: 5
        };

        resource.prototype.OUTCOME = {
            UNKNOWN: 0,
            WON: 1,
            LOSE: 2,
            TECH_LOSE: 3
        };

        resource.prototype.isInvitationPeriod = function () {
            return this.status === STATUS.NEW ||
                this.status === STATUS.PRE_START;
        }

        resource.prototype.isRunning = function () {
            return this.status === STATUS.IN_PROGRESS;
        }

        resource.prototype.isAwaiting = function () {
            return this.status === STATUS.COMPLETED ||
                this.status === STATUS.TECH_LOSE ||
                this.status === STATUS.EXPIRED;
        }

        resource.prototype.getInvitationPeriodEnd = function () {
            return Date.parse(new Date(this.lobby_expired_at));
        }

        resource.prototype.getOutcome = function () {
            return this.outcome;
        }

        resource.prototype.getGroups = function () {
            return this.groups;
        }

        resource.prototype.getTournamentId = function () {
            return this.tournament_id;
        }

        return resource;
    }
})();
