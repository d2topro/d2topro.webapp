(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('CurrencyResource', resource);

    /* @ngInject */
    function resource($resource) {
        var resource = $resource('/api/currencies', {}, {});
        return resource;
    }
})();
