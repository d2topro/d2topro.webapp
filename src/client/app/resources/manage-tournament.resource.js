(function () {
	'use strict';

	angular
		.module('app.resources')
		.factory('ManageTournamentResource', resource);

	/* @ngInject */
	function resource($resource) {
		var resource = $resource('/api/manage/tournaments/:id', {}, {
			query: {
				method: 'GET',
				isArray: true
			}
		});

		return resource;
	}
})();
