/**
 * Created by DTuzenkov on 12/1/15.
 */
(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('AccountTeamResource', accountTeamResource);

    /* @ngInject */
    function accountTeamResource($resource, lodash,
        TeamResourceResponseTransformer) {
        var resource = $resource('/api/account/teams/:id/', {
            id: '@id'
        }, {
            query: {
                method: 'GET',
                isArray: true,
                transformResponse: [TeamResourceResponseTransformer.transform]
            },
            get: {
                method: 'GET',
                isArray: true,
                transformResponse: [TeamResourceResponseTransformer.transform]
            },
            create: {
                method: 'POST',
                transformResponse: [TeamResourceResponseTransformer.transform]
            },
            update: {
                method: 'PUT',
                transformResponse: [TeamResourceResponseTransformer.transform]
            },
            delete: {
                method: 'DELETE',
                transformResponse: [TeamResourceResponseTransformer.transform]
            }
        });

        resource.prototype.getWinrate = function () {
            return this.win_rate;
        };

        resource.prototype.getActivePlayers = function () {
            return lodash.filter(this.members, function (m) {
                return m.TeamPlayer.isActivePlayer();
            });
        };

        resource.prototype.getBenchPlayers = function () {
            return lodash.filter(this.members, function (m) {
                return m.TeamPlayer.isBenchPlayer();
            });
        };

        resource.prototype.getCaptain = function () {
            return lodash.find(this.members, function (m) {
                return m.TeamPlayer.isCaptain();
            });
        };

        return resource;
    }
})();
