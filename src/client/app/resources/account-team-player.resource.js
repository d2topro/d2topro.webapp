/**
 * Created by DTuzenkov on 12/1/15.
 */
(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('AccountTeamPlayerResource', resource);

    /* @ngInject */
    function resource($resource) {
        var STATUS = {
            PENDING_TO_TEAM: 0,
            PENDING_TO_PLAYER: 1,
            ACTIVE: 2,
            DELETED: 3
        };
        var ROLE = {
            BENCH_PLAYER: 0,
            TEAM_PLAYER: 1,
            TEAM_CAPTAIN: 2
        };

        var resource = $resource('/api/teams/:id/players/:playerid', {
            id: '@team_id',
            playerid: '@account_id'
        }, {
            join: {
                method: 'POST',
                url: '/api/teams/:id/player/'
            },
            leave: {
                method: 'DELETE',
                url: '/api/teams/:id/player/',
                params: {
                    id: '@id',
                }
            },
            update: { method: 'PUT' },
            expel: { method: 'DELETE' },
        });

        // resource.prototype.togglePlayerRole = function () {
        //     this.role = (this.role === ROLE.TEAM_PLAYER) ? ROLE.BENCH_PLAYER : ROLE.TEAM_PLAYER;
        //     return this.$update();
        // };

        resource.prototype.isCaptain = function () {
            return this.role === ROLE.TEAM_CAPTAIN;
        };

        resource.prototype.isActivePlayer = function () {
            return this.role === ROLE.TEAM_PLAYER;
        };

        resource.prototype.isBenchPlayer = function () {
            return this.role === ROLE.BENCH_PLAYER;
        };

        resource.prototype.isActive = function () {
            return this.status === STATUS.ACTIVE;
        };

        return resource;
    }
})();
