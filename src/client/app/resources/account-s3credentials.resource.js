(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('AccountS3CredentialsResource', resource);

    /* @ngInject */
    function resource($resource) {
        var resource = $resource('/api/account/s3-credentials', {}, {});
        return resource;
    }
})();
