(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('AccountNotification', accountNotification);

    /* @ngInject */
    function accountNotification($resource, lodash) {
        var resource = $resource('/api/account/notifications/:id',
            {
                id: '@id'
            },
            {
                read: {
                    method: 'PUT'
                }

            });

        var TYPE = {
            COMMON_NOOP: 1,
            // When account send request to play in team and team captain accept or reject
            TEAM_ACCEPT_INVITATION: 100,
            TEAM_REJECT_INVITATION: 101,
            // Team members
            // When team invites an account to be a member of a team
            TEAM_MEMBER_ACCEPT_INVITATION: 103,
            TEAM_MEMBER_REJECT_INVITATION: 104,
            // When account send request to play in team
            TEAM_INVITATION: 105,
            TEAM_MEMBER_CANCEL_INVITATION: 106,
            // Member has left a team
            TEAM_MEMBER_LEFT: 107,
            // Tournaments
            TOURNAMENT_SPAWN: 200,
            TOURNAMENT_STARTED: 201,
            TOURNAMENT_RECEIVE_REWARD: 203,
            TOURNAMENT_WON: 204,
            TOURNAMENT_LOSE: 205,
            // Payments
            PAYMENT_ADD_FUNDS: 300,
            PAYMENT_WITHDRAW: 301
        };
        var inverted = lodash.invert(TYPE);

        resource.prototype.type = TYPE;
        resource.prototype.getTypeName = function () {
            return !inverted[this.type] ? this.type : lodash.capitalize(inverted[this.type].toLowerCase());
        };

        return resource;
    }
})();
