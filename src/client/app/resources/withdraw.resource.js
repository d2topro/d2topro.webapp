(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('WalletWithdrawResource', withdrawResource);

    /* @ngInject */
    function withdrawResource($resource) {
        var resource = $resource('/api/account/wallet/withdraw', {}, {
            get: {
                method: 'GET',
                isArray: false
            },
            withdraw: {
                method: 'POST',
                isArray: false
            },
            preWithdraw: {
                url: '/api/account/wallet/pre-withdraw',
                method: 'POST',
                isArray: false
            },
        });

        return resource;
    }
})();
