(function() {
    'use strict';

    angular
        .module('app.resources')
        .factory('TournamentResource', tournamentResource);

    /* @ngInject */
    function tournamentResource($resource, lodash, moment,
        TournamentPlayRequestResource) {

        // Tournament statuses from back end
        var NEW = 0,
            READY = 1,
            PREPARED = 2,
            PRESTARTED = 3,
            STARTED = 4,
            PASSED = 5,
            FAILED = 6;

        var resource = $resource('/api/tournaments/:id/', {
            id: '@id'
        }, {
            get: {
                method: 'GET'
            },
            getAccountTournaments: {
                method: 'GET',
                url: '/api/account/tournaments',
                isArray: true
            }
        });

        resource.prototype.isNew = function() {
            return this.status === NEW;
        };

        resource.prototype.isReady = function() {
            return this.status === READY;
        };

        resource.prototype.isPreConditions = function() {
            return this.status === (PREPARED || PRESTARTED);
        };

        resource.prototype.isStarted = function() {
            return this.status === STARTED;
        };

        resource.prototype.isPremium = function() {
            return this.is_premium === true;
        };

        resource.prototype.isInvitationPeriod = function() {
            return this.status === STARTED &&
                Date.now() < this.getInvitationPeriodEnd();
        };

        resource.prototype.isRunning = function() {
            return this.status === STARTED &&
                Date.now() >= this.getInvitationPeriodEnd();
        };

        resource.prototype.isPassed = function() {
            return this.status === PASSED;
        };

        resource.prototype.getInvitationPeriodEnd = function() {
            var delay = this.policy.lobby_expiration;
            return Date.parse(new Date(this.starts_at)) + delay * 1000;
        };

        resource.prototype.isSolo = function() {
            return this.is_solo;
        };

        resource.prototype.isCooldown = function() {
            return this.is_cooldown;
        };

        resource.prototype.isFailed = function() {
            return this.status === FAILED;
        };

        resource.prototype.isTechLosePeriod = function() {
            return Date.now() >= Date.parse(new Date(this.starts_at)) &&
                Date.now() < (new Date(this.starts_at))
                .valueOf() + this.policy.lobby_expiration_delay * 1000;
        };

        resource.prototype.getCardBgImageUrl = function() {
            return this.image.small;
        };

        resource.prototype.getLobbyBgImageUrl = function() {
            return this.image.large;
        };

        resource.prototype.getAverageDuration = function() {
            return this.predictable_average_duration;
        };

        resource.prototype.getMatchesCount = function() {
            return this.predictable_matches_count;
        };

        resource.prototype.getFreeSlotsCount = function() {
            return this.participants_count - this.requests.length;
        };

        resource.prototype.participants = function() {
            var total = this.participants_count /
                (this.is_solo ? 1 : this.policy.max_players_count);
            var current = this.requests.length /
                (this.is_solo ? 1 : this.policy.max_players_count);

            return {
                total: total,
                current: current
            };
        };
        resource.prototype.getPlayerFee = function() {
            return this.fee_amount;
        };

        resource.prototype.makePlayRequest = function() {
            return TournamentPlayRequestResource.create({
                    id: this.id
                })
                .$promise;
        };

        resource.prototype.undoPlayRequest = function() {
            return TournamentPlayRequestResource.delete({
                    id: this.id
                })
                .$promise;
        };

        resource.prototype.calendarStartsAt = function() {
            return moment(this.starts_at)
                .format('L LT');
        };

        resource.prototype.getPlaceRewards = function() {
            return this.is_cooldown ?
                this.predictable_place_reward_amount :
                this.rollup_reward_place_amount;
        };

        // ===================

        resource.prototype.getPrizePoolAmount = function() {
            return this.is_cooldown ?
                this.predictable_reward_amount :
                this.rollup_reward_pool_amount;
        };

        return resource;
    }
})();
