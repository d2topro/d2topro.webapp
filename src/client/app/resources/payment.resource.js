(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('PaymentResource', paymentResource);

    /* @ngInject */
    function paymentResource($resource) {
        return $resource('/api/account/payments', {}, {
            withdrawMoney: {
                method: 'POST',
                isArray: true
            },
            virtualCurrencyPackages: {
                method: 'GET',
                url: '/api/account/payments/packages',
                isArray: true
            },
            buyVirtualCurrency: {
                method: 'POST',
                url: '/api/account/payments/invoice/payeer'
            }
        });
    }
})();
