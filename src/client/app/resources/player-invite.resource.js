/**
 * Created by DTuzenkov on 12/1/15.
 */
(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('PlayerInviteResource', playerInviteResource);

    /* @ngInject */
    function playerInviteResource($resource) {
        return $resource('/api/players/invite', {}, {
            invite: {method: 'POST'}
        });
    }
})();
