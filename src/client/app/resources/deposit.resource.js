(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('WalletDepositResource', depositResource);

    /* @ngInject */
    function depositResource($resource) {

        var resource = $resource('/api/account/wallet/deposit', {}, {
            get: {
                method: 'GET',
                isArray: false
            },
            post: {
                method: 'POST',
                isArray: false
            },
            payGateway: {
                url: '/api/account/wallet/pay-page',
                method: 'POST',
                isArray: false
            }
        });

        return resource;
    }
})();
