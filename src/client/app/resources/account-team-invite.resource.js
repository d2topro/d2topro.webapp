/**
 * Created by DTuzenkov on 12/1/15.
 */
(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('AccountTeamInviteResource', accountTeamInviteResource);

    /* @ngInject */
    function accountTeamInviteResource($resource) {
        return $resource('/api/account/team/invites/:id', {id:'@id'}, {
            invite: {method: 'POST'},
            accept: {method: 'PUT'}
        });
    }
})();
