(function() {
    'use strict';

    angular
        .module('app.resources')
        .factory('GameResource', gameResource);

    /* @ngInject */
    function gameResource($resource, lodash) {
        var resource = $resource('/api/account/matches', {}, {
            getCurrentGames: {
                method: 'GET',
                isArray: true
            }
        })

        var GameStatusEnum = {
            NEW: 0,
            PRE_START: 1,
            IN_PROGRESS: 2,
            COMPLETED: 3
        };

        resource.prototype.isNew = function() {
            return this.status === GameStatusEnum.NEW;
        };

        resource.prototype.isPreStart = function() {
            return this.status === GameStatusEnum.PRE_START;
        };

        resource.prototype.isInProgress = function() {
            return this.status === GameStatusEnum.IN_PROGRESS;
        };

        resource.prototype.isCompleted = function() {
            return this.status === GameStatusEnum.COMPLETED
        };

        resource.prototype.isTournamentRunning = function(myActiveTournaments) {
            var currentGame = this;
            return lodash.find(myActiveTournaments, function(tournament) {
                return currentGame.tournament_id === tournament.id;
            })
        };

        resource.prototype.startedAt = function() {
            return this.started_at ? Date.parse(new Date(this.started_at)) : null
        };

        resource.prototype.isInvitationPeriod = function() {
            return Date.now() < this.startedAt();
        }

        return resource;
    }
})();
