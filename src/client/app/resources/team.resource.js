(function () {
    'use strict';

    angular
        .module('app.resources')
        .factory('TeamResource', team);

    /* @ngInject */
    function team($resource, TeamResourceResponseTransformer) {
        return $resource('/api/teams/:id/', {
            id: '@id'
        }, {
            query: {
                method: 'GET',
                isArray: true,
                transformResponse: [TeamResourceResponseTransformer.transform]
            },
            get: {
                method: 'GET',
                isArray: false,
                transformResponse: [TeamResourceResponseTransformer.transform]
            }
        });
    }
})();
