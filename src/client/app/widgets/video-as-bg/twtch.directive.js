(function() {
    'use strict';

    angular
        .module('app.widgets')
        .directive('twitchDirective', twitchDirective);

    /* @ngInject */
    function twitchDirective() {
        return {
            restrict: 'A',
            templateUrl: 'app/widgets/video-as-bg/template.html',
            link: function(scope, element, attr) {
                var headerElement = angular.element('.page-header'),
                    height = headerElement.width() / 1.78,
                    width = angular.element('.video-player')
                    .width(),
                    expandDescrEl = angular.element('.expand-player'),
                    turnDescEl = angular.element('.turn-player'),
                    hidebleEl = angular.element('.hideble'),
                    wrapEl = angular.element('.player-wrapper');

                scope.resize = function() {
                    scope.twitchPlayer.getMuted() ? expandPlayer() : turnPlayer();
                };

                function expandPlayer() {
                    headerElement.css('height', height);
                    expandDescrEl.css('display', 'none');
                    hidebleEl.css('display', 'none');
                    turnDescEl.css('display', 'block');
                    wrapEl.css('opacity', '0');
                    scope.twitchPlayer.setMuted(false);
                }

                function turnPlayer() {
                    headerElement.css('height', '250px');
                    expandDescrEl.css('display', 'block');
                    hidebleEl.css('display', 'block');
                    turnDescEl.css('display', 'none');
                    wrapEl.css('opacity', '0.6');
                    scope.twitchPlayer.setMuted(true);
                }

                var options = {
                    width: width,
                    height: width / 1.78,
                    controls: false,
                    muted: true
                };

                if (attr.isStream === 'true') {
                    options.channel = attr.videoId;
                } else {
                    options.video = attr.videoId;
                }

                scope.twitchPlayer = new Twitch.Player('player', options);

                scope.twitchPlayer.addEventListener(Twitch.Player.PAUSE,
                    function() {
                        scope.twitchPlayer.play();
                    });
            }
        };
    }


})();
