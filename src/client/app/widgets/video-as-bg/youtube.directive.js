(function() {
    'use strict';

    angular
        .module('app.widgets')
        .directive('youtubeDirective', youtubeDirective);

    /* @ngInject */
    function youtubeDirective() {
        return {
            restrict: 'A',
            templateUrl: 'app/widgets/video-as-bg/template.html',
            link: function(scope, element, attr) {
                var headerElement = angular.element('.page-header'),
                    height = headerElement.width() / 1.78,
                    width = angular.element('.video-player')
                    .width(),
                    expandDescrEl = angular.element('.expand-player'),
                    turnDescEl = angular.element('.turn-player'),
                    hidebleEl = angular.element('.hideble'),
                    wrapEl = angular.element('.player-wrapper');


                scope.resize = function() {
                    scope.player.isMuted() ? expandPlayer() : turnPlayer();
                };

                function expandPlayer() {
                    headerElement.css('height', height);
                    expandDescrEl.css('display', 'none');
                    hidebleEl.css('display', 'none');
                    turnDescEl.css('display', 'block');
                    wrapEl.css('opacity', '0');
                    scope.player.unMute();
                }

                function turnPlayer() {
                    headerElement.css('height', '250px');
                    expandDescrEl.css('display', 'block');
                    hidebleEl.css('display', 'block');
                    turnDescEl.css('display', 'none');
                    wrapEl.css('opacity', '0.6');
                    scope.player.mute();
                }

                scope.player = new YT.Player('player', {
                    width: width,
                    height: width / 1.78,
                    playerVars: {
                        'rel': 0,
                        'controls': 0,
                        'showinfo': 0,
                        'iv_load_policy': 3,
                        'fs': 0,
                        // 'loop': 1, //DON'T WORK
                        'modestbranding': 1,
                        'disablekb': 1
                    },
                    videoId: attr.videoId,
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onStateChange
                    }
                });

                function onPlayerReady() {
                    scope.player.mute();
                    scope.player.setPlaybackQuality('highres');
                    scope.player.playVideo();
                }

                function onStateChange(state) {
                    if (state.data === YT.PlayerState.ENDED) {
                        scope.player.playVideo();
                    }
                }
            }
        };
    }

})();
