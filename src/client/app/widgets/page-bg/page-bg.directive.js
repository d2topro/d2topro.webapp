(function() {
    'use strict';

    angular
        .module('app.widgets')
        .directive('pageBg', pageBg);

    /* @ngInject */
    function pageBg(environment) {
        return {
            restrict: 'A',
            scope: {
                page: '='
            },
            link: function(scope, element) {
                var imgUrl = environment.assetsUrl +
                    '/miscellaneous/pages-bg/' + scope.page + '-bg.jpg';

                element.append('<div class="page-bg-overlay"></div>');
                // element.append('<div class="page-bg-bottom"></div>');

                element.css('background-image', 'url(' + imgUrl + ')');
            }
        }
    }
})();
