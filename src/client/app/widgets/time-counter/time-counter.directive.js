(function() {
    'use strict';

    angular
        .module('app.widgets')
        .directive('timeCounter', timeCounter)
        .factory('Convert', convert);

    /* @ngInject */
    function timeCounter(Convert) {
        return {
            restrict: 'A',
            scope: {
                hours: '='
            },
            link: function(scope, element) {
                return element.text(Convert.decimalHoursToTime(scope.hours));
            }
        };
    }

    function convert(gettextCatalog) {
        return {
            decimalHoursToTime: function(hours) {
                var h = Math.floor(hours);

                var reminder = hours % 1;
                var m = Math.floor(reminder * 60);

                var totalHours = h !== 0 ? h + ' ' + gettextCatalog.getPlural(h, 'hour', 'hours') : '';
                var totalMinutes = m !== 0 ? m + ' ' + gettextCatalog.getPlural(m, 'minute', 'minutes') : '';

                return totalHours + ' ' + totalMinutes;
            }
        };
    }
})();
