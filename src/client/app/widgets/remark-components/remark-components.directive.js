(function () {
    'use strict';


    angular
        .module('app.widgets')
        .directive('remarkComponentDirective', remarkComponentDirective);

    /* @ngInject */
    function remarkComponentDirective($window) {
        return {
            restrict: 'AE',
            link: function (scope, elem, attrs) {
                var component = $window.$.components.get(attrs.component);
                if (!component) {
                    return;
                }

                if (component.api) {
                    return component.api();
                } else if (component.init) {
                    return component.init();
                }

                return;
            }
        };
    }

})();
