(function() {
    'use strict';

    angular
        .module('app.widgets')
        .directive('languageSwitcher', languageSwitcher);

    /* @ngInject */
    function languageSwitcher($rootScope, $cookies) {
        return {
            restrict: 'A',
            templateUrl: 'app/widgets/language-switcher/language-switcher.html',
            link: function(scope) {
                scope.language = $cookies.get('customLang');

                scope.switchTo = function(language) {
                    scope.language = language;
                    $rootScope.customLang = language;
                    return language;
                };
            }
        };
    }
})();
