(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('countdownTimer', countdownTimer)
        .factory('Timer', timer);

    /* @ngInject */
    function countdownTimer($interval, Timer) {
        return {
            restrict: 'A',
            scope: {
                date: '=',
                callback: '&'
            },
            link: function (scope, element) {
                var interval = $interval(function () {
                    if (Date.now() < scope.date) {
                        var diff = Math.floor((scope.date - Date.now()) / 1000);

                        return element.text(Timer.ms(diff));
                    } else {
                        scope.callback();
                        $interval.cancel(interval);
                    }
                }, 1000);
            }
        };
    }

    function timer() {
        return {
            ms: function (t) {
                var minutes = Math.floor(t / 60) % 60;
                t -= minutes * 60;

                var seconds = ('0' + t % 60)
                    .slice(-2);

                return [minutes, seconds].join(':');
            }
        };
    }
})();
