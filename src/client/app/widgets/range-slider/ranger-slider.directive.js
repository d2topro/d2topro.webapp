(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('rangeSliderDirective', directive);

    function directive($window, $timeout) {
        return {
            require: 'ngModel',
            restrict: 'AE',
            // transclude: true,
            scope: {
                value: '=ngModel',
                step: '=',
                limit: '=',
                range: '='
            },
            // template: '<div class="asRange" data-plugin="asRange" data-namespace="angular-rangeUi" ></div>',
            link: link
        };

        function link(scope, elem, attrs, ngModel) {
            function initialize() {
                $timeout(function () {
                    return $window.$.components.init('asRange', elem, {
                        step: scope.step,
                        limit: !!scope.limit,
                        range: !!scope.range,
                        value: scope.value,
                        min: scope.limit[0],
                        max: scope.limit[1],
                        tip: true,
                        format: function (value) {
                            return '$' + value;
                        },
                        onChange: function (value) {
                            scope.value = value;
                            scope.$apply(function () {
                                ngModel.$setViewValue(value);
                            });
                        }
                    });
                }, 0);
            }

            return initialize();
        }
    }
})();
