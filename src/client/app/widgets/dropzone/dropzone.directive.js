(function() {
    'use strict';

    angular.module('app.widgets')
        .directive('dropzoneDirective', directive);

    /* @ngInject */
    function directive(gettextCatalog, AccountTeamService) {
        return {
            restrict: 'A',
            scope: {
                onFileSelect: '&',
                defaultFileUrl: '='
            },
            link: function(scope, element, attributes) {
                element.dropify({
                    // TODO use attributes instead of team logo
                    height: 250,
                    minWidth: 100,
                    maxWidth: 1000,
                    minHeight: 100,
                    maxHeight: 1000,
                    maxFileSize: '0.1M',
                    imgFileExtensions: [
                        'png', 'jpg', 'jpeg', 'gif', 'bmp'
                    ],
                    allowedFormats: [
                        'portrait', 'square', 'landscape'
                    ],
                    allowedFileExtensions: [
                        'png', 'jpg', 'jpeg', 'gif', 'bmp'
                    ],
                    defaultFile: scope.defaultFileUrl,
                    messages: {
                        'default': gettextCatalog.getString('Drag and drop a team logo here or click. <br> Max size - 100KB'),
                        'replace': gettextCatalog.getString('Drag and drop or click to replace'),
                        'remove': gettextCatalog.getString('Remove')
                    },
                    error: {
                        'fileSize': gettextCatalog.getString('The file size is too big ({{ value }} max).'),
                        'minWidth': gettextCatalog.getString('The image width is too small ({{ value }}}px min).'),
                        'maxWidth': gettextCatalog.getString('The image width is too big ({{ value }}}px max).'),
                        'minHeight': gettextCatalog.getString('The image height is too small ({{ value }}}px min).'),
                        'maxHeight': gettextCatalog.getString('The image height is too big ({{ value }}px max).'),
                        'imageFormat': gettextCatalog.getString('The image format is not allowed ({{ value }} only).')
                    }
                });

                element.bind('change', function(e) {
                    var file = (e.srcElement || e.target)
                        .files[0];
                    scope.onFileSelect({
                        file: file
                    });
                });
            }
        }
    }
})();
