/**
 * Created by DTuzenkov on 12/9/15.
 */
(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardTeamStatsController', dashboardTeamStatsController);

    /* @ngInject */
    function dashboardTeamStatsController(AccountTeamService) {
        var vm = this;
        vm.team = null;
        activate();

        function activate() {
            return AccountTeamService.getTeam().then(function (team) {
                vm.team = team;
                return team;
            });
        }
    }

})();
