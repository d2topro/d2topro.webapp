(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('Dashboard', controller);

    function controller($q, $scope, $intercom, lodash, AccountService,
        AccountTeamService, AccountTeamModalService) {

        var vm = this;
        var accountListener, teamListener, debounceOptions = {
            leading: true,
            trailing: false
        };
        vm.isReady = false;
        vm.team = null;
        vm.account = null;

        vm.createTeam = lodash.debounce(createTeam, 500, debounceOptions);
        vm.setSoloMode = lodash.debounce(setSoloMode, 500, debounceOptions);
        vm.setTeamMode = lodash.debounce(setTeamMode, 500, debounceOptions);
        vm.isSoloModeEnabled = isSoloModeEnabled;

        activate();

        $scope.$on('destroy', function () {
            teamListener();
            accountListener();
        });

        teamListener = $scope.$watch(
            function () {
                return AccountTeamService.getTeam();
            },
            function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                return newValue.then(function (team) {
                    vm.team = team;
                    activate();
                });
            }
        );

        accountListener = $scope.$watch(
            function () {
                return AccountService.getAccount();
            },
            function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                return newValue.then(function (account) {
                    vm.account = account;
                    activate();
                });
            }
        );

        function activate() {
            return $q
                .all({
                    account: AccountService.getAccount(),
                    team: AccountTeamService.getTeam()
                })
                .then(function (response) {
                    vm.account = response.account;
                    vm.team = response.team;
                    vm.isReady = true;
                });
        }

        function isSoloModeEnabled() {
            return vm.account.is_solo;
        }

        function createTeam() {
            $intercom('trackEvent', 'WR_CREATE_TEAM_CLICKED');
            return AccountTeamModalService.showAccountTeamCreateModal();
        }

        function setSoloMode() {
            if (vm.account.is_solo) {
                return;
            }
            $intercom('trackEvent', 'WR_SOLO_MODE_CLICKED');
            vm.account.is_solo = true;
            return vm.account.$update();
        }

        function setTeamMode() {
            if (!vm.account.is_solo) {
                return;
            }
            $intercom('trackEvent', 'WR_TEAM_MODE_CLICKED');
            vm.account.is_solo = false;
            return vm.account.$update();
        }
    }
})();
