(function () {
	'use strict';

	angular
		.module('app.dashboard')
		.controller('TournamentListController', controller);

	/* @ngInject */
	function controller($scope, $q, $log, lodash, logger, AccountService,
		TournamentService, SocketService, gettextCatalog, localStorageService) {

		var vm = this;
		vm.tournaments = [];
		vm.account = null;
		vm.filterIsNew = filterIsNew;

		vm.isNewTournaments = (localStorageService.get('isNewTournaments')) ?
			localStorageService.get('isNewTournaments') : false;

		activate();

		var tournamentsListener = $scope.$watch(
			function () {
				return TournamentService.getTournaments();
			},
			function (newValue, oldValue) {
				if (newValue !== oldValue) {
					newValue.then(function (tournaments) {
						vm.tournaments = TournamentService
							.filerTournaments(tournaments, vm.isNewTournaments, vm.account);
					});
				}
			}
		);

		$scope.$on('destroy', function () {
			tournamentsListener();
		});

		function filterIsNew() {
			localStorageService.set('isNewTournaments', vm.isNewTournaments);
			TournamentService.getTournaments()
				.then(function (tournaments) {
					vm.tournaments = TournamentService
						.filerTournaments(tournaments, vm.isNewTournaments, vm.account);

					return vm.tournaments;
				});

		}

		function activate() {
			return $q.all({
					tournaments: TournamentService.getTournaments(),
					account: AccountService.getAccount()
				})
				.then(function (response) {
					vm.account = response.account;
					vm.tournaments = TournamentService.filerTournaments(
						response.tournaments,
						vm.isNewTournaments,
						vm.account
					);

					return response.tournaments;
				});
		}

		SocketService.on('tm:request:create', function (data) {
			var tournament, requests;
			return TournamentService.getTournaments()
				.then(function (tournaments) {
					tournament = lodash.find(tournaments, function (t) {
						return t.id === data.tournament.id;
					});

					if (!tournament) {
						$log.error('Tournament is undefined');
						return false;
					}

					requests = lodash.map(data.accounts, function (id) {
						return {
							account_id: id,
							tournament_id: data.tournament.id,
							team_id: data.team_id
						};
					});

					tournament.status = data.tournament.status || 0;
					tournament.starts_at = data.tournament.starts_at;
					tournament.requests = lodash.concat(
						tournament.requests,
						lodash.differenceBy(requests, tournament.requests,
							function (r) {
								return r.account_id;
							}
						)
					);

					return true;
				});
		});

		SocketService.on('tm:request:remove', function (data) {
			var tournament;
			return TournamentService.getTournaments()
				.then(function (tournaments) {
					tournament = lodash.find(tournaments, function (t) {
						return t.id === data.tournament.id;
					});

					if (!tournament) {
						$log.error('Tournament is undefined');
						return false;
					}

					tournament.status = data.tournament.status || 0;
					lodash.remove(tournament.requests, function (request) {
						return lodash
							.includes(data.accounts, request.account_id);
					});
				});
		});

		SocketService.on('tm:start', function (data) {
			return TournamentService.getTournaments()
				.then(function (tournaments) {
					var tournament = lodash.find(tournaments, function (t) {
						return t.id === data.id;
					});

					tournament.status = 4;
				});
		});

		SocketService.on('tm:kick', function (data) {
			if ((data.accounts)
				.indexOf(vm.account.id) === -1) {
				return;
			}

			// TODO add tournament details in logger
			logger.warning(
				gettextCatalog.getString('You was kicked from tournament')
			);
		});

	}
})();
