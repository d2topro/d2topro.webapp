(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardTeamController', dashboardTeamController);

    /* @ngInject */
    function dashboardTeamController(lodash, AccountTeamService, AccountTeamModalService) {
        var vm = this;
        vm.team = null;
        vm.captain = null;
        vm.playersFilter = playersFilter;
        vm.showAccountTeamModal = showAccountTeamModal;

        activate();

        function activate() {
            return AccountTeamService.getTeam()
                .then(function (team) {
                    if (team) {
                        vm.team = team;
                        vm.captain = team.getCaptain();
                    }

                    return team;
                });
        }

        function showAccountTeamModal() {
            return AccountTeamModalService.showAccountTeamModal();
        }

        function playersFilter() {
            return function (member) {
                return member.TeamPlayer.role === AccountTeamService.ROLE.TEAM_PLAYER;
            }
        }

    }

})();
