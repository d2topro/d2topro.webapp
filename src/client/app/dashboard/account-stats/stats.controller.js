/**
 * Created by DTuzenkov on 12/9/15.
 */
(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardPlayerStatsController', dashboardPlayerStatsController);

    /* @ngInject */
    function dashboardPlayerStatsController (AccountService) {
        var vm = this;
        vm.account = null;
        activate();

        function activate() {
            return AccountService
                .fetch()
                .then(function (account) {
                    vm.account = account;
                    return account;
                });
        }
    }

})();
