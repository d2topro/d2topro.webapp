(function () {
	'use strict';

	angular
		.module('app.dashboard')
		.run(appRun);

	/* @ngInject */
	function appRun(routerHelper, gettextCatalog) {
		routerHelper.configureStates(getStates());

		function getStates() {
			return [{
				state: 'root.dashboard',
				config: {
					parent: 'root',
					url: '/',
					templateUrl: 'app/dashboard/dashboard.html',
					controller: 'Dashboard',
					controllerAs: 'vm',
					title: gettextCatalog.getString('Tournaments'),
					settings: {
						nav: 10,
						icon: 'icons8-trophy',
						content: gettextCatalog.getString('Tournaments')
					},
					resolve: {
						/* @ngInject */
						account: function (AccountService, TournamentService) {
							return AccountService.fetch()
								.then(function () {
									return TournamentService.fetch();
								});
						}
					}
				}
            }];
		}
	}
})();
