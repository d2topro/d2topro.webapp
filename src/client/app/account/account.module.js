/**
 * Created by DTuzenkov on 12/1/15.
 */
(function() {
    'use strict';

    angular
        .module('app.account', [
            'app.core'
        ]);
})();
