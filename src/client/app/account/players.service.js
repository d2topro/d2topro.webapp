/**
 * Created by DTuzenkov on 12/1/15.
 */
(function () {
    'use strict';

    angular
        .module('app.account')
        .factory('PlayersService', playersService);

    /* @ngInject */
    function playersService($q, lodash, PlayerResource, PlayerInviteResource, exception) {
        var service = {
            search: search,
            inviteByEmailAddress: inviteByEmailAddress
        };

        function search(query) {
            if (query.length < 3) {
                return $q.when(null);
            }
            return PlayerResource
                .search({
                    query: query
                })
                .$promise
                .then(function (resources) {
                    if (resources.length) {
                        return resources;
                    }

                    return [{
                        steam_personal_name: query,
                        email: query
                    }];
                });
        }

        function inviteByEmailAddress(email) {
            return PlayerInviteResource
                .invite({
                    email: email
                })
                .$promise;
        }

        return service;
    }
})();
