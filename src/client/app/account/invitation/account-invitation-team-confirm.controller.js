/**
 * Created by DTuzenkov on 12/8/15.
 */
(function () {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountInvitationTeamConfirmController', accountInvitationTeamConfirmController);

    /* @ngInject*/
    function accountInvitationTeamConfirmController($scope, $rootScope, $uibModalInstance, TeamResource,
                                                    AccountTeamInviteResource, AccountTeamService,
                                                    invitation) {
        var vm = this;
        vm.team = null;
        vm.invitation = invitation;
        vm.accept = accept;
        vm.reject = reject;
        vm.close = close;
        vm.playersFilter = playersFilter;

        activate();

        function activate() {
            return TeamResource
                .get({id: invitation.id})
                .$promise
                .then(function (resource) {
                    vm.team = resource;
                    return resource;
                });
        }

        function accept() {
            return invitation
                .$accept({id: invitation.id})
                .then(close);
        }

        function reject() {
            return invitation
                .$delete(({id: invitation.id}))
                .then(close);
        }

        function close() {
            $rootScope.$emit('update:notifications');
            $uibModalInstance.close();
        }

        function playersFilter() {
            return function (member) {
                return member.isActiveTeamPlayer === true
                    || member.TeamPlayer.role === AccountTeamService.ROLE.TEAM_CAPTAIN;
            };
        }

    }

})();



