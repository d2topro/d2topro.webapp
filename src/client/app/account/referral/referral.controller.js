(function () {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountReferralController', controller);


    function controller($location, $window, AccountService, logger, translate) {
        var vm = this;
        vm.account = {};
        vm.getUrl = getUrl;
        vm.successCopyToClipboard = successCopyToClipboard;

        activate();

        function activate() {
            return AccountService.getAccount()
                .then(function (account) {
                    vm.account = account;
                });
        }

        function getUrl() {
            var parts = $location.$$absUrl.split('/')
                .splice(0, 3);
            parts.push('r', $window.btoa(vm.account.id));

            return parts.join('/');
        }

        function successCopyToClipboard() {
            logger.success(translate('The link was copied to clipboard!'));
        }
    }
})();
