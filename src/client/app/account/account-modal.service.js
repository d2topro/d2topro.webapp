(function() {
    'use strict';

    angular
        .module('app.account')
        .factory('AccountModalService', accountModalService);

    /* @ngInject */
    function accountModalService($uibModal) {

        return {
            showAccountInfoModal: showAccountInfoModal,
        };

        function showAccountInfoModal(account) {
            return $uibModal
                .open({
                    animation: true,
                    templateUrl: 'app/account/modals/account-info/account-info.html',
                    controller: 'AccountInfoModalController',
                    controllerAs: 'vm',
                    size: 'sm modal-center width-400',
                    backdropClass: 'BACKDROP_CLASS',
                    windowTopClass: '',
                    resolve: {
                        account: function() {
                            return account;
                        }
                    }
                })
                .result;
        }
    }
})();
