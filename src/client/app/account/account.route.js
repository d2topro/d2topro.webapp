(function () {
    'use strict';

    angular
        .module('app.account')
        .run(appRun);

    /* @ngInject */
    function appRun(routerHelper, gettextCatalog) {
        routerHelper.configureStates(getStates());


        function getStates() {

            return [{
                    state: 'root.account',
                    config: {
                        templateUrl: 'app/account/account.html',
                        abstract: true,
                    }
                }, {
                    state: 'root.account.info',
                    config: {
                        url: '/info',
                        templateUrl: 'app/account/info/info.html',
                        controller: 'AccountInfoController',
                        controllerAs: 'vm',
                        title: 'Info'
                    }

                }, {
                    state: 'root.account.referral',
                    config: {
                        url: '/referral',
                        templateUrl: 'app/account/referral/referral.html',
                        controller: 'AccountReferralController',
                        controllerAs: 'vm',
                        title: 'Referral'
                    }
                }, {
                    state: 'root.account.wallet',
                    config: {
                        url: '/wallet',
                        templateUrl: 'app/wallet/wallet.html',
                        controller: 'WalletController',
                        controllerAs: 'vm',
                        title: 'Wallet'
                    }
                },
                {
                    state: 'root.account.join-team',
                    config: {
                        url: '/join-team/:code/:id',
                        controller: 'TeamJoinController',
                        controllerAs: 'vm',
                        title: 'Join team',
                        resolve: {
                            /* @ngInject */
                            team: function ($stateParams, TeamService) {
                                return TeamService.getTeam($stateParams.id);
                            }
                        }
                    }
                }
            ];
        }
    }
})();
