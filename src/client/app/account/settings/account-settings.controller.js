(function() {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountSettingsController', accountSettingsController);

    /* @ngInject*/
    function accountSettingsController(lodash, AccountService) {
        var vm = this;
        vm.account = null;
        vm.accountData = {
            email: null,
            skype: null,
            phone: null,
            payment_wallet: null
        };
        vm.submit = submit;

        activate();

        function activate() {
            return AccountService
                .fetch()
                .then(function(resource) {
                    vm.account = resource;
                    return resource;
                });
        }

        function submit() {
            vm.account.$update();
        }

    }
})();
