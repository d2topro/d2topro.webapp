/**
 * Created by DTuzenkov on 12/1/15.
 */
(function () {
    'use strict';

    angular
        .module('app.account')
        .factory('AccountService', service);

    /* @ngInject */
    function service($q, $http, lodash, environment,
        AccountResource, AccountTeamService, AccountTeamPlayerResource) {
        var promise, accessPromise;

        return {
            fetch: fetch,
            getAccount: getAccount,
            getTeamPlayer: getTeamPlayer,
            checkAccess: checkAccess
        };

        function fetch() {
            promise = AccountResource.get()
                .$promise;

            return promise;
        }

        function getAccount() {
            return promise ? promise : fetch();
        }

        function getTeamPlayer() {
            return $q.all({
                    team: AccountTeamService.getTeam(),
                    account: getAccount(),
                })
                .then(function (response) {

                    if (!response.team) {
                        return null;
                    }

                    var member = lodash.find(response.team.members,
                        function (m) {
                            return response.account.id === m.id;
                        }
                    );

                    return new AccountTeamPlayerResource(member.TeamPlayer);
                });
        }

        function checkAccess() {

            if (accessPromise) {
                return accessPromise;
            }

            accessPromise = $http
                .get(environment.apiUrl + '/api/manage/access');

            return accessPromise
                .then(function (response) {
                    return response.status === 200;
                });
        }
    }
})();
