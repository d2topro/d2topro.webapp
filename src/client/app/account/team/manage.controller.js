/**
 * Created by DTuzenkov on 12/8/15.
 */
(function () {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountTeamManageController', controller);

    /* @ngInject*/
    function controller($q, $scope, $uibModalInstance, $intercom, AccountTeamService, AccountService) {
        var vm = this;
        vm.team = null;
        vm.player = null;
        vm.tab = 'players';
        vm.cancel = cancel;
        vm.isReady = false;
        vm.intercomWidget = angular.element('#intercom-container .intercom-launcher-frame');

        activate();

        function activate() {
            vm.intercomWidget.css('display', 'none');
            return $q.all({
                    team: AccountTeamService.fetch(),
                    player: AccountService.getTeamPlayer(),
                })
                .then(function (response) {
                    vm.team = response.team;
                    vm.player = response.player;
                    vm.isReady = true;
                });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
            vm.intercomWidget.css('display', 'block');
        }
    }

})();
