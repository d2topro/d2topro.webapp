/**
 * Created by DTuzenkov on 12/12/15.
 */
(function () {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountTeamPlayersController', accountTeamPlayersController);

    /* @ngInject*/
    function accountTeamPlayersController(lodash, config, AccountService, AccountTeamService, PlayersService) {
        var vm = this;
        vm.account = null;
        vm.team = null;
        vm.captain = null;
        vm.invite = null;
        vm.disableAll = false;
        vm.benchPlayers = [];
        vm.activePlayers = [];
        vm.foundPlayers = null;
        vm.sortableOptions = {
            items: '> a',
            //handle: '.player-handle',
            opacity: 0.5,
            delay: 100,
            connectWith: '.members',
            placeholder: 'player-placeholder',
            cursor: 'pointer',
            helper: 'clone',
            distance: 0,
            //receive: sortableReceive,
            //stop: sortableStop,
            //update: sortableUpdate
        };
        vm.inviteMembers = inviteMembers;
        vm.searchPlayers = searchPlayers;
        vm.expelMember = expelMember;
        vm.getInvitationUrl = getInvitationUrl;

        activate();

        function activate() {
            return AccountService
                .fetch()
                .then(function (account) {
                    vm.account = account;
                    return AccountTeamService.getTeam();
                })
                .then(function (team) {
                    if (!team) {
                        vm.team = null;
                        vm.benchPlayers = [];
                        vm.activePlayers = [];
                    } else {
                        vm.team = team;
                        vm.benchPlayers = team.getBenchPlayers();
                        vm.activePlayers = team.getActivePlayers();
                    }

                    return team;
                });
        }

        function inviteMembers() {
            var promise = (vm.invite.id)
                ? AccountTeamService.invitePlayerById(vm.invite.id)
                : PlayersService.inviteByEmailAddress(vm.invite.email);
            promise
                .then(function (resource) {
                    return AccountTeamService.refresh();
                })
                .then(activate);
        }

        function searchPlayers(query) {
            PlayersService
                .search(query)
                .then(function (resources) {
                    vm.foundPlayers = resources;
                });
        }

        function expelMember(member) {
            if (!confirm('Do you really want to expel mate?')) {
                return;
            }

            return member.TeamPlayer.$delete()
                .then(AccountTeamService.fetch)
                .then(activate);
        }

        /* Sortable */

        function getInvitationUrl() {
            return config.apiUrl + '/i/' + vm.team.invites[0].code;
        }
    }

})();
