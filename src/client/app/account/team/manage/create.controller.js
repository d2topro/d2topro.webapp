(function () {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountCreateTeamController', controller);

    function controller($scope, $uibModalInstance, $intercom, $q, logger,
        translate, AccountService, AccountTeamService, AccountTeamModalService,
        Upload, S3Service, ServerError, DEFAULT_TEAM_LOGO) {
        var vm = this;
        var file;
        vm.defaultUrl = DEFAULT_TEAM_LOGO;
        vm.account = null;
        vm.attributes = {
            name: '',
            tag: ''
        };
        vm.create = create;
        vm.close = close;

        $scope.onFileSelect = function (selected) {
            file = selected;
        };

        activate();

        function activate() {
            return AccountService.getAccount()
                .then(function (account) {
                    vm.account = account;
                });
        }

        function create() {
            var s3, promise = $q.when(true);

            if (file) {
                promise = S3Service.getS3Credentials(file)
                    .then(function (data) {
                        s3 = data;
                        vm.attributes.logo = { url: s3.credentials.params.key };

                        return;
                    });
            }

            return promise
                .then(function () {
                    return AccountTeamService.create(vm.attributes);
                })
                .then(function (response) {
                    $intercom('trackEvent', 'WR_TEAM_CREATED', {
                        id: response.id,
                        name: response.name,
                        tag: response.tag
                    });

                    if (file) {
                        return Upload.upload({
                            url: s3.credentials.endpoint_url,
                            data: s3.credentials.params
                        });
                    }
                })
                .then(close)
                .then(AccountTeamModalService.showAccountTeamModal)
                .then(function () {
                    logger.success(
                        translate('The team has been created successfully'));
                })
                .catch(function (cause) {
                    logger.error(ServerError.message(cause));
                });
        }

        function close() {
            $intercom('trackEvent', 'WR_CLOSED_CREATE_TEAM_MODAL');
            $uibModalInstance.close();
        }
    }
})();
