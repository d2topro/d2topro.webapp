/**
 * Created by DTuzenkov on 12/8/15.
 */
(function () {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountTeamSettingsController', controller);

    /* @ngInject*/
    function controller($scope, $intercom, $q, translate, logger,
        AccountTeamService, AccountService, Upload, S3Service, ServerError) {

        var vm = this;
        var file;

        vm.team = null;
        vm.player = null;

        vm.update = update;
        vm.remove = remove;

        $scope.onFileSelect = function (selected) {
            file = selected;
        };

        activate();

        function activate() {
            return $q.all({
                    team: AccountTeamService.getTeam(),
                    player: AccountService.getTeamPlayer()
                })
                .then(function (response) {
                    vm.team = response.team;
                    vm.player = response.player;
                });
        }

        function update() {
            var s3, promise = $q.when(true);

            if (file) {
                promise = S3Service
                    .getS3Credentials(file)
                    .then(function (data) {
                        s3 = data;
                        vm.team.logo = { url: s3.credentials.params.key };
                    });
            }

            return promise
                .then(function () {
                    return vm.team.$update();
                })
                .then(function () {
                    return (!file) ? null : Upload.upload({
                        url: s3.credentials.endpoint_url,
                        data: s3.credentials.params
                    });
                })
                .then(function () {
                    logger.success(translate(
                        'You information has successfully been update'));

                    $intercom('trackEvent', 'WR_TEAM_UPDATED', {
                        id: vm.team.id,
                        name: vm.team.name
                    });
                })
                .then(activate)
                .catch(function (cause) {
                    logger.error(ServerError.message(cause));
                });
        }

        function remove() {
            var message =
                translate('Please type a \'DELETE\' word to confirm delete');

            if (prompt(message) !== 'DELETE') {
                return;
            }

            return vm.team.$delete()
                .then(function () {
                    $intercom('trackEvent', 'WR_TEAM_DELETED');
                    logger.success(translate('You team has been deleted'));

                    return $q.all([
                        AccountTeamService.fetch(),
                        AccountService.fetch()
                    ]);
                })
                .then(close)
                .catch(function (cause) {
                    logger.error(ServerError.message(cause));
                });
        }

        function close() {
            $scope.$parent.$close();
        }
    }
})();
