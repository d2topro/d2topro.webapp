(function () {
    'use strict';

    angular
        .module('app.account')
        .factory('S3Service', service);

    /* @ngInject */
    function service(AccountS3CredentialsResource) {
        return {
            getS3Credentials: getS3Credentials,
        };

        function getS3Credentials(file) {
            return AccountS3CredentialsResource.get({
                    filename: file.name
                })
                .$promise
                .then(function (response) {
                    response.credentials.params.file = file;
                    return response;
                });
        }
    }

})();
