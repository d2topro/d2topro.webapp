/**
 * Created by DTuzenkov on 12/18/15.
 */
(function() {
    'use strict';

    angular
        .module('app.account')
        .factory('AccountTeamModalService', service);
    /* @ngInject */
    function service($uibModal) {
        return {
            showAccountTeamModal: showAccountTeamModal,
            showAccountTeamCreateModal: showAccountTeamCreateModal,
            showAccountTeamInviteModal: showAccountTeamInviteModal
        };

        function showAccountTeamModal() {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/account/team/manage-modal.html',
                controller: 'AccountTeamManageController',
                controllerAs: 'vm',
                size: 'sm width-350 modal-sidebar',
                backdropClass: 'BACKDROP_CLASS',
                windowTopClass: 'manage-team-modal',
                resolve: {}
            });
        }

        function showAccountTeamCreateModal() {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/account/team/manage/create-modal.html',
                controller: 'AccountCreateTeamController',
                controllerAs: 'vm',
                size: 'sm width-350 modal-sidebar',
                backdropClass: 'BACKDROP_CLASS',
                windowTopClass: 'create-team-modal',
                resolve: {}
            });
        }

        function showAccountTeamInviteModal() {
            //return $uibModal.open({
            //    animation: false,
            //    templateUrl: 'app/account/team/manage/create-modal.html',
            //    controller: 'AccountCreateTeamController',
            //    controllerAs: 'vm',
            //    size: 'sm width-350 modal-sidebar',
            //    backdropClass: 'BACKDROP_CLASS',
            //    windowTopClass: 'create-team-modal',
            //    resolve: {}
            //});
        }

    }
})();
