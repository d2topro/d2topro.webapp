/**
 * Created by DTuzenkov on 4/29/16.
 */
(function () {
    'use strict';
    angular
        .module('app.account')
        .controller('AccountTeamInviteController', controller);

    /* @ngInject */
    function controller($location, logger, translate, AccountTeamService) {
        var vm = this;
        vm.team = null;
        vm.successCopyToClipboard = successCopyToClipboard;
        vm.getUrl = getUrl;

        activate();

        function activate() {
            return AccountTeamService.getTeam()
                .then(function (response) {
                    vm.team = response;
                });
        }

        function getUrl() {
            return [
                location.origin,
                'join-team',
                vm.team.code,
                vm.team.id
            ].join('/');
        }

        function successCopyToClipboard() {
            logger.success(translate('The link was copied to clipboard!'));
        }
    }
})();
