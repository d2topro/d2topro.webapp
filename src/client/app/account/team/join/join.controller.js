(function () {
    'use strict';

    angular
        .module('app.account')
        .controller('TeamJoinController', TeamJoinController);

    /* @ngInject */
    function TeamJoinController($uibModal, $state, team) {
        var vm = this;

        activate();

        function activate() {
            return $uibModal
                .open({
                    animation: true,
                    templateUrl: 'app/account/modals/join-team/join-team.html',
                    controller: 'JoinTeamModalController',
                    controllerAs: 'vm',
                    size: 'sm modal-center width-400',
                    backdropClass: 'BACKDROP_CLASS',
                    windowTopClass: '',
                    resolve: {
                        team: function () {
                            return team;
                        }
                    }
                })
                .result
                .then(function () {
                    return $state.go('root.dashboard');
                });
        }

    }

})();
