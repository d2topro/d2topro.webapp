(function () {
    'use strict';

    angular
        .module('app.account')
        .factory('AccountTeamService', service);

    /* @ngInject */
    function service(AccountTeamResource, AccountTeamPlayerResource) {

        var promise;

        return {
            fetch: fetch,
            create: create,
            leave: leave,
            getTeam: getTeam,
        };

        function getTeam() {
            return promise ? promise : fetch();
        }

        function fetch() {
            promise = AccountTeamResource.query()
                .$promise
                .then(function (resources) {
                    return resources.length ? resources[0] : null;
                });

            return promise;
        }

        function create(attributes) {
            return AccountTeamResource.create(attributes)
                .$promise;
        }

        function leave() {
            return getTeam()
                .then(function (team) {
                    return AccountTeamPlayerResource.leave({
                            id: team.id
                        })
                        .$promise;
                });
        }

    }
})();
