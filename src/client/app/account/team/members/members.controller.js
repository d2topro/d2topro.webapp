(function () {
    'use strict';
    angular
        .module('app.account')
        .controller('AccountTeamMembersController', controller);

    /* @ngInject */
    function controller($scope, lodash, AccountService, translate,
        AccountTeamService, logger, ServerError, gettextCatalog) {

        var vm = this;
        vm.team = null;
        vm.player = null;
        vm.benchPlayers = [];
        vm.activePlayers = [];
        vm.sortableOptions = {
            items: '> a',
            //handle: '.player-handle',
            opacity: 0.5,
            delay: 100,
            connectWith: '.members',
            placeholder: 'player-placeholder',
            cursor: 'pointer',
            helper: 'clone',
            distance: 0,
            receive: sortableReceive,
            stop: sortableStop,
            update: sortableUpdate
        };

        vm.expelMember = expelMember;
        vm.leave = leave;

        activate();

        function activate() {
            AccountTeamService.getTeam()
                .then(function (team) {
                    if (!team) {
                        vm.team = null;
                        vm.benchPlayers = [];
                        vm.activePlayers = [];
                    } else {
                        vm.team = team;
                        vm.benchPlayers = team.getBenchPlayers();
                        vm.activePlayers = team.getActivePlayers();
                    }

                    return AccountService.getTeamPlayer()
                })
                .then(function (player) {
                    vm.player = player;
                });
        }

        function expelMember(member) {
            var message = translate(
                'Do you really want to remove "{{name}}" from team?', {
                    name: member.steam_personal_name
                });

            if (!confirm(message)) {
                return;
            }

            return member.TeamPlayer.$delete()
                .then(AccountTeamService.fetch)
                .then(activate)
                .catch(function (cause) {
                    logger.error(
                        ServerError.message(cause), {}, translate('Error'));
                });
        }


        function sortableUpdate(event, ui) {
            var sortable = ui.item.sortable;

            if (sortable.isCanceled()) {
                return;
            }

            //if (sortable.model.TeamPlayer.isDisabled()) {
            //    return sortable.cancel();
            //}

            if (sortable.sourceModel === sortable.droptargetModel) {
                return sortable.cancel();
            }
        }

        function sortableReceive(event, ui) {
            var sortable = ui.item.sortable;

            if (sortable.isCanceled()) {
                return;
            }

            if (lodash.isUndefined(sortable.model)) {
                return sortable.cancel();
            }

            if (sortable.droptarget[0].id === 'activePlayers' && sortable.droptargetModel.length > 4) {
                sortable.cancel();
                alert('Cannot more than 4');
            }
        }

        function sortableStop(event, ui) {
            var sortable = ui.item.sortable;

            if (sortable.isCanceled()) {
                return;
            }

            if (lodash.isUndefined(sortable.droptargetModel)) {
                return;
            }

            sortable.model.TeamPlayer.togglePlayerRole()
                .then(function () {
                    logger.success('You action has been done successfully');
                });
        }

        function leave() {
            var message = translate('Do you really want to leave', {
                name: vm.team.name
            });

            if (!confirm(message)) {
                return;
            }

            return AccountTeamService.leave()
                .then(function () {
                    logger.success(
                        translate('You have successfully left the team'));
                    $scope.$parent.$close();
                })
                .then(AccountTeamService.fetch)
                .catch(function (cause) {
                    logger.error(
                        ServerError.message(cause), {}, translate('Error'));
                });
        }
    }
})();
