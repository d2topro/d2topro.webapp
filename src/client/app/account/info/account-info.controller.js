(function() {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountInfoController', controller);


    function controller($log, AccountService, logger, gettextCatalog, lodash) {
        var vm = this;
        vm.account = {};
        vm.submit = lodash.debounce(submit, 2000);

        activate();

        function activate() {
            return AccountService
                .getAccount()
                .then(function(account) {
                    vm.account = account;
                });
        }

        function submit() {
            return vm.account
                .$update()
                .then(function() {
                    return logger
                        .success('Information has been update successfully');
                })
                .catch(function(cause) {
                    $log.error(
                        cause.detail || cause.message,
                        cause.extendedInfo
                    );
                });
        }
    }
})();
