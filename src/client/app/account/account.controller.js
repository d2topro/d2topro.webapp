(function() {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountController', accountController);

    /* @ngInject*/
    function accountController($scope, $state, AccountService, $location) {
        var vm = this,
            tabsListener;
        vm.account = null;
        vm.viewMode = $state.current.name;


        activate();

        function activate() {
            return AccountService
                .getAccount()
                .then(function(account) {
                    vm.account = account;
                });
        }



        tabsListener = $scope.$watch(function() {
            return $state.current.name;
        }, function(newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            }

            vm.viewMode = newValue;
        });


        $scope.$on('destroy', tabsListener);
    }
})();
