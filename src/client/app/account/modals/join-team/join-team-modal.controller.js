(function () {
    'use strict';

    angular
        .module('app.account')
        .controller('JoinTeamModalController', controller);

    /* @ngInject*/
    function controller($stateParams, logger, ServerError, translate,
        $uibModalInstance, team, TeamService, AccountService,
        AccountTeamModalService) {
        var vm = this;
        vm.team = team;
        vm.accept = accept;
        vm.close = close;

        activate();

        function activate() {
            return AccountService.getTeamPlayer()
                .then(function (player) {
                    vm.player = player;
                });
        }

        function accept() {
            return TeamService.join($stateParams.id, $stateParams.code)
                .then(close)
                .then(AccountTeamModalService.showAccountTeamModal)
                .catch(function (cause) {
                    logger.error(
                        ServerError.message(cause), {}, translate('Error'));
                });
        }

        function close() {
            $uibModalInstance.close();
        }
    }
})();
