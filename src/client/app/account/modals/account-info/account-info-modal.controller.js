(function() {
    'use strict';

    angular
        .module('app.account')
        .controller('AccountInfoModalController', accontInfoModalController);

    /* @ngInject*/
    function accontInfoModalController($uibModalInstance, account) {

        var vm = this;
        vm.close = close;
        vm.account = account;

        function close() {
            $uibModalInstance.close();
        }
    }
})();
