/**
 * Created by DTuzenkov on 12/11/15.
 */
(function() {
    'use strict';

    angular
        .module('app.static', [
            'app.core'
        ]);
})();
