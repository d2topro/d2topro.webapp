(function () {
	'use strict';

	angular
		.module('app.static')
		.run(appRun);

	/* @ngInject */
	function appRun(routerHelper, gettextCatalog) {
		routerHelper.configureStates(getStates());


		function getStates() {
			return [{
				state: 'root.static',
				config: {
					url: '/static',
					//templateUrl: 'app/tournaments/tournament.html',
					template: '<ui-view class="shuffle-animation"/>',
					abstract: true
				}
            }, {
				state: 'root.static.ratings',
				config: {
					url: '/ratings',
					templateUrl: 'app/static/ratings.html',
					controller: 'StaticRatingsController',
					controllerAs: 'vm',
					title: 'Ratings',
					settings: {
						// nav: 6,
						// icon: 'wb-star-outline',
						// content: 'Ratings'
					}
				}
            }, {
				state: 'root.static.updates',
				config: {
					url: '/updates',
					templateUrl: 'app/static/updates.html',
					controller: 'StaticUpdatesController',
					controllerAs: 'vm',
					title: 'Product Updates',
					settings: {
						// nav: 6,
						// icon: 'wb-grid-4',
						// content: 'Product Updates'
					}
				}
            }, {
				state: 'root.static.faq',
				config: {
					url: '/faq',
					templateUrl: 'app/static/faq.html',
					controller: 'StaticFaqController',
					controllerAs: 'vm',
					title: gettextCatalog.getString('FAQ'),
					settings: {
						nav: 30,
						icon: 'icons8-help',
						content: 'FAQ'
					}
				}
            }, {
				state: 'root.static.contact',
				config: {
					url: '/contact',
					templateUrl: 'app/static/contact.html',
					controller: 'StaticContactController',
					controllerAs: 'vm',
					title: 'Contact Us',
					settings: {
						// nav: 6,
						// icon: 'wb-envelope',
						// content: 'Contact Us'
					}
				}
            }];
		}
	}
})();
