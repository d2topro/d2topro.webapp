(function () {
    'use strict';

    angular
        .module('app.core')
        .run(appRun);

    /* @ngInject */
    function appRun($window, $rootScope, $cookies, $intercom, routerHelper,
        gettextCatalog, moment, IntercomService) {

        var otherwise = '/';
        var dateExpiration = new Date();
        dateExpiration.setDate(dateExpiration.getDate() + 30);

        routerHelper.configureStates(getStates(), otherwise);
        putCookiesLocale(getLanguage());
        setLocale();

        $rootScope.$watch('customLang', function (newValue, oldValue) {
            if (newValue !== oldValue) {
                putCookiesLocale(newValue);
                $window.location.reload();
            }
        });

        IntercomService
            .getUserData()
            .then($intercom.boot);

        function getStates() {
            return [{
                    state: '404',
                    config: {
                        url: '/404',
                        templateUrl: 'app/core/404.html',
                        title: '404'
                    }
                },
                {
                    state: 'root',
                    config: {
                        abstract: true,
                        templateUrl: 'app/layout/layout.html'
                    }
                },
                {
                    state: 'root.summon',
                    config: {
                        url: '/r/:summoner',
                        controller: 'SummonController',
                        controllerAs: 'vm',
                    }
                }
            ];
        }

        function setLocale() {
            gettextCatalog.setCurrentLanguage(getLanguage());
            moment.tz(moment.tz.guess());
            moment.locale(getLanguage());
        }

        function putCookiesLocale(newLanguage) {
            $cookies.put('customLang', newLanguage, {
                expires: dateExpiration
            });
        }

        function getLanguage() {
            var browserLang = (window.navigator.language)
                .substr(0, 2);
            var customLang = $rootScope.customLang;
            var cookiesLang = $cookies.get('customLang');

            cookiesLang ? cookiesLang : putCookiesLocale(customLang ? customLang : browserLang);

            return cookiesLang;
        }
    }
})();
