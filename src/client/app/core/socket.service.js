(function () {
    'use strict';
    angular
        .module('app.core')
        .service('SocketService', socketService);

    /* @ngInject */
    function socketService($q, $rootScope, $log, socketFactory, environment,
        AccountResource) {

        var EVENT_CHANNELS = [
            // Notification
            // 'notification',
            // 'nt',
            // Accept tournament confirmation
            // 'tm:participant:confirm',
            // 'tm:participant:cancel',
            // Tournament common
            'tm:request:create',
            'tm:request:remove',
            'tm:request:prepare',
            'tm:request:update',
            // tournament
            'tm:kick',
            'tm:start',
            'tm:update',
            'tm:fail',
            'tm:complete',
            // Match activity
            'tm:match:start',
            'tm:match:lobby:start',
            'tm:match:lobby:complete',
            'tm:match:complete',
        ];

        var promise = AccountResource.getToken()
            .$promise
            .then(factory);

        // TODO: Maybe use socket.forward() with some events
        return {
            on: function (eventName, callback) {
                return promise.then(function (socket) {
                    return socket.on(eventName, callback);
                });
            }
        };

        function factory(response) {
            var socket = io.connect(environment.socketServerUrl, {
                transports: ['websocket', 'polling'],
                query: 'token=' + response.token
            });

            if (environment.key !== 'production') {
                EVENT_CHANNELS.forEach(function (channel) {
                    return socket.on(channel, function (data) {
                        $log.info('-- trace --', channel, data);
                    });
                });

            }

            return socketFactory({
                prefix: 'websocket',
                ioSocket: socket
            });
        }
    }
})();
