(function () {
    'use strict';

    angular
        .module('app.core')
        .service('IntercomService', service)
        .config(function ($intercomProvider, INTERCOM_APPID) {
            $intercomProvider
                .appID(INTERCOM_APPID);

            $intercomProvider
                .asyncLoading(true);
        });

    /* @ngInject */
    function service(AccountService) {
        return {
            getUserData: getUserData
        };

        function getUserData() {
            return AccountService.getAccount()
                .then(function (account) {
                    return {
                        // TODO should add sha256 for some attributes
                        user_id: account.steam_id,
                        name: account.steam_personal_name,
                        created_at: account.created_at,
                        avatar: {
                            type: 'avatar',
                            image_url: account.steam_avatar_url
                        }
                    };
                });
        }

    }
})();
