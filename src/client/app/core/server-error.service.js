(function () {
    'use strict';

    angular
        .module('app.core')
        .service('ServerError', serverError);

    /* @ngInject */
    function serverError(translate) {
        return {
            message: message
        };

        function message(error) {
            var msg = error.message;

            if (!!error.detail) {
                msg = error.detail;
            }

            return translate(msg, error.extendedInfo);
        }
    }

})();
