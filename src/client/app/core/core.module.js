(function() {
    'use strict';

    angular
        .module('app.core', [
            /* Angular modules */
            'ngAnimate',
            'ngSanitize',
            'ngResource',
            'ngFileUpload',
            'ngCookies',
            'ngIntercom',
            'ngAudio',
            /* Cross-app modules */
            'blocks.diagnostics',
            'blocks.exception',
            'blocks.logger',
            'blocks.router',
            /* 3rd-party modules */
            'ui.bootstrap',
            'ui.router',
            'ui.select',
            'ui.sortable',
            'ngLodash',
            'ngplus',
            'NgSwitchery',
            'toastr',
            'btford.socket-io',
            'gettext',
            'angular-clipboard',
            'rzModule',
            'countrySelect',
            'angular-timezone-selector',
            'ngclipboard'
            //'ngDropzone'
        ]);

})();
