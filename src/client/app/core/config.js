(function () {
    'use strict';

    var core = angular.module('app.core'),
        environment;

    environment = window.__env || {
        key: (document.location.hostname === 'localhost') ?
            'development' : 'production',
        // Rest Api url endpoint
        apiUrl: 'https://localhost:3000',
        managerUrl: 'https://localhost:3001',
        // Tournament manager api url endpoint (sockets only)
        socketsServerUrl: 'https://localhost:3000',
        debug: true
    };

    core.config(toastrConfig);
    core.constant('environment', environment);
    /* @ngInject */
    function toastrConfig(toastr) {
        toastr.options.timeOut = 2000;
        toastr.options.positionClass = 'toast-top-right';
        toastr.options.preventDuplicates = true;
    }
    // @see http://www.jvandemo.com/how-to-configure-your-angularjs-application-using-environment-variables/
    var config = {
        // Configure the exceptionHandler decorator
        appErrorPrefix: '[Error] ',
        appTitle: '',
        imageBasePath: '',
        apiUrl: environment.apiUrl,
        endpointPrefix: environment.apiUrl
    };

    core.value('config', config);

    core.config(configure);

    configure.$inject = ['$compileProvider', '$logProvider',
        'diagnostics', 'exceptionHandlerProvider', 'routerHelperProvider'
    ];
    /* @ngInject */
    function configure($compileProvider, $logProvider,
        diagnostics, exceptionHandlerProvider, routerHelperProvider) {

        diagnostics.enable = false;

        $compileProvider.debugInfoEnabled(false);

        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        exceptionHandlerProvider.configure(config.appErrorPrefix);
        configureStateHelper();

        function configureStateHelper() {
            var resolveAlways = {
                /* @ngInject */
                //ready: function () {
                //    return dataservice.ready();
                //}
                account: function (AccountService) {
                    return AccountService.getAccount();
                }
            };

            routerHelperProvider.configure({
                docTitle: '',
                resolveAlways: resolveAlways
            });
        }
    }
})();
