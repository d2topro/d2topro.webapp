/**
 * Created by DTuzenkov on 4/16/16.
 */

(function() {
    'use strict';
    angular
        .module('app.core')
        .service('DropzoneService', service);
    /* @ngInject */
    function service (toastr, environment, config) {
        return {
            show: show
        };

        function show(message) {
            toastr.error(message)
        }
    }
})();
