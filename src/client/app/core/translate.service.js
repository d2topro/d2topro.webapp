(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('translate', translate);

    /* @ngInject */
    function translate(gettextCatalog) {
        return function () {
            return gettextCatalog.getString.apply(gettextCatalog, arguments);
        };
    }

})();
