/* global toastr:false, moment:false */
(function () {
    'use strict';

    angular
        .module('app.core')
        .constant('toastr', toastr)
        .constant('moment', moment)
        .constant('INTERCOM_APPID', 'lsuq9mxy')
        .constant('DEFAULT_TEAM_LOGO', defaultLogo())
        .constant('DEFAULT_ACCOUNT_LOGO', defaultLogo())
        .constant('TOURNAMENT_START_SOUND', '/sounds/powershot.wav')
        .constant('RULES_PAGE_URL', 'https://winranger.com/Rules.html');


    /* @ngInject */
    function defaultLogo() {
        return '/images/logo/default_logo.png';
    }
})();
