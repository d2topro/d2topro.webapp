(function () {

	'use strict';

	angular
		.module('app', [
        /* Shared modules */
        'app.core',
        'app.resources',
        'app.interceptors',
        'app.widgets',

        /* Feature areas */
        'app.admin',
        'app.layout',
        'app.dashboard',
        'app.account',
        'app.teams',
        'app.tournaments',
        'app.static',
        'app.wallet',
        'app.account-tournaments',
        'app.tournament-manager'
    ]);

})();
