angular.module('gettext')
    .run(['gettextCatalog', function(gettextCatalog) {
        /* jshint -W100 */
        gettextCatalog.setStrings('ru', {
            "(drag and move players)": "(перетащите игроков)",
            "... Starting": "...Стартуем",
            "1 PLACE": "1 МЕСТО",
            "1. General Info": "1. Основная информация",
            "129th-256th": "129е-256е",
            "17th-32st": "17е-32е",
            "1st": "1е",
            "2 PLACE": "2 МЕСТО",
            "2. Org. Structure": "2. Орг. Структура",
            "257th-512th": "257е-512е",
            "2nd": "2е",
            "3,4 PLACE": "3, 4 МЕСТО",
            "3. Prize Settings": "3. Призовые",
            "33nd-64rd": "33е-64е",
            "3rd-4th": "3е-4е",
            "4. Rules and Policy": "4. Правила",
            "50 for each authorized!": "50 за каждую авторизацию",
            "5th-8th": "5е-8е",
            "65th-128th": "65е-128е",
            "9th-16th": "9е-16е",
            "Accept": "Подтвердить",
            "Accepted": "Подтвержден",
            "Account Details": "Подробности аккаунта",
            "Account Info": "Инфо аккаунта",
            "Account Owner": "Владелец аккаунта",
            "Actions": "Действия",
            "Active": "Активный",
            "All notifications": "Все уведомления",
            "Allow sound signal when the game begins": "Звуковой сигнал при старте игры",
            "Amount": "Сумма",
            "Appointed Time": "Назначенное время",
            "Are you sure?": "Ты уверен?",
            "Are you willing to lose 50% of chargeback?": "Ты готов потерять 50% оплаченного взноса?",
            "Authorize via Steam": "Авторизуйся через Steam",
            "Average Elo rating": "Средний ELO рейтинг",
            "Awaiting Opponents...": "Ждем соперника...",
            "BUY WR COINS": "КУПИТЬ WR COINS",
            "Back": "Назад",
            "Bench": "Скамья запасных",
            "Best of": "Best of ",
            "By filling": "По заполнению",
            "CHOOSE METHOD": "ВЫБРАТЬ СПОСОБ",
            "COLLECT FEE": "СБОР ВЗНОСОВ",
            "COLLECT PRIZE": "ЗАБРАТЬ ПРИЗ",
            "Captain": "Капитан",
            "Change the profile settings and view the statistics": "Изменить настройки профиля и посмотреть статистику",
            "Check the wallet’s state and track the history of transactions": "Проверить состояние счета и посмотреть историю платежей",
            "Choose currency and the method to deposit": "Выбери валюту и способ пополнения",
            "Choose currency and the method to withdraw": "Выбери валюту и способ вывода",
            "Choose the sum to deposit": "Выбери нужный пакет для пополнения",
            "Choose the sum to withdraw": "Выбери сумму для вывода средств",
            "Collapse": "Свернуть",
            "Company": "Компания",
            "Company Info": "Инфо о компании",
            "Congratulations,": "Поздравляем,",
            "Contact Email": "Контактный Email",
            "Contact info": "Контактная инфо",
            "Copy link": "Скопировать",
            "Country": "Страна",
            "Create Team": "Создать команду",
            "Create Tournament": "Создать турнир",
            "Create team": "Создать команду",
            "Cur": "Вал",
            "Currencies": "Валюты",
            "DELETE TEAM": "Удалить команду",
            "DEPOSIT": "ДЕПОЗИТ",
            "DEPOSIT USD": "ДЕПОЗИТ $",
            "Deposit the WR balance to participate the premium tournaments": "Пополнить свой счет  для игры в премиум турнирах",
            "Description": "Описание",
            "Dota2TM is a registered trademark of Valve Inc ®": "Dota2TM является зарегистрированной торговой маркой Valve Inc ®",
            "Drag and drop a team logo here or click. <br> Max size - 100KB": "Перетащи лого команды сюда. <br> Максимальный размер - 100KБ",
            "Drag and drop or click to replace": "Перетащи лого для замены",
            "Drag player from bench here": "Перемести игрока со скамьи запасных",
            "Edit Tournament": "Редактировать турнир",
            "Elo rating": "ELO рейтинг",
            "Empty slot": "Пустой слот",
            "Enter": "Войти",
            "Enter Dota 2": "Войти в Dota2",
            "Enter correct value of payment account number": "Введи правильный номер кошелька",
            "Enter payment account number": "Введи номер кошелька",
            "Enter the game and wait until you receive an invitation to the practice lobby.": "Войди в игру и подожди, пока придет приглашение в лобби. ",
            "Enter the lobby": "Войти в лобби",
            "Enter tournament details": "Посмотреть информацию о турнире",
            "Enter your team name": "Введи название вашей команды",
            "Enter your team tag, 5 symbols": "Введи тэг команды, до 5 символов",
            "Entry Fee": "Вступительный взнос",
            "Error": "Ошибка",
            "Expand": "Развернуть",
            "FAQ": "FAQ",
            "Final": "Финал",
            "Follow facebook": "Следи в facebook",
            "Follow steam group": "Заходи в группу steam",
            "General Info": "Информация",
            "Get a chance to win $50 by Winranger!": "Прими участие в розыгрыше 50$ от winranger",
            "Hide running tournaments": "Скрыть турниры в прогрессе ",
            "I've carefully read and agre with the": "Я внимательно прочитал и согласен с",
            "If the tournament is canceled due to tech reasons you get 100% chargeback.": "Если турнир был отменен - твой взнос возвращается в 100% размере.",
            "If the tournament is launched you can’t get the chargeback.": "Если турнир запустился - возможности вернуть взнос не будет.",
            "If you disconnected, You can join to the game again with the Dota 2 client<br>": "Если ты отключился - можешь присоединиться к игре в клиенте Dota 2",
            "If you don’t enter the game lobby and don’t choose your side you get the tech loose.": "Если ты не явишься на игру - тебе будет засчитано техническое поражение и взнос не вернется.",
            "If you leave the tournament until it starts you get 100% chargeback.": "Если ты выйдешь из турнира до его старта - твой взнос возвращается в 100% размере.",
            "In addition to the options above we could send you the notifications via email about the tournament-related activity": "Как дополнительная возможность - мы можем отсылать уведомления о турнирах тебе на Email",
            "Input the wallet num and validate it": "Введи номер кошелька и проверь его",
            "Instant": "Мгновенно",
            "Instant Launch": "Мгновенный запуск",
            "Invitation Period": "Период приглашений",
            "JOIN": "ВОЙТИ",
            "Join VK group": "Вступай в группу в Вконтакте",
            "Just be ready this time and launch Dota 2;)": "Просто будь готов к этому времени запустить Dota 2 ;)",
            "LEAVE": "ПОКИНУТЬ",
            "LEAVE TEAM": "ПОКИНУТЬ КОМАНДУ",
            "Launch Dota2 and start the game": "Запустить Dota2 и начать игру",
            "Leave": "Покинуть",
            "Leave the tournament": "Покинуть турнир",
            "Let your friends know about us": "Расскажи о нас друзьям",
            "Let's play": "Играть",
            "Lobby Name": "Название лобби",
            "Log out of the system": "Выйти из системы",
            "Look and Design": "Вид и дизайн",
            "Main Roster": "Основной состав",
            "Match Settings": "Настройки матча",
            "Match in progress": "Матч в процессе",
            "My Tournaments": "Мои турниры",
            "My team": "Моя команда",
            "NOTIFICATIONS": "УВЕДОМЛЕНИЯ",
            "Name": "Имя",
            "New": ["Новое", "Новых", "Новых"],
            "Now": "Сейчас",
            "Only team captain can make a play request": "Только капитан может регистрироваться в турнире",
            "Participants": "Участники",
            "Participate the tournament": "Принять участие в турнире",
            "Passed": "Прошедший",
            "Password": "Пароль",
            "Permissions": "Разрешения",
            "Players": "Игроки",
            "Please deposit and try again.": "Пополни кошелек и попробуй заново",
            "Please turn ON to get the notifications when the tournament matches begin. P.S. We don't use them for nothing else": "Включи для того, чтоб получать уведомления о начале турнира",
            "Preparing": "Подготовка",
            "Prize": "Приз",
            "Prize Distribution": "Распределение призовых",
            "Prize Type": "Тип призовых",
            "Prize pool": "Призовой фонд",
            "Prizes": "Призовой фонд",
            "RUB": "РУБ",
            "Rake": "Рейк",
            "Read twitter": "Следи в Twitter",
            "Ready": "Готов",
            "Referral link": "Реферальная ссылка",
            "Referrals": "Рефералы",
            "Region": "Регион",
            "Register": "Войти",
            "Regular tournament": "Регулярный турнир",
            "Remove": "Удалить",
            "Restrictions to participate": "Ограничения для участия",
            "Round": "Раунд",
            "Rules description": "Описание правил",
            "Running": "Идет",
            "SMS Code": "SMS-код",
            "Semi-final": "Полуфинал",
            "Send Notifications via Email": "Отправлять уведомления по Email",
            "Send Push Notifications": "Отправлять Push-уведомления",
            "Send link to invite your teammate:": "Отправьте ссылку для приглашения тиммейта",
            "So,": "Что-же, ",
            "Solo": "Соло",
            "Solo Mode": "Соло-режим",
            "Solo players": "Соло-игроки",
            "Sorry, but tournament has already started. Please try another tournament.": "Турнир уже начался, побробуй зарегистрироваться в другом",
            "Sorry, but you can not reject play request because tournament ready to play": "Извини, турнир уже начался и ты не можешь его покинуть",
            "Sorry, there is no free slots for now. Please try another tournament.": "Извините, но слоты в этом турнире закончились. Попробуй принять участие в другом турнире",
            "Sort by": "Отсортировать по",
            "State": "Состояние",
            "Stay positive and continue playing in the next tournaments!": "Не вешай нос и желаем удачи в следующем турнире!",
            "Step 1. Choose pack": "Шаг 1. Выбери пак",
            "Step 2. Method": "Шаг 2. Способ вывода",
            "Step 3. Withdrawal": "Шаг 3. Вывод средств",
            "Step 4. Validate": "Шаг 4. Проверка",
            "Surname": "Фамилия",
            "TO DASHBOARD": "НА ГЛАВНУЮ",
            "Team": "Команда",
            "Team Editor": "Редактор Команды",
            "Team Mode": "Командный режим",
            "Team awards": "Награда команде",
            "Team name": "Название команды",
            "Team players": "Участники команды",
            "The Team Name has already taken": "Название команды уже занято",
            "The Team Tag has already taken": "Тэг команды уже занят",
            "The email is not valid": "Электронная почта не валидна",
            "The file size is too big ({{ value }} max).": "Файл слишком большой",
            "The image format is not allowed ({{ value }} only).": "Формат картинки не поддерживается ({{ value }} only).",
            "The image height is too big ({{ value }}px max).": "Высота картинки слишком большая ({{ value }}px max).",
            "The image height is too small ({{ value }}}px min).": "Высота картинки слишком маленькая ({{ value }}}px min).",
            "The image width is too big ({{ value }}}px max).": "Ширина картинки слишком большая ({{ value }}}px max).",
            "The image width is too small ({{ value }}}px min).": "Ширина картинки слишком маленькая ({{ value }}}px min).",
            "The link was copied to clipboard!": "Ссылка была скопирована в буфер",
            "The name is not valid": "Имя не валидно",
            "The skype is not valid": "Логин не валидный",
            "The surname is not valid": "Фамилия не валидна",
            "The team name is not valid": "Название команды не валидно",
            "The team tag is not valid": "Тэг команды не валиден",
            "There is no free slots in tournament": "В этом турнире нет свободных слотов ",
            "This website is not affiliated with Steam or Valve ®": "Этот сайт никак не связан со Steam или Valve ®",
            "Time": "Время",
            "Timed": "Прошедший",
            "Timezone": "Часовой пояс",
            "To begin just register for one of the tournaments and wait until it begins.": "Для начала - просто зарегистрируйся на один из турниров и жди его запуска.",
            "Today": "Сегодня",
            "Tournament": "Турнир",
            "Tournament List": "Список турниров",
            "Tournament Policy": "Правила турнира",
            "Tournament is running...": "Турнир идет...",
            "Tournament prize pool": "Призовой фонд турнира",
            "Tournaments": "Турниры",
            "Tournaments in progress": "Турниров проходит сейчас",
            "Transactions": "Транзакции",
            "Type": "Тип",
            "UAH": "ГРН",
            "US DOLLARS": "ДОЛЛАРЫ США",
            "USD": "USD",
            "Update": "Обновить",
            "Validate": "Проверка",
            "Validate your {{vm.chosenMoneyService.title}} wallet with SMS": "Проверить твой  {{vm.chosenMoneyService.title}} кошелек с помощью SMS",
            "View Progress": "Прогресс",
            "View Results": "Результаты",
            "View match progress": "Посмотреть состояние турнира и турнирную таблицу",
            "View the info about the tournament and match": "Посмотреть состояние турнира и турнирную таблицу",
            "WITHDRAW": "ВЫВОД",
            "WITHDRAW {{vm.preWithdrawOptions.amount}} {{vm.payway.currencyCode.toUpperCase()}}": "ВЫВОД {{vm.preWithdrawOptions.amount}} {{vm.payway.currencyCode.toUpperCase()}}",
            "WR COINS": "WR COINS",
            "Wait until the Capitan add to the main roster": "Подожди, пока капитан переместит тебя в основной состав",
            "Waiting": "Ожидание",
            "Wallet": "Кошелек",
            "Watch us on twitch": "Смотри на Twitch",
            "We give the bucks for the sharing a post from our VK group!": "Даем баксы за репост из нашей группы ВК!",
            "We use a sound sygnal to inform you about the tournament matches start even if you are away from PC": "Включи звуковое уведомление о старте турнира, в случае, если ты отошел от компьютера",
            "We're sorry,": "Извини,",
            "Welcome to Winranger!": "Добро пожаловать на Winranger!",
            "Withdraw the money from your WR balance": "Вывести средства со своего счета",
            "Yesterday": "Вчера",
            "You are absent from the tournament and your team is lost": "Ты пропустил турнир и твоя команда проиграла",
            "You are already a participant": "Ты уже принимаешь участие в турнире",
            "You are already registered for the tournament of the same type. Cancel your registration there first to register for this tournament.": "Ты уже зарегистрирован на турнир такого же типа. Отмени регистрацию, чтоб принять участие в этом турнире",
            "You are leave this tournament many times, wait": "Ты покинул турнир слишком много раз, подожди",
            "You have": "У тебя есть",
            "You have a low balance": "У тебя недостаточно средств на счету",
            "You session has expired": "Твоя сессия истекла",
            "You successfully join the tournament": "Ты успешно присоединился к турниру!",
            "You successfully leave the tournament": "Ты успешно покинул турнир!",
            "You was kicked from tournament": "Ты был исключен из турнира",
            "You've been invited to <b>{{ vm.team.name }}</b> team": "Вас пригласили в команду  <b>{{ vm.team.name }}</b>",
            "Your Logo": "Лого",
            "Your Tournament is Ready!": "Твой турнир готов!",
            "Your Tournaments": "Твои турниры",
            "Your email address for notifications": "Твой email для уведомлений",
            "Your match is ready!": "Твоя игра готова!",
            "Your match is running!": "Твой матч в процессе",
            "Your opponent is absent from the tournament": "Твой соперник пропустил турнир",
            "Your skype account login": "Твой логин в Skype",
            "Your team is already a participant": "Твоя команда уже является участником",
            "Your team lost...": "Твоя команда проиграла…",
            "Your team name": "Название команды",
            "Your team tag": "Тэг команды",
            "Your team won!": "Твоя команда победила!",
            "Your wallet balance is low": "Баланс твоего кошелька низкий",
            "Your wallet balance is low (total: {{total}}). Please deposit and try again.": "Баланс твоего кошелька низкий (total: {{total}}). Сделай депозит и попробуй снова",
            "accept": "принять",
            "average duration": "средняя продолжительность",
            "begins": "начало",
            "brackets": "сетка",
            "current match": "текущий матч",
            "free": "беспл.",
            "general info": "основная информация",
            "get": "получи ",
            "get 10% fee!": "получи 10% от депозита",
            "hour": ["час", "часа", "часов"],
            "let your friends deposit": "предложи друзьям задонатить",
            "matches count": "количество раундов",
            "min(s) for register again": "мин, чтоб ты снова мог зарегистрироваться",
            "minute": ["минута", "минуты", "минут"],
            "participants": "участники",
            "partipants": "участники",
            "reject": "отклонить",
            "teams": "команды",
            "terms & conditions": "условия и положения",
            "to accepting.": "для принятия."
        });
        /* jshint +W100 */
    }]);
