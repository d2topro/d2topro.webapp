(function () {
    'use strict';

    angular
        .module('app.account')
        .controller('TranslateMockController', mockController);

    function mockController(gettextCatalog) {
        gettextCatalog.getString('You are leave this tournament many times, wait');
        gettextCatalog.getString(' min(s) for register again');
        gettextCatalog.getString('The link was copied to clipboard!');
        gettextCatalog.getString('You are already registered for the tournament of the same type. Cancel your registration there first to register for this tournament.');
        gettextCatalog.getString('Sorry, there is no free slots for now. Please try another tournament.');
        gettextCatalog.getString('Your wallet balance is low');
        gettextCatalog.getString(' Please deposit and try again.');
        gettextCatalog.getString('You successfully join the tournament');
        gettextCatalog.getString('You successfully leave the tournament');
        gettextCatalog.getString('Tournament');
        gettextCatalog.getString('Error');
        gettextCatalog.getString('Your wallet balance is low (total: {{total}}). Please deposit and try again.');
        gettextCatalog.getString('Sorry, but tournament has already started. Please try another tournament.');
        gettextCatalog.getString('You are already a participant');
        gettextCatalog.getString('Your team is already a participant');
        gettextCatalog.getString('There is no free slots in tournament');
        gettextCatalog.getString('You have a low balance');
        gettextCatalog.getString('Only team captain can make a play request');
        gettextCatalog.getString('Sorry, but you can not reject play request because tournament ready to play');
        // gettextCatalog.getString('');
    }
})();
