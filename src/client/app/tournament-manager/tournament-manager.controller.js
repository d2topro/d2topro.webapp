(function () {
	'use strict';

	angular
		.module('app.tournament-manager')
		.controller('TournamentManagerController', controller);

	/* @ngInject */
	function controller(tournament) {
		var vm = this;
		vm.tournament = tournament;
		vm.isEditingAdvancedOptions = false;
		vm.primaryOpts = primaryOpts;
		vm.advancedOpts = advancedOpts;

		activate();

		function activate() {

		}

		function primaryOpts() {
			vm.isEditingAdvancedOptions = false;
		}

		function advancedOpts() {
			vm.isEditingAdvancedOptions = true;
		}
	}

})();
