(function () {
	'use strict';

	angular
		.module('app.tournament-manager')
		.run(appRun);

	/* @ngInject */
	function appRun(routerHelper, translate) {
		routerHelper.configureStates(getStates());

		function getStates() {
			return [{
				state: 'root.tournament-manager',
				config: {
					url: '/tournament-manager',
					template: '<ui-view class="shuffle-animation"/>',
					abstract: true
				}
			}, {
				state: 'root.tournament-manager.create',
				config: {
					url: '/create',
					templateUrl: 'app/tournament-manager/manager/create.html',
					controller: 'TournamentManagerController',
					controllerAs: 'vm',
					title: translate('Create Tournament'),
					settings: {
						// nav: 11,
						// icon: 'icons8-trophy',
						// content: translate('Create Tournament')
					},
					resolve: {
						/* @ngInject */
						tournament: function (TournamentManagerService) {
							return TournamentManagerService.getDefaults();
						}
					}
				}
			}];
		}
	}
})();
