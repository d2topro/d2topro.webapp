(function () {
	'use strict';

	angular
		.module('app.tournament-manager')
		.factory('TournamentManagerService', factory);

	/* @ngInject */
	function factory(ManageTournamentResource) {
		return {
			getDefaults: getDefaults
		};

		function getDefaults() {
			return new ManageTournamentResource({
				id: null,
				policy_id: null,
				currency_id: null,
				reward_currency_id: null,
				game_app_id: 1,
				name: 'My Awesome Tournament',
				fee_amount: 10,
				prize_pool_amount: 0,
				participants_count: 2,
				is_solo: true,
				is_cooldown: true,
				starts_at: null,
				comission: 0,
				reward_rates: [0.4, 0.3, 0.15],
				meta: {
					// video: { id: 'qWERQza21', type: 'youtube' }
				},
				image: {
					large: 'https://s3.eu-central-1.amazonaws.com/play.winranger.com/' +
						'images/pages-bg/dashboard-bg.jpg'
				},
				policy: {
					// elo_rank: [1000, 4000],
					// level: [1300, 4100],
					// leave_rate: [10, 30],
					// system: 0,
					game_id: 570,
					regular_series_count: 1,
					final_series_count: 3,
					max_match_duration: 10800,
					match_start: 0,
					lobby_expiration: 3000,
					max_players_count: 5,
					min_players_count: 2,
					is_players_count_equal: true,
					// has_password: true,
					server_region: 3,
					game_mode: 1,
					cm_pick: 0,
					allow_spectating: true,
					strict_mode: true
				}
			});
		}
	}

})();
