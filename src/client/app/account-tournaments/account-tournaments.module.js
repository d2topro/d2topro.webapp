(function() {
    'use strict';

    angular.module('app.account-tournaments', [
        'app.core'
    ]);

})();
