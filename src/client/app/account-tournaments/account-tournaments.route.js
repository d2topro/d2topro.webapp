(function () {
    'use strict';

    angular
        .module('app.account-tournaments')
        .run(appRun);

    /* @ngInject */
    function appRun(routerHelper, gettextCatalog) {
        routerHelper.configureStates(getStates());

        function getStates() {
            return [{
                state: 'root.account-tournaments',
                config: {
                    url: '/account/tournaments',
                    templateUrl: 'app/account-tournaments/account-tournaments.html',
                    title: gettextCatalog.getString('My Tournaments'),
                    settings: {
                        // nav: 2,
                        // icon: 'wb-dashboard',
                        // content: gettextCatalog.getString('My Tournaments')
                    }
                }
            }];
        }
    }
})();
