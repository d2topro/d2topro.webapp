(function () {
    'use strict';

    angular
        .module('app.teams')
        .factory('TeamService', service);

    /* @ngInject */
    function service($intercom, TeamResource, AccountTeamPlayerResource) {
        var promises = {};
        return {
            getTeam: getTeam,
            join: join
        };

        function fetch(id) {
            promises[id] = TeamResource.get({
                    id: id
                })
                .$promise;

            return promises[id];
        }

        function getTeam(id) {
            return promises[id] ? promises[id] : fetch(id);
        }

        function join(teamId, code) {
            return getTeam(teamId)
                .then(function (team) {
                    $intercom('trackEvent', 'WR_JOINED_TEAM', {
                        id: team.id,
                        name: team.name
                    });

                    return AccountTeamPlayerResource.join({
                            team_id: teamId,
                            code: code
                        })
                        .$promise;
                });
        }
    }
})();
