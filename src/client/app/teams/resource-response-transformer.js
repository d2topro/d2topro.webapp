(function () {
    'use strict';

    angular
        .module('app.teams')
        .factory('TeamResourceResponseTransformer', transformer);

    /* @ngInject */
    function transformer(AccountTeamPlayerResource, DEFAULT_TEAM_LOGO) {
        return {
            transform: transform
        };

        function transform(data) {
            var response = angular.fromJson(data),
                teams = response instanceof Array ? response : [response];

            angular.forEach(teams, function (team) {
                if (!team.logo.url || team.logo.url === '') {
                    team.logo.url = DEFAULT_TEAM_LOGO;
                }

                angular.forEach(team.members, function (member) {
                    member.TeamPlayer =
                        new AccountTeamPlayerResource(member.TeamPlayer);
                });
            });

            return response;
        }
    }
})();
