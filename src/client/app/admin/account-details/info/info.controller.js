(function() {
    'use strict';

    angular
        .module('app.admin')
        .controller('AdminAccountDetailsInfoController', controller);

    /* @ngInject */
    function controller() {
        var vm = this;
        vm.account = {
            name: '',
            surname: '',
            email: '',
            company: '',
            country: '',
            timezone: ''
        };
    }
})();
