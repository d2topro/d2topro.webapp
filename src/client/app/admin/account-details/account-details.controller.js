(function() {
    'use strict';

    angular
        .module('app.admin')
        .controller('AdminAccountDetailsController', controller);

    /* @ngInject */
    function controller($state, $scope) {
        var vm = this;
        vm.viewMode = $state.current.name;

        var tabsListener = $scope.$watch(function() {
            return $state.current.name;
        }, function(newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            }

            vm.viewMode = newValue;
        });

        $scope.$on('destroy', tabsListener);
    }
})();
