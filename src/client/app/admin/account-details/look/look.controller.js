(function() {
    'use strict';

    angular
        .module('app.admin')
        .controller('AdminAccountDetailsLookController', controller);

    /* @ngInject */
    function controller($scope) {
        return $scope;
    }
})();
