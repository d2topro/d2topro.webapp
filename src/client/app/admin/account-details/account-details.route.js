(function () {
	'use strict';

	angular
		.module('app.admin')
		.run(appRun);

	/* @ngInject */
	function appRun(routerHelper, gettextCatalog) {
		routerHelper.configureStates(getStates());

		function getStates() {
			return [{
				state: 'root.admin.account.info',
				config: {
					url: '/info',
					templateUrl: 'app/admin/account-details/info/info.html',
					controller: 'AdminAccountDetailsInfoController',
					controllerAs: 'vm',
					title: gettextCatalog.getString('Account Info'),
					settings: {
						// nav: 1,
						// restrict: true,
						// icon: 'wb-dropright',
						// content: gettextCatalog.getString('Account Info')
					}
				}
            }, {
				state: 'root.admin.account.look',
				config: {
					url: '/look',
					templateUrl: 'app/admin/account-details/look/look.html',
					controller: 'AdminAccountDetailsLookController',
					controllerAs: 'vm',
					title: gettextCatalog.getString('Look and Design'),
					settings: {
						// nav: 1,
						// restrict: true,
						// icon: 'wb-dropright',
						// content: gettextCatalog.getString('Look and Design')
					}
				}
            }, ];
		}
	}
})();
