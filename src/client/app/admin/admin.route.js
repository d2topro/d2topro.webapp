(function () {
	'use strict';

	angular
		.module('app.admin')
		.run(appRun);

	/* @ngInject */
	function appRun(routerHelper) {
		routerHelper.configureStates(getStates());

		function getStates() {
			return [{
				state: 'root.admin',
				config: {
					url: '/admin',
					template: '<ui-view class="shuffle-animation"/>',
					abstract: true,
					settings: {}
				}
            }, {
				state: 'root.admin.tournaments',
				config: {
					url: '/tournaments',
					template: '<ui-view class="shuffle-animation"/>',
					abstract: true,
					settings: {}
				}
            }, {
				state: 'root.admin.account',
				config: {
					url: '/account',
					controller: 'AdminAccountDetailsController',
					controllerAs: 'vm',
					templateUrl: 'app/admin/account-details/account-details.html',
					abstract: true,
					settings: {}
				}
            }];
		}
	}
})();
