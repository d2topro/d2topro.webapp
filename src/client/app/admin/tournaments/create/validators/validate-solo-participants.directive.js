(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .directive('validateSoloParticipants', validateSoloParticipants);

    /* @ngInject */
    function validateSoloParticipants(lodash) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                min: '=',
                max: '='
            },
            link: function (scope, elem, attr, ctrl) {
                function customValidator(ngModelValue) {
                    // @TODO uncomment when need it
                    // var validationRange = scope.min === scope.max ?
                    //     [Math.log2(ngModelValue / scope.min) % 1 === 0] :
                    //     lodash
                    //         .range(scope.min, scope.max)
                    //         .map(function(index) {
                    //             return Math.log2(ngModelValue / index) % 1 === 0
                    //         });
                    //
                    // if (lodash.compact(validationRange).length > 0) {
                    //     ctrl.$setValidity('soloDegreeValidator', true);
                    // } else {
                    //     ctrl.$setValidity('soloDegreeValidator', false);
                    // }

                    if ((ngModelValue / 2) % 1 === 0) {
                        ctrl.$setValidity('multipleOfTwo', true);
                    } else {
                        ctrl.$setValidity('multipleOfTwo', false);
                    }

                    if (ngModelValue >= scope.min * 2) {
                        ctrl.$setValidity('soloValueValidator', true);
                    } else {
                        ctrl.$setValidity('soloValueValidator', false);
                    }

                    return ngModelValue;
                }

                ctrl.$parsers.push(customValidator);
            }
        };
    }
})();
