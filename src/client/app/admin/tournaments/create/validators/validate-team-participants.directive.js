(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .directive('validateTeamParticipants', valTeamParticipants);

    /* @ngInject */
    function valTeamParticipants() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attr, ctrl) {
                function customValidator(ngModelValue) {
                    if (Math.log2(ngModelValue) % 1 === 0) {
                        ctrl.$setValidity('teamDegreeValidator', true);
                    } else {
                        ctrl.$setValidity('teamDegreeValidator', false);
                    }

                    if (ngModelValue > 1) {
                        ctrl.$setValidity('teamValueValidator', true);
                    } else {
                        ctrl.$setValidity('teamValueValidator', false);
                    }

                    return ngModelValue;
                }

                ctrl.$parsers.push(customValidator);
            }
        };
    }
})();
