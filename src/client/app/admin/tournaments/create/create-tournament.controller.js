(function () {
    'use strict';

    angular
        .module('app.tournaments')
        .controller('AdminCreateTournamentController', controller);

    /* @ngInject */
    function controller($scope, lodash, gettextCatalog,
        CreateTournamentService, tournament) {

        var vm = this;
        var service = CreateTournamentService;
        vm.attributes = tournament ? tournament : service.getAttributes();
        vm.setPrizePool = setPrizePool;
        vm.setPlaces = setPlaces;
        vm.changePlaceReward = changePlaceReward;
        vm.prizeDistibutionIsValid = prizeDistibutionIsValid;
        vm.switchTo = switchTo;

        activate();

        function activate() {
            switchTo('generalInfo');
            setPrizePool();
            setPlaces();
        }

        function switchTo(step) {
            vm.step = step;
        }

        function setPlaces() {
            vm.places = service.setPlaces(vm.attributes);
        }

        function setPrizePool() {
            vm.attributes.prize_pool_amount =
                service.setPrizePool(vm.attributes);
        }

        function prizeDistibutionIsValid() {
            return service.prizeDistibutionIsValid(vm.places);
        }

        function changePlaceReward(index, value) {
            var place = lodash.find(vm.places, function (place) {
                return place.index === index;
            });
            place.reward = (vm.attributes.prize_pool_amount / 100) * value;
        }

    }
})();
