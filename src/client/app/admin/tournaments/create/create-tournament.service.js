(function() {
    'use strict';

    angular
        .module('app.tournaments')
        .service('CreateTournamentService', createTournamentService);

    /* @ngInject */
    function createTournamentService(TournamentDetailsService, lodash) {
        return {
            getAttributes: getAttributes,
            setPlaces: setPlaces,
            setPrizePool: setPrizePool,
            prizeDistibutionIsValid: prizeDistibutionIsValid
        }

        function getAttributes() {
            return {
                currency_id: 1,
                name: "",
                fee_amount: 10,
                prize_pool_amount: 0,
                participants_count: 2,
                is_solo: true,
                is_cooldown: true,
                starts_at: null,
                comission: 0,
                reward_rates: [
                    0.4,
                    0.3,
                    0.15
                ],
                image: {
                    large: "https://s3.eu-central-1.amazonaws.com/play.winranger.com/images/pages-bg/dashboard-bg.jpg"
                },
                game_app_id: 1,
                policy: {
                    elo_rank: [1000, 4000],
                    level: [1300, 4100],
                    leave_rate: [10, 30],
                    system: 0,
                    game_id: 570,
                    regular_series_count: 1,
                    final_series_count: 3,
                    max_match_duration: 10800,
                    match_start: 0,
                    lobby_expiration: 3000,
                    max_players_count: 5,
                    min_players_count: 2,
                    is_players_count_equal: true,
                    has_password: true,
                    server_region: 3,
                    game_mode: 1,
                    cm_pick: 0,
                    allow_spectating: true,
                    strict_mode: true
                }
            }
        }

        function setPlaces(attributes) {
            var n = Math.log2(attributes.participants_count);

            return lodash.range(n + 1).map(function(index) {
                return {
                    index: index,
                    title: TournamentDetailsService.getPlaceTitle(index),
                    value: null,
                    reward: 0
                }
            })
        }

        function setPrizePool(attributes) {
            var total = attributes.participants_count * attributes.fee_amount;
            return total - (total / 100) * attributes.comission;
        }

        function prizeDistibutionIsValid(places) {
            var total = lodash.reduce(places, function(sum, place) {
                if (place.value) {
                    sum += parseInt(place.value);
                }

                return sum;
            }, 0);

            return total === 100;
        }
    }
})();
