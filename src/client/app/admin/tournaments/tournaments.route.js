(function () {
	'use strict';

	angular
		.module('app.admin')
		.run(appRun);

	/* @ngInject */
	function appRun(routerHelper, gettextCatalog) {
		routerHelper.configureStates(getStates());

		function getStates() {
			return [{
					state: 'root.admin.tournaments.create',
					config: {
						url: '/create',
						templateUrl: 'app/admin/tournaments/create/create-tournament.html',
						controller: 'AdminCreateTournamentController',
						controllerAs: 'vm',
						title: gettextCatalog.getString('Create Tournament'),
						resolve: {
							/* @ngInject */
							tournament: function () {
								return null;
							}
						},
						settings: {
							// nav: 1,
							// restrict: true,
							// icon: 'wb-dropright',
							// content: gettextCatalog.getString('Create Tournament')
						}
					}
                },
				{
					state: 'root.admin.tournaments.edit',
					config: {
						url: '/:id/edit',
						templateUrl: 'app/admin/tournaments/create/create-tournament.html',
						controller: 'AdminCreateTournamentController',
						controllerAs: 'vm',
						title: gettextCatalog.getString('Edit Tournament'),
						resolve: {
							/* @ngInject */
							tournament: function ($stateParams, TournamentService) {
								return TournamentService
									.getTournament($stateParams.id);
							}
						},
						settings: {
							restrict: true
						}
					}
                },
				{
					state: 'root.admin.tournaments.list',
					config: {
						url: '/list',
						templateUrl: 'app/admin/tournaments/list/tournament-list.html',
						controller: 'AdminTournamentListController',
						controllerAs: 'vm',
						title: gettextCatalog.getString('Tournament List'),
						settings: {
							// nav: 1,
							// restrict: true,
							// icon: 'wb-dropright',
							// content: gettextCatalog.getString('Tournament List')
						}
					}
                }
            ];
		}
	}
})();
