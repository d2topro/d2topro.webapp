(function() {
    'use strict';

    angular
        .module('app.admin')
        .controller('AdminTournamentListController', controller);

    /* @ngInject */
    function controller($state, lodash, TournamentService) {
        var vm = this;
        vm.tournaments = [];
        vm.isSoloMode = true;
        vm.isCooldownMode = true;
        vm.goToEditTournament = goToEditTournament;

        activate();

        function activate() {
            return TournamentService
                .getTournaments()
                .then(function(tournaments) {
                    vm.tournaments = toSort(tournaments);
                })
        }

        function goToEditTournament(id) {
            $state.go('admin.tournaments.edit', {
                id: id
            });
        }

        function toSort(transactions) {
            return lodash
                .chain(transactions)
                .sort(function(t1, t2) {
                    return (
                        new Date(t2.created_at)) - (new Date(t1.created_at)
                    );
                })
                .groupBy(function(t) {
                    return
                        new Date(t.created_at).toLocaleDateString();
                })
                .values()
                .value();
        }
    }
})();
