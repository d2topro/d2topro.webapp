(function () {
    'use strict';

    angular
        .module('app.wallet')
        .controller('WalletDepositController', walletDepositController);

    /* @ngInject */
    function walletDepositController($uibModalInstance, $window, DepositService) {
        var vm = this;
        vm.close = close;
        vm.payGateway = payGateway;
        vm.data = {
            value: 5,
            step: 5,
            min: 5,
            max: 100,
        };

        activate();

        function close() {
            return $uibModalInstance.close();
        }

        function activate() {
            return DepositService.getOptions()
                .then(function (deposit) {
                    vm.data = deposit;
                    return deposit;
                });
        }

        function payGateway() {
            return DepositService
                .getPayGateway(vm.data.value)
                .then(function (response) {
                    $window.location.href = response.url;
                    return;
                });
        }
    }
})();
