/**
 * Created by DTuzenkov on 1/14/16.
 */

(function () {
    'use strict';

    angular
        .module('app.wallet')
        .controller('WalletWithdrawController', walletWithdrawController);

    /* @ngInject*/
    function walletWithdrawController($uibModalInstance, $log, lodash,
        WithdrawService) {
        var vm = this;
        vm.data = {
            value: 10,
            limit: [10, 100],
            step: 5,
            payway: []
        };
        vm.account = null;
        vm.payway = null;

        vm.currency = 'RUB';
        vm.getPaywaysByCurrency = getPaywaysByCurrency;
        vm.selectPaywayByCurrency = selectPaywayByCurrency;
        vm.withdraw = withdraw;
        vm.preWithdraw = preWithdraw;
        vm.close = close;
        vm.step = null;
        vm.first = first;
        vm.second = second;
        vm.third = third;
        vm.paywayDetails = WithdrawService.getPaywayDetails();
        vm.paywayValidator = WithdrawService.validatePayway;

        activate();

        function activate() {
            return WithdrawService.getOptions()
                .then(function (response) {
                    vm.step = 'first';
                    vm.data = response;
                    return response;
                })
                .catch(function (cause) {
                    $log.error(cause.message);
                });
        }

        function first() {
            vm.step = 'first';
        }

        function second() {
            vm.step = 'second';
        }

        function third(payway) {
            vm.step = 'third';
            vm.payway = payway;
            return preWithdraw();
        }

        function selectPaywayByCurrency(currency) {
            vm.currency = currency;
        }

        function getPaywaysByCurrency() {
            return lodash
                .chain(vm.data.payway)
                .filter(function (payway) {
                    var parts = payway.code.split('_');
                    return parts.pop() === vm.currency.toLowerCase();
                })
                .each(function (payway) {
                    var parts = payway.code.split('_');
                    payway.currencyCode = parts.pop();
                    payway.vendorCode = parts.join('_');
                })
                .value();

        }

        function preWithdraw() {
            return WithdrawService
                .preWithdraw(vm.payway.code, vm.data.value)
                .then(function (response) {
                    if (response.error) {
                        return $log.error(response.error.message);
                    }

                    vm.preWithdrawOptions = response;
                    return response;
                })
                .catch(function (cause) {
                    $log.error(cause.message);
                });
        }

        function withdraw() {
            return WithdrawService
                .withdraw(vm.payway.code, vm.account, vm.data.value)
                .then(function (response) {
                    if (response.error) {
                        return $log.error(response.error.message);
                    }

                    return close();
                })
                .catch(function (cause) {
                    $log.error(cause.message);
                });
        }

        function close() {
            $uibModalInstance.close();
        }
    }
})();
