(function () {
    'use strict';

    angular
        .module('app.wallet')
        .directive('paywayAccountValidatorDirective', validator);

    /* @ngInject */
    function validator(WithdrawService) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$validators.paywayAccountValidator = function (modelValue, viewValue) {
                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty models to be valid
                        return true;
                    }

                    return WithdrawService.validatePayway(attrs.paywayAccountValidatorDirective, modelValue);
                };
            }
        };
    }

})();
