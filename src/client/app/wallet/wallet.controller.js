(function() {
    'use strict';

    angular
        .module('app.wallet')
        .controller('WalletController', walletController);

    /* @ngInject*/
    function walletController($uibModal, $q, $scope, logger, moment,
        BalanceService, TransactionService, SocketService) {

        var vm = this;
        vm.moment = moment;
        vm.transactions = [];
        vm.balanceWRC = 0;
        vm.balanceUSD = 0;
        vm.showWithdrawModal = showWithdrawModal;
        vm.showBuyModal = showBuyModal;
        vm.isCanWithdraw = false;

        SocketService.on('tm:complete', function() {
            return BalanceService.fetch();
        });

        SocketService.on('tm:match:complete', function() {
            return BalanceService.fetch();
        });

        var balanceListener = $scope.$watch(
            function() {
                return BalanceService.getBalance();
            },
            function(newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                return newValue.then(function(balance) {
                    vm.balanceWRC = BalanceService.getWRC(balance);
                    vm.balanceUSD = BalanceService.getUSD(balance);
                });
            }
        );

        var transactionsListener = $scope.$watch(
            function() {
                return TransactionService.getTransactions();
            },
            function(newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                return newValue.then(function(transactions) {
                    vm.transactions = TransactionService.toSort(transactions);
                });
            }
        );

        $scope.$on('destroy', function() {
            balanceListener();
            transactionsListener();
        });

        activate();

        function activate() {
            return $q
                .all({
                    transactions: TransactionService.getTransactions(),
                    balance: BalanceService.getBalance()
                })
                .then(function(response) {
                    vm.balanceWRC = BalanceService.getWRC(response.balance);
                    vm.balanceUSD = BalanceService.getUSD(response.balance);

                    vm.transactions = TransactionService
                        .toSort(response.transactions);
                })
                .catch(function(cause) {
                    // TODO add console log instead of logger
                    logger.error(cause);
                })
                .then(function() {
                    vm.isCanWithdraw = (vm.balanceUSD >= 10) ? true : false;
                });
        }

        function showWithdrawModal() {
            if (!vm.isCanWithdraw) {
                return;
            }
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/wallet/withdraw/withdraw-modal.html',
                controller: 'WalletWithdrawController',
                controllerAs: 'vm',
                size: '',
                backdropClass: 'BACKDROP_CLASS',
                windowTopClass: 'wallet-withdraw-modal',
                resolve: {}
            });
        }

        function showBuyModal() {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/wallet/deposit/deposit-modal.html',
                controller: 'WalletDepositController',
                controllerAs: 'vm',
                size: '',
                backdropClass: 'BACKDROP_CLASS',
                windowTopClass: 'wallet-deposit-modal',
                resolve: {
                    account: vm.account
                }
            });
        }
    }

})();
