(function () {
    'use strict';

    angular
        .module('app.wallet')
        .factory('DepositService', service);

    /* @ngInject */
    function service($log, WalletDepositResource) {
        var promise;

        return {
            fetch: fetch,
            deposit: deposit,
            getPayGateway: getPayGateway,
            getOptions: getOptions
        };

        function fetch() {
            promise = WalletDepositResource.get()
                .$promise;

            return promise
                .then(function (resource) {
                    return resource;
                })
                .catch(function (cause) {
                    return $log.error(cause);
                });
        }

        function getOptions() {
            return promise ? promise : fetch();
        }

        function deposit(value) {
            return WalletDepositResource.post({
                    amount: value
                })
                .$promise;
        }

        function getPayGateway(value) {
            return WalletDepositResource.payGateway({
                    amount: value
                })
                .$promise;
        }
    }
})();
