(function () {
    'use strict';

    angular
        .module('app.wallet')
        .factory('CurrencyService', service);

    /* @ngInject */
    function service($log, lodash, CurrencyResource) {
        var promise;

        return {
            fetch: fetch,
            getCurrency: getCurrency,
            getCurrencies: getCurrencies
        };

        function fetch() {
            promise = CurrencyResource.query()
                .$promise;

            return promise
                .then(function (currencies) {
                    return currencies;
                })
                .catch(function (cause) {
                    return $log.error(cause);
                });
        }

        function getCurrencies() {
            return promise ? promise : fetch();
        }

        function getCurrency(id) {
            return getCurrencies()
                .then(function (currencies) {
                    return lodash.find(currencies, function (c) {
                        return c.id === id;
                    });
                });
        }
    }
})();
