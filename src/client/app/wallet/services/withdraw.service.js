(function () {
    'use strict';

    angular
        .module('app.wallet')
        .factory('WithdrawService', service);

    /* @ngInject */
    function service($log, lodash, WalletWithdrawResource) {
        var promise;
        var paywayDetails = {
            card: {
                name: 'Visa & MasterCard',
                account: 'Enter card number'
            },
            privat_card: {
                name: 'Privat 24',
                account: 'Enter card number'
            },
            yamoney: {
                name: 'Yandex Money',
                account: 'Enter Yandex Money account number',
            },
            qiwi: {
                name: 'Qiwi',
                account: 'Enter Qiwi account numumber',
            }
        };

        return {
            fetch: fetch,
            withdraw: withdraw,
            getOptions: getOptions,
            preWithdraw: preWithdraw,
            validatePayway: validatePayway,
            getPaywayDetails: getPaywayDetails,
        };

        function fetch() {
            promise = WalletWithdrawResource.get()
                .$promise;

            return promise
                .then(function (resource) {
                    return resource;
                })
                .catch(function (cause) {
                    return $log.error(cause);
                });
        }

        function getOptions() {
            return promise ? promise : fetch();
        }

        function withdraw(payway, account, amount) {
            return WalletWithdrawResource.withdraw({
                    payway: payway,
                    account: account,
                    amount: amount
                })
                .$promise;
        }

        function preWithdraw(payway, amount) {
            return WalletWithdrawResource.preWithdraw({
                    payway: payway,
                    amount: amount
                })
                .$promise;
        }

        function getPaywayDetails() {
            return paywayDetails;
        }

        function validatePayway(payway, account) {
            switch (payway) {
            case 'card_uah':
                return !isPrivate24Card(account) && (/^\d{16}$/)
                    .test(account);

            case 'card_privat_uah':
                return isPrivate24Card(account) && (/^\d{16}$/)
                    .test(account);

            case 'card_rub':
                return (/^\d{16,18}$/)
                    .test(account);

            case 'webmoney_rub':
                return (/^R\d{12}$/)
                    .test(account);

            case 'qiwi_usd':
            case 'qiwi_rub':
            case 'qiwi_uah':
                return (/^\d{9,15}$/)
                    .test(account);

            case 'yamoney_rub':
                return (/^41001\d*$/)
                    .test(account);

            default:
                return null;
            }
        }

        function isPrivate24Card(number) {
            var parts = [
                404030, 410653, 413051, 414939, 414943, 414949, 414960, 414961,
                414962, 414963, 417649, 423396, 424600, 424657, 432334, 432335,
                432336, 432337, 432338, 432339, 432340, 432575, 434156, 440129,
                440509, 440535, 440588, 458120, 458121, 458122, 462705, 462708,
                473114, 473117, 473118, 473121, 476065, 476339, 513399, 516798,
                516874, 516875, 516915, 516933, 516936, 517691, 521152, 521153,
                521857, 530217, 532032, 532957, 535145, 536354, 544013, 545708,
                545709, 552324, 557721, 558335, 558424, 670509, 676246
            ];

            return lodash.find(parts, function (num) {
                var str = number.toString();
                return str.substr(0, 6) === num.toString();
            });
        }


    }
})();
