(function() {
    'use strict';

    angular
        .module('app.wallet')
        .factory('TransactionService', transactionService);

    /* @ngInject */
    function transactionService($log, TransactionResource, lodash) {
        var promise;
        return {
            fetch: fetch,
            getTransactions: getTransactions,
            toSort: toSort
        };

        function fetch() {
            promise = TransactionResource.get().$promise;
            return promise
                .then(function(resource) {
                    return resource;
                })
                .catch(function(cause) {
                    return $log.error(cause);
                });
        }

        function getTransactions() {
            return promise ? promise : fetch();
        }

        function toSort(transactions) {
            return lodash
                .chain(transactions)
                .sort(function(t1, t2) {
                    return (
                        new Date(t2.created_at))
                            - (new Date(t1.created_at)
                    );
                })
                .groupBy(function(t) {
                    return
                        new Date(t.created_at).toLocaleDateString();
                })
                .values()
                .value();
          }
    }
})();
