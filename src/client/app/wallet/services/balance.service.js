(function () {
    'use strict';

    angular
        .module('app.wallet')
        .factory('BalanceService', balanceService);

    /* @ngInject */
    function balanceService($log, lodash, BalanceResource) {
        var promise;
        return {
            fetch: fetch,
            getBalance: getBalance,
            getUSD: getUSD,
            getWRC: getWRC
        };

        function fetch() {
            promise = BalanceResource.query()
                .$promise;
            return promise.then(function (resource) {
                    return resource;
                })
                .catch(function (cause) {
                    return $log.error(cause);
                });
        }

        function getBalance() {
            return promise ? promise : fetch();
        }

        function getUSD(balance) {
            var balance = lodash.find(balance, function (balance) {
                return balance.isUSD();
            });

            return balance.total;
        }

        function getWRC(balance) {
            var balance = lodash.find(balance, function (balance) {
                return balance.isWRC();
            });

            return balance.total;
        }
    }
})();
