/**
 * Created by DTuzenkov on 12/1/15.
 */
(function () {
    'use strict';
    angular
        .module('app.interceptors')
        .factory('SpinnerInterceptor', spinnerInterceptor);

    /* @ngInject */
    function spinnerInterceptor(SpinnerService) {
        return {
            request: function (configs) {
                SpinnerService.start();
                return configs;
            },
            response: function (response) {
                SpinnerService.stop();
                return response;
            },
            responseError: function (response) {
                SpinnerService.stop();
                return response;
            }
        };
    }
})();
