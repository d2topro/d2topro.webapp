(function () {
    'use strict';
    angular
        .module('app.interceptors')
        .factory('CsrfInterceptor', csrfInterceptor);

    /* @ngInject */
    function csrfInterceptor($q, $injector) {
        var csrfToken;
        return {
            request: function(configs) {
                configs.headers['X-CSRF-Token'] = csrfToken;
                return configs;
            },
            response: processCSRFToken,
            responseError: processCSRFToken
        };

        function processCSRFToken(response) {
            if (response.headers('X-CSRF-Token')) {
                csrfToken = response.headers('X-CSRF-Token');
            }
            return response;
        }
    }
})();
