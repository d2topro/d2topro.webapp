(function () {
    'use strict';
    angular
        .module('app.interceptors')
        .factory('CredentialsInterceptor', credentialsInterceptor);

    /* @ngInject */
    function credentialsInterceptor(environment) {
        return {
            request: function(config) {
                if (config.url.indexOf('/api') === 0) {
                    config.url = environment.apiUrl + config.url;
                }
                config.withCredentials = true;

                return config;
            }
        };
    }
})();
