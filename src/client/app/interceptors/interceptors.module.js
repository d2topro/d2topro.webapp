(function () {
    'use strict';

    angular
        .module('app.interceptors', ['app.core']);

    angular
        .module('app.interceptors')
        .config(['$httpProvider', function ($httpProvider) {
            // TODO: Configure those interceptors
            // $httpProvider.interceptors.push('SpinnerInterceptor');

            $httpProvider.interceptors.push('ErrorInterceptor');
            $httpProvider.interceptors.push('AuthInterceptor');
            $httpProvider.interceptors.push('CsrfInterceptor');
            $httpProvider.interceptors.push('CredentialsInterceptor');
        }]);

})();
