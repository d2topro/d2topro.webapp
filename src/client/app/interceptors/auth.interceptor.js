(function () {
    'use strict';
    angular
        .module('app.interceptors')
        .factory('AuthInterceptor', authInterceptor);

    /* @ngInject */
    function authInterceptor($injector, $q) {
        return {
            response: function (response) {
                if (response.status === 401) {
                    var AuthService = $injector.get('AuthorizationService');
                    return AuthService.goToSteamAuth();
                }

                return $q.resolve(response);
            }
        };
    }
})();
