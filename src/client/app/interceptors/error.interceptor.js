(function () {
    'use strict';
    angular
        .module('app.interceptors')
        .factory('ErrorInterceptor', interceptor);

    /* @ngInject */
    function interceptor($q) {
        return {
            response: function (response) {
                if (response.data.error) {
                    return $q.reject(response.data.error);
                }

                return $q.resolve(response);
            }
        };
    }
})();
