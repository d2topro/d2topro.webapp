(function () {
    'use strict';

    angular
        .module('app.layout')
        .factory('NotificationService', notificationService);

    /* @ngInject */
    function notificationService(AccountNotification) {
        return {
            getUnread: getUnread
        };

        function getUnread() {
            return AccountNotification.query({new: 1}).$promise;
        }
    }
})();
