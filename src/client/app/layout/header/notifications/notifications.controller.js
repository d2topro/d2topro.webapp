/**
 * Created by DTuzenkov on 12/7/15.
 */
(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('NotificationsController', notificationsController);

    /* @ngInject */
    function notificationsController($scope, $uibModal, lodash, NotificationService, SocketService) {
        var vm = this;
        vm.notifications = null;
        vm.showTeamInvitationModal = showTeamInvitationModal;
        vm.markAsRead = markAsRead;

        activate();

        function activate() {
            return NotificationService.getUnread().then(function (resources) {
                vm.notifications = resources;
            });
        }

        function showTeamInvitationModal(invitation) {
            return $uibModal.open({
                animation: false,
                backdrop: 'static',
                templateUrl: 'app/account/invitation/team-confirm-modal.html',
                controller: 'AccountInvitationTeamConfirmController',
                controllerAs: 'vm',
                size: 'sm width-400',
                backdropClass: 'BACKDROP_CLASS',
                windowTopClass: '',
                resolve: {
                    invitation: function () {
                        return invitation;
                    }
                }
            });
        }

        function markAsRead(notification) {
            lodash.remove(vm.notifications, function (item) {
                return item.id === notification.id;
            });
        }

    }
})();
