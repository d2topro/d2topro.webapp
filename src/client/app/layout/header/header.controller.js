/**
 * Created by DTuzenkov on 12/7/15.
 */
(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('HeaderController', headerController);

    /* @ngInject */
    function headerController($scope, $rootScope, $q, $timeout, $window, $log,
        AccountTeamService, AccountTeamModalService, SocketService,
        BalanceService) {

        var vm = this,
            balanceListener, teamListener;
        vm.account = null;
        vm.showAccountTeamModal = showAccountTeamModal;
        vm.isAnimate = false;
        vm.getEnvironment = getEnvironment;
        vm.balanceWRC = 0;
        vm.balanceUSD = 0;

        activate();

        SocketService.on('tm:request:create', function () {
            return BalanceService.fetch();
        });

        SocketService.on('tm:request:remove', function () {
            return BalanceService.fetch();
        });

        SocketService.on('tm:match:complete', function () {
            return BalanceService.fetch();
        });

        $rootScope.$on('wallet:animation', function () {
            vm.isAnimate = true;

            $timeout(function () {
                vm.isAnimate = false;
            }, 6000);
        });

        balanceListener = $scope.$watch(
            function () {
                return BalanceService.getBalance();
            },
            function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                return newValue.then(function (balance) {
                    vm.balanceWRC = BalanceService.getWRC(balance);
                    vm.balanceUSD = BalanceService.getUSD(balance);
                });
            }
        );

        teamListener = $scope.$watch(
            function () {
                return AccountTeamService.getTeam();
            },
            function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                vm.team = newValue;
            }
        );

        $scope.$on('destroy', function () {
            balanceListener();
            teamListener();
        });

        function getEnvironment() {
            return $window.__env.key;
        }

        function activate() {
            return $q.all({
                    team: AccountTeamService.getTeam(),
                    balance: BalanceService.getBalance()
                })
                .then(function (response) {
                    vm.balanceWRC = BalanceService.getWRC(response.balance);
                    vm.balanceUSD = BalanceService.getUSD(response.balance);
                    vm.buildVersion = window.version;
                    vm.team = response.team;
                })
                .catch(function (cause) {
                    $log.error(cause);
                });
        }


        function showAccountTeamModal() {
            return AccountTeamService.getTeam()
                .then(function (team) {
                    return team ?
                        AccountTeamModalService.showAccountTeamModal() :
                        AccountTeamModalService.showAccountTeamCreateModal();
                });
        }

    }
})();
