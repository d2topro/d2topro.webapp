/**
 * Created by DTuzenkov on 12/8/15.
 */
(function () {
    'use strict';

    angular
        .module('app.layout')
        .factory('SpinnerService', spinnerService);

    function spinnerService() {
        var activeSpinnersCount = 0;
        return {
            start: start,
            stop: stop
        };

        function start(element) {
            activeSpinnersCount++;
            attach(element);
        }

        function stop(element) {
            activeSpinnersCount--;
            detach(element);
        }

        function attach(element) {
            element
                .append(
                    '<div id="spinner" class="vertical-align text-center">' +
                        '<div class="loader vertical-align-middle loader-circle" data-type="circle"></div>' +
                    '</div>'
                );
        }

        function detach(element) {
            angular
                .element('body')
                .find('#spinner')
                .remove();
        }

    }
})();
