(function() {
    'use strict';

    angular
        .module('app.layout')
        .directive('loader', loader);

    /* @ngInject */
    function loader() {
        return {
            restrict: 'A',
            link: link,
        };

        /* @ngInject*/
        function link($rootScope, $document) {
            var spinnerEl = angular.element($document);
            console.dir(spinnerEl);

            $rootScope.$on('$stateChangeStart', function() {
                return attach(spinnerEl);
            });

            $rootScope.$on('$stateChangeSuccess', function() {
                return detach(spinnerEl);
            });



            function attach() {
                spinnerEl
                    .append(
                        '<div id="spinner" class="vertical-align text-center">' +
                        '<div class="loader loader-circle vertical-align-middle font-size-30" data-type="circle"></div>' +
                        '</div>'
                    );
            }

            function detach() {
                spinnerEl
                    .find('#spinner')
                    .remove();
            }

        }
    }
}());
