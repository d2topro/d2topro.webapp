(function() {
    'use strict';

    angular
        .module('app.account')
        .factory('AgreementModalService', agreementModalService);

    /* @ngInject */
    function agreementModalService($uibModal) {

        return {
            showAgreementModal: showAgreementModal,
        };

        function showAgreementModal(account) {
            return $uibModal
                .open({
                    animation: true,
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'app/layout/modals/agreement/agreement.html',
                    controller: 'AgreementModalController',
                    controllerAs: 'vm',
                    size: 'sm modal-center width-400',
                    backdropClass: 'BACKDROP_CLASS',
                    windowTopClass: '',
                    resolve: {
                        account: function() {
                            return account;
                        }
                    }
                })
                .result;
        }
    }
})();
