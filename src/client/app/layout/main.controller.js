(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('MainController', mainController);

    /* @ngInject */
    function mainController($state, $scope, $rootScope, $window, $q,
        lodash, AccountService, TournamentService, MainService) {

        var vm = this;
        vm.account = null;

        activate()
            .then(isFirstStart)
            .then(MainService.checkPreparedTournaments)
            .then(MainService.bindSocketEvents)
            .then(MainService.bindIntercomService);

        function activate() {
            return $q.all({
                    account: AccountService.getAccount(),
                })
                .then(function (response) {
                    vm.account = response.account;
                });
        }

        //@TODO: Figure out with first start event
        function isFirstStart() {
            return MainService.handleFirstStart(vm.account);
        }
    }
})();
