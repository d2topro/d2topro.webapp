(function () {
    'use strict';

    angular
        .module('app.core')
        .controller('SummonController', controller);

    /* @ngInject */
    function controller($state, AccountService) {
        var vm = this;

        activate();

        function activate() {
            return AccountService.getAccount()
                .then(function () {
                    $state.go('root.dashboard');
                });
        }
    }

})();
