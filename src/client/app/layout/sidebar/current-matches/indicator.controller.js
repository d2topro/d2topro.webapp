(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('IndicatorController', controller);

    function controller($scope, lodash, TournamentService, SocketService,
        CurrentMatchService) {

        var vm = this;
        vm.currentMatches = [];
        vm.enterLobby = enterLobby;

        activate();

        var currentMatchListener = $scope.$watch(
            function() {
                return CurrentMatchService.getMatches();
            },
            function(newValue, oldValue) {
                if (newValue !== oldValue) {
                    return newValue
                        .then(function(matches) {
                            vm.currentMatches = matches;
                        });
                }
            }
        );

        $scope.$on('destroy', function() {
            currentMatchListener();
        });

        function activate() {
            return CurrentMatchService.getMatches()
                .then(function(matches) {
                    vm.currentMatches = matches;
                })
        }

        function enterLobby(tournamentId) {
            return TournamentService.enterLobby(tournamentId);
        }
    }
})();
