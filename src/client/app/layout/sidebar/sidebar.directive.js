(function() {
    'use strict';

    angular
        .module('app.layout')
        .directive('sidebar', sidebar);

    function sidebar() {
        return {
            restrict: 'E',
            templateUrl: 'app/layout/sidebar/sidebar.html',
            controller: 'Sidebar',
            controllerAs: 'vm'
        }
    }
}());
