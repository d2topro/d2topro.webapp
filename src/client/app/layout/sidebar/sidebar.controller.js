(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('Sidebar', Sidebar);

    /* @ngInject */
    function Sidebar($q, $state, $intercom, routerHelper, lodash,
        AuthorizationService,
        AccountService, TournamentService, SocketService) {

        var vm = this;
        vm.account = null;
        vm.access = null;
        vm.currentTournamnets = [];
        vm.logout = logout;
        vm.isCurrent = isCurrent;
        vm.getRoutes = getRoutes;
        vm.getRestrictRoutes = getRestrictRoutes;

        activate();

        SocketService.on('tm:start', function () {
            return TournamentService.fetch()
                .then(function (tournaments) {
                    vm.currentTournaments = TournamentService
                        .filterNewTournaments(tournaments);
                });
        });

        SocketService.on('tm:complete', function () {
            return TournamentService.fetch()
                .then(function (tournaments) {
                    vm.currentTournaments = TournamentService
                        .filterNewTournaments(tournaments);
                });
        });

        function activate() {
            return $q.all({
                    tournaments: TournamentService.getTournaments(),
                    account: AccountService.getAccount(),
                    access: AccountService.checkAccess()
                })
                .then(function (response) {
                    vm.account = response.account;
                    vm.currentTournaments = TournamentService
                        .filterNewTournaments(response.tournaments);
                    vm.access = response.access;
                });
        }

        function getRoutes() {
            return lodash
                .chain(routerHelper.getStates())
                .filter(function (r) {
                    return r.settings && r.settings.nav && !r.settings.restrict;
                })
                .sort(sort)
                .value();
        }

        function getRestrictRoutes() {
            return lodash
                .chain(routerHelper.getStates())
                .filter(function (r) {
                    return r.settings && r.settings.restrict;
                })
                .sort(sort)
                .value();
        }

        function sort(r1, r2) {
            return r1.settings.nav - r2.settings.nav;
        }

        function isCurrent(route) {
            return $state.current.name === route.name;
        }

        function logout() {
            $intercom('trackEvent', 'WR_LOGGED_OUT');
            AuthorizationService.logout();
        }
    }
})();
