(function() {
    'use strict';

    angular
        .module('app.account')
        .factory('OnboardingTourModalService', onboardingTourModalService);

    /* @ngInject */
    function onboardingTourModalService($uibModal) {

        return {
            showOnboardingTourModal: showOnboardingTourModal,
        };

        function showOnboardingTourModal(account) {
            return $uibModal
                .open({
                    animation: true,
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'app/layout/modals/onboarding-tour/onboarding-tour.html',
                    controller: 'onboardingTourController',
                    controllerAs: 'vm',
                    size: 'onboarding-layout modal-center',
                    backdropClass: 'BACKDROP_CLASS',
                    windowTopClass: '',
                    resolve: {
                        account: function() {
                            return account;
                        }
                    }
                })
                .result;
        }
    }
})();
