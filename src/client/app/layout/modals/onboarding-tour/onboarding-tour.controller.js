(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('onboardingTourController', onboardingTourController);

    /* @ngInject */
    function onboardingTourController(AccountService, $uibModalInstance,
        $intercom) {
        var vm = this;
        vm.stepCount = 1;
        vm.goNextStep = goNextStep;
        vm.goPrewStep = goPrewStep;
        vm.account = {};

        activate();

        function activate() {
            return AccountService.getAccount()
                .then(function(account) {
                    vm.account = account;
                });
        }

        function goNextStep() {
            if (vm.stepCount < angular.element('.onboard-step')
                .length) {
                return vm.stepCount++;
            }
            $intercom('trackEvent', 'WR_TOUR_COMPLETED');
            close();

        }

        function close() {
            $uibModalInstance.close();
        }

        function goPrewStep() {
            if (vm.stepCount >= 1) {
                vm.stepCount--;
            }
        }

    }
})();
