(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('UnauthorizedController', unauthorizedController);

    function unauthorizedController (AuthorizationService) {
        var vm = this;

        vm.authorize = function () {
            AuthorizationService.goToSteamAuth();
        }
    }
})();
