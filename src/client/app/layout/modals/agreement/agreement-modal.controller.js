(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('AgreementModalController', agreementModalController);

    /* @ngInject*/
    function agreementModalController($uibModalInstance, account,
        OnboardingTourModalService, RULES_PAGE_URL, $intercom) {

        var vm = this;
        vm.account = null;
        vm.close = close;
        vm.account = account;
        vm.rules_page_url = RULES_PAGE_URL;
        vm.accept = accept;
        vm.close = close;
        vm.accepted = false;


        function accept() {
            if (vm.accepted) {
                $uibModalInstance.close();
                OnboardingTourModalService.showOnboardingTourModal(account);
                $intercom('trackEvent', 'WR_RULES_ACCEPTED');

                return vm.account.$acceptTermsConds()
                    .then(vm.account.$firstStart()
                        .$promise);
            }
        }
    }
})();
