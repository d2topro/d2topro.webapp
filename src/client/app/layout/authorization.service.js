(function () {
    'use strict';

    angular
        .module('app.layout')
        .factory('AuthorizationService', authorizationService);

    /* @ngInject */
    function authorizationService($q, $timeout, $stateParams,
        $window, $location, $uibModal, $http, config) {

        return {
            showUnauthorizedModal: showUnauthorizedModal,
            goToSteamAuth: goToSteamAuth,
            logout: logout
        };

        function showUnauthorizedModal() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/layout/modals/unauthorized.html',
                controller: 'UnauthorizedController',
                controllerAs: 'vm',
                size: 'center',
                windowClass: '',
                resolve: {}
            });

            return modalInstance;
        }

        function goToSteamAuth() {
            var returnUrl = $location.absUrl(),
                deferred = $q.defer(),
                params = {},
                parts = $location.absUrl()
                .split('/'),
                summoner = $window.atob(parts.pop());

            if (summoner) {
                params.summoner = summoner;
            } else {
                params.returnUrl = returnUrl;
            }

            $window.location.href =
                config.apiUrl + '/auth/steam?' + $.param(params);

            $timeout(deferred.resolve, 5000);

            return deferred.promise;
        }

        function logout() {
            $http.get(config.apiUrl + '/auth/logout')
                .then(function () {
                    $window.location.reload();
                });
        }
    }
})();
