(function() {
    'use strict';

    angular
        .module('app.layout')
        .service('MainService', mainService);

    /* @ngInject */
    function mainService($q, $intercom, lodash, ngAudio,
        AccountService, TournamentService, TournamentRequestService,
        BalanceService, TransactionService, IntercomService, MatchService,
        CurrentMatchService, SocketService, AgreementModalService,
        TournamentsModalService, TOURNAMENT_START_SOUND) {

        return {
            bindSocketEvents: bindSocketEvents,
            handleFirstStart: handleFirstStart,
            checkPreparedTournaments: checkPreparedTournaments
        };

        // TODO: Create sound service instead
        function playTournamentStartSound() {
            var sound = ngAudio.load(TOURNAMENT_START_SOUND);
            sound.volume = 0.65;
            return sound.play();
        }

        // TODO: WTF THIS FUCKING GOING WRONG!
        function bindIntercomService() {
            return IntercomService.getUserData()
                .then(function(userData) {
                    $scope.user = userData;
                    $intercom.$on('show', function() {
                        // currently Intercom onShow callback isn't working
                        $scope.showing = true;
                    });
                    $intercom.$on('hide', function() {
                        $scope.showing = false;
                    });

                    $scope.show = function() {
                        $intercom.show();
                    };

                    $scope.hide = function() {
                        $intercom.hide();
                    };

                    $scope.update = function(user) {
                        $intercom.update(user);
                    };
                });
        }

        function checkPreparedTournaments() {
            return TournamentService.getMyTournaments()
                .then(function(tournaments) {
                    var prepared = lodash.find(
                        tournaments,
                        function(tournament) {
                            return tournament.isPreConditions();
                        }
                    );

                    if (!prepared) {
                        return;
                    }

                    return TournamentsModalService
                        .showTournamentRequestsModal(prepared.id);
                });
        }

        function bindSocketEvents() {
            //TODO: Implement
            // SocketService.on('notification', function (data) {});

            SocketService.on('tm:start', function(data) {
                var tournament;
                return $q.all({
                        tournaments: TournamentService.getMyTournaments(),
                        account: AccountService.getAccount(),
                        tournament: TournamentService.fetch()
                    })
                    .then(function(response) {
                        tournament = lodash
                            .find(response.tournaments, function(t) {
                                return t.id === data.id;
                            });

                        if (tournament && response.account.allow_sound) {
                            return playTournamentStartSound();
                        }
                    });
            });

            SocketService.on('tm:complete', function() {
                return TournamentService.fetch();
            });

            SocketService.on('tm:match:complete', function(data) {
                if (data.has_series_end) {
                    checkResults(data);
                }
                BalanceService.fetch();
                TransactionService.fetch();
                MatchService.fetch(data.tournament_id);
                CurrentMatchService.fetch();
            });

            SocketService.on('tm:match:lobby:start', function() {
                return CurrentMatchService.fetch();
            });

            SocketService.on('tm:match:start', function() {
                return CurrentMatchService.fetch();
            });

            SocketService.on('tm:request:prepare', function(data) {
                return AccountService
                    .getAccount()
                    .then(function(account) {
                        if (account.allow_sound) {
                            playTournamentStartSound();
                        }

                        return TournamentsModalService
                            .showTournamentRequestsModal(data.id);
                    });
            });
        }

        function checkResults(data) {
            return TournamentRequestService
                .fetch(data.tournament_id)
                .then(function() {
                    return $q.all({
                        match: MatchService
                            .getMatchWithOutcome(data.tournament_id, data.id),
                        accountRequest: TournamentRequestService
                            .getAccountRequest(data.tournament_id),
                    });
                })
                .then(function(response) {
                    var match = response.match;
                    var action;

                    if (match.accountOutcome === match.OUTCOME.WIN) {
                        action = 'showTournamentWonModal';
                    } else if (match.accountOutcome === match.OUTCOME.LOSE ||
                        match.accountOutcome === match.OUTCOME.TECH_LOSE) {
                        action = 'showTournamentLoseModal';
                    }

                    if (!action) {
                        return;
                    }

                    return TournamentService[action]({
                        reward: response.accountRequest.reward_amount,
                        place: response.accountRequest.place_taken,
                        matchId: data.id,
                        tournamentId: data.tournament_id,
                        match: match
                    });
                });
        }

        function handleFirstStart(account) {
            if (account.is_first_start) {
                AgreementModalService.showAgreementModal(account);
                console.log('First start');
            }
        }
    }
})();
