/**
 * Created by DTuzenkov on 11/30/15.
 */
(function () {
    'use strict';
    angular
    // TODO: Should move this controller into next module
        .module('app.onboarding')
        .controller('OnboardingAddPlayersController', onboardingAddPlayersController);

    /* @ngInject */
    function onboardingAddPlayersController($scope, $rootScope, $state, $q, $uibModal, lodash,
                                            AccountTeamService,
                                            PlayerInviteResource, PlayerResource,

                                            AccountTeamPlayerResource, logger) {

        var EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        var INVITES_LIMIT = 4;
        var vm = this;
        vm.invitesLimit = INVITES_LIMIT;
        vm.players = [];
        vm.team = null;
        vm.inviteResults = [];
        vm.invites = [
            //{steam_personal_name: 'player1@email.com', email: 'player1@email.com'},
            //{steam_personal_name: 'player2@email.com', email: 'player2@email.com'},
            //{steam_personal_name: 'player3@email.com', email: 'player3@email.com'},
            //{steam_personal_name: 'player4@email.com', email: 'player4@email.com'}
        ];
        vm.invitePlayers = invitePlayers;
        vm.search = search;
        vm.createEmailInvite = createEmailInvite;
        vm.invitesLeft = invitesLeft;
        vm.isNotEmail = isNotEmail;

        activate();

        function activate() {
            return AccountTeamService
                .fetch()
                .then(function (team) {
                    vm.team = team;
                });
        }

        function invitePlayers() {
            var notYetPlayersResults;
            var alreadyPlayers = lodash
                .chain(vm.invites)
                .filter(function (invite) {
                    return !!invite.id;
                })
                .map(function (invite) {
                    return AccountTeamPlayerResource
                        .invite(
                            {team_id: vm.team.id, account_id: null},
                            {account_id: invite.id}
                        )
                        .$promise;
                })
                .value();

            var notYetPlayers = lodash
                .chain(vm.invites)
                .filter(function (invite) {
                    return !!invite.email;
                })
                .map(function (invite) {
                    return PlayerInviteResource.invite({email: invite.email}).$promise;
                })
                .value();


            $q
                .all(notYetPlayers)
                .then(function (results) {
                    notYetPlayersResults = results;
                    return $q.all(alreadyPlayers);
                })
                .then(function (results) {
                    showInviteResultsModal(results, notYetPlayersResults);
                });
        }

        function showInviteResultsModal(alreadyPlayers, notYetPlayers) {
            return $uibModal.open({
                    animation: false,
                    backdrop: 'static',
                    templateUrl: 'app/account/team/account-team-players-invite-result-modal.html',
                    controller: 'AccountTeamPlayersInviteResultModalController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        invites: function () {
                            return vm.invites;
                        },
                        alreadyPlayers: function () {
                            return alreadyPlayers;
                        },
                        notYetPlayers: function () {
                            return notYetPlayers;
                        }

                    }
                })
                .result
                .then(function (result) {
                    $state.go('dashboard');
                    $rootScope.$emit('captain:update');
                });
        }

        function search(query) {
            if (query.length < 3) {
                return;
            }
            return PlayerResource
                .search({
                    query: query
                })
                .$promise
                .then(function (resources) {
                    vm.players = resources;
                });
        }

        function createEmailInvite(input) {
            return {
                steam_personal_name: input,
                email: input
            };
        }

        function isNotEmail(input) {
            return EMAIL_REGEXP.test(input) == false;
        }

        function invitesLeft() {
            return INVITES_LIMIT - vm.invites.length;
        }
    }
})();
