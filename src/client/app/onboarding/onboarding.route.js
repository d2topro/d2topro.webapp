(function () {
    'use strict';

    angular
        .module('app.onboarding')
        .run(appRun);

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [{
            state: 'root.onboarding',
            config: {
                url: '/onboarding',
                abstract: true,
                template: '<ui-view class="shuffle-animation"/>'
            }
        }, {
            state: 'root.onboarding.welcome',
            config: {
                url: '/welcome',
                templateUrl: 'app/onboarding/onboarding.html',
                controller: 'OnboardingController',
                controllerAs: 'vm',
                title: 'Onboarding',
                settings: {}
            }
        }, {
            state: 'root.onboarding.register-team',
            config: {
                url: '/register-team',
                templateUrl: 'app/onboarding/onboarding-register-team.html',
                controller: 'OnboardingRegisterTeamController',
                controllerAs: 'vm',
                title: 'onboarding',
                settings: {}
            }
        }, {
            state: 'root.onboarding.add-players',
            config: {
                url: '/add-players',
                templateUrl: 'app/onboarding/onboarding-add-players.html',
                controller: 'OnboardingAddPlayersController',
                controllerAs: 'vm',
                title: 'onboarding',
                settings: {}
            }
        }];
    }
})();
