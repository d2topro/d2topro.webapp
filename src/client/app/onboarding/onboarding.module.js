/**
 * Created by DTuzenkov on 11/30/15.
 */
(function() {
    'use strict';

    angular.module('app.onboarding', [
        'app.core'
    ]);

})();
