/**
 * Created by DTuzenkov on 11/30/15.
 */
(function () {
    'use strict';
    angular
        .module('app.onboarding')
        .controller('OnboardingRegisterTeamController', onboardingRegisterTeamController);

    /* @ngInject */
    function onboardingRegisterTeamController($q, $state, AccountService, AccountTeamService) {
        var vm = this;
        vm.account = null;
        vm.create = create;
        vm.team = {
            name: '',
            tag: ''
            //logo: ''
        };

        activate();

        function activate() {
            return $q
                .all([
                    AccountService.fetch(),
                    AccountTeamService.fetch()
                ])
                .then(function (results) {
                    vm.account = results[0];
                    if (results[1]) {
                        $state.go('onboarding.add-players');
                    }
                });
        }

        function create() {
            AccountTeamService
                .create(vm.team)
                .then(function (response) {
                    var fields =
                        response.description && response.description.fields ? response.description.fields : null;
                    if (fields) {
                        vm.errorNameIsAlreadyTaken = fields && fields.name;
                        vm.errorTagIsAlreadyTaken = fields && fields.tag;
                        return $q.reject();
                    }
                    return AccountService.update({
                        is_first_start: false,
                        is_solo: false
                    });
                })
                .then(function () {
                    return $state.go('onboarding.add-players');
                });
        }


    }
})();
