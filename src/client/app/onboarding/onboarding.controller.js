/**
 * Created by DTuzenkov on 11/30/15.
 */
(function () {
    'use strict';
    angular
        .module('app.onboarding')
        .controller('OnboardingController', onboardingController);

    /* @ngInject */
    function onboardingController($state, AccountService, AccountTeamService, logger) {
        var vm = this;
        vm.registerYourTeam = registerYourTeam;
        vm.playAsSoloPlayer = playAsSoloPlayer;
        vm.account = {
            steam_personal_name: 'John',
            steam_avatar_url: null
        };
        activate();
        function activate() {
            return AccountService
                .fetch()
                .then(function (account) {
                    vm.account = account;
                    return AccountTeamService.fetch();
                })
                .then(function(team) {
                    if (!vm.account.is_first_start && team) {
                        //return goDashboard();
                    }
                });
        }

        function registerYourTeam() {
            $state.go('onboarding.register-team');
        }

        function playAsSoloPlayer() {
            return AccountService
                .playSoloMode()
                .then(goDashboard);
        }

        function goDashboard () {
            $state.go('dashboard');
        }
    }
})();
