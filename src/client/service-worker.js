/* global self, message, clients, console */
'use strict';

self.addEventListener('install', function (event) {
    self.skipWaiting();
    console.log('Installed', event);
});

self.addEventListener('activate', function (event) {
    console.log('Activated', event);
});

self.addEventListener('push', function (event) {
    console.log('Push message', event);
    var json;
    try {
        json = event.data.json();
    } catch (e) {
        json = { action: 'trace' };
    }

    var message = {
        icon: '/images/push-notification-icon.jpg'
    };

    switch (json.action) {
    case 'tournament:ready':
        message.title = 'Tournament is started';
        message.body = 'Accept your invitation in Dota 2';
        break;

    case 'trace':
    default:
        message.title = 'Tournament is started';
        message.body = 'Accept your invitation in Dota 2';
    }

    event.waitUntil(
        self.registration.showNotification(message.title, {
            'body': message.body,
            'icon': message.icon
        }));
});

self.addEventListener('notificationclick', function (event) {
    event.notification.close();
    var url = location.origin;

    event.waitUntil(
        clients.matchAll({
            type: 'window'
        })
        .then(function (windowClients) {
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
});
